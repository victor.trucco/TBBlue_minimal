--
-- TBBlue / ZX Spectrum Next project
-- Copyright (c) 2015 - Fabio Belavenuto & Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
-- ID 250 = ZX Spectrum Next Anti-brick
--
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

library UNISIM;
use UNISIM.VComponents.all;
		
entity next_ab_issue2_top is
	generic (

		machine_id_g	   : unsigned(7 downto 0)	:= X"FA";			-- 250 = ZX Spctrum Next Anti-brick
		version_g			: unsigned(7 downto 0)	:= X"00";	-- 0.00
		sub_version_g		: unsigned(7 downto 0)	:= X"00"		-- .0
	);
	port (
			-- Clocks
		clock_50_i			: in    std_logic;

		-- SRAM (AS7C34096)
		ram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		ram_data_io			: inout std_logic_vector(15 downto 0)	:= (others => 'Z');
		ram_oe_n_o			: out   std_logic								:= '1';
		ram_we_n_o			: out   std_logic								:= '1';
		ram_ce_n_o			: out   std_logic_vector( 3 downto 0)	:= (others => '1');

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_pin6_io			: inout std_logic								:= 'Z';	-- Mouse clock
		ps2_pin2_io 		: inout std_logic								:= 'Z';	-- Mouse data

		-- SD Card
		sd_cs0_n_o			: out   std_logic								:= '1';
		sd_cs1_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Flash
		flash_cs_n_o		: out   std_logic								:= '1';
		flash_sclk_o		: out   std_logic								:= '0';
		flash_mosi_o		: out   std_logic								:= '0';
		flash_miso_i		: in    std_logic;
		flash_wp_o			: out   std_logic								:= '0';
		flash_hold_o		: out   std_logic								:= '1';

		-- Joystick
		joyp1_i				: in    std_logic;
		joyp2_i				: in    std_logic;
		joyp3_i				: in    std_logic;
		joyp4_i				: in    std_logic;
		joyp6_i				: in    std_logic;
		joyp7_o				: out   std_logic								:= '1';
		joyp9_i				: in    std_logic;
		joysel_o				: out   std_logic								:= '0';

		-- Audio
		audioext_l_o		: out   std_logic								:= '0';
		audioext_r_o		: out   std_logic								:= '0';
		audioint_o			: out   std_logic								:= '0';

		-- K7
		ear_port_i			: in    std_logic;
		mic_port_o			: out   std_logic								:= '0';

		-- Buttons
		btn_divmmc_n_i		: in    std_logic;
		btn_multiface_n_i	: in    std_logic;
		btn_reset_n_i		: in    std_logic;

		-- Matrix keyboard
		keyb_row_o			: out   std_logic_vector( 7 downto 0)	:= (others => '1');
		keyb_col_i			: in    std_logic_vector( 4 downto 0);

		-- Bus
		bus_rst_n_io		: inout std_logic								:= 'Z';
		bus_clk35_o			: out   std_logic								:= '0';
		bus_addr_o			: out   std_logic_vector(15 downto 0)	:= (others => '0');
		bus_data_io			: inout std_logic_vector( 7 downto 0)	:= (others => 'Z');
		bus_int_n_i			: in    std_logic;
		bus_nmi_n_i			: in    std_logic;
		bus_ramcs_i			: in    std_logic;
		bus_romcs_i			: in    std_logic;
		bus_wait_n_i		: in    std_logic;
		bus_halt_n_o		: out   std_logic								:= '1';
		bus_iorq_n_o		: out   std_logic								:= '1';
		bus_m1_n_o			: out   std_logic								:= '1';
		bus_mreq_n_o		: out   std_logic								:= '1';
		bus_rd_n_o			: out   std_logic								:= '1';
		bus_wr_n_o			: out   std_logic								:= '1';
		bus_rfsh_n_o		: out   std_logic								:= '1';
		bus_busreq_n_i		: in    std_logic;
		bus_busack_n_o		: out   std_logic								:= '1';
		bus_iorqula_n_i	: in    std_logic;

		-- VGA
		rgb_r_o				: out   std_logic_vector( 2 downto 0)	:= (others => '0');
		rgb_g_o				: out   std_logic_vector( 2 downto 0)	:= (others => '0');
		rgb_b_o				: out   std_logic_vector( 2 downto 0)	:= (others => '0');
		hsync_o				: out   std_logic								:= '1';
		vsync_o				: out   std_logic								:= '1';
		csync_o				: out   std_logic								:= '1';

		-- HDMI
		hdmi_p_o				: out   std_logic_vector(3 downto 0);
		hdmi_n_o				: out   std_logic_vector(3 downto 0);
--		hdmi_cec_i			: in    std_logic;

		-- I2C (RTC and HDMI)
		i2c_scl_io			: inout std_logic								:= 'Z';
		i2c_sda_io			: inout std_logic								:= 'Z';

		-- ESP
		esp_gpio0_io		: inout std_logic								:= 'Z';
		esp_gpio2_io		: inout std_logic								:= 'Z';
		esp_rx_i				: in    std_logic;
		esp_tx_o				: out   std_logic								:= '0';

		-- ACCELERATOR BOARD
		accel_io_27		: out std_logic; -- RPi pin 13
		accel_io_26		: out std_logic; -- RPi pin 37
		accel_io_25		: out std_logic; -- RPi pin 22
		accel_io_24		: out std_logic; -- RPi pin 18
		accel_io_23		: out std_logic; -- RPi pin 16
		accel_io_22		: out std_logic; -- RPi pin 15
		accel_io_21		: out std_logic; -- RPi pin 40
		accel_io_20		: out  std_logic;  -- RPi pin 38
		accel_io_19		: out  std_logic; -- RPi pin 35
		accel_io_18		: out  std_logic; -- RPi pin 12
		accel_io_17		: out  std_logic; -- RPi pin 11
		accel_io_16		: out  std_logic;  -- RPi pin 36
		accel_io_15		: out  std_logic; -- RPi pin 10
		accel_io_14		: out  std_logic; -- RPi pin 8
		accel_io_13		: out  std_logic; -- RPi pin 33
		accel_io_12		: out  std_logic; -- RPi pin 32
		accel_io_11		: out  std_logic  := '0'; 	-- RPi pin 23 -- SPI0 SCLK
		accel_io_10		: out  std_logic  := '0'; 	-- RPi pin 19 -- SPI0 MOSI
		accel_io_9		: in  std_logic;  			-- RPi pin 21 -- SPI0 MISO
		accel_io_8		: out std_logic 	:= '1'; 	-- RPi pin 24 -- SPI0 CS0
		accel_io_7		: out std_logic 	:= '1'; 	-- RPi pin 26 -- SPI0 CS1
		accel_io_6		: out std_logic; -- RPi pin 31
		accel_io_5		: out std_logic; -- RPi pin 29
		accel_io_4		: out std_logic; -- RPi pin 7
		accel_io_3		: out std_logic; -- RPi pin 5
		accel_io_2		: out std_logic; -- RPi pin 3
		accel_io_1		: out std_logic; -- RPi pin 28
		accel_io_0		: out std_logic; -- RPi pin 27
		
		
		-- Vacant pins
		extras_io			: inout std_logic_vector(2 downto 0)	:= (others => 'Z')
	);
end entity;

architecture behavior of next_ab_issue2_top is

	component ps2mouse 
	port
	(
		clk 		: in std_logic;		    					-- bus clock
		reset 	: in std_logic;			   				-- reset 
		
		ps2mdat_i 	: in std_logic;			 				-- mouse PS/2 data
		ps2mclk_i 	: in std_logic;			 				-- mouse PS/2 clk
		ps2mdat_o 	: out std_logic;			 				-- mouse PS/2 data
		ps2mclk_o	: out std_logic;			 				-- mouse PS/2 clk
		
		mou_emu 	: in std_logic_vector(5 downto 0); 		-- mouse with joystick input
		sof 		: in std_logic;								-- mouse joystick emulation enable bit
		zcount	: out std_logic_vector(7 downto 0);  	-- mouse Z counter
		ycount	: out std_logic_vector(7 downto 0);		-- mouse Y counter
		xcount	: out std_logic_vector(7 downto 0);		-- mouse X counter
		mleft		: out std_logic;								-- left mouse button output
		mthird	: out std_logic;								-- third(middle) mouse button output
		mright	: out std_logic;								-- right mouse button output
		test_load: in std_logic;								-- load test value to mouse counter
		test_data: in std_logic_vector(15 downto 0)		-- mouse counter test value
  );
  end component;
  
	-- Clocks
	signal clock_28_s			: std_logic;
	signal clock_28_180o_s	: std_logic;
	signal clock_14_s			: std_logic;
	signal clock_7_s			: std_logic;
	signal clock_3m5_s		: std_logic;
	signal clock_psg_s		: std_logic;
	signal clock_cpu_s		: std_logic;
	signal cpu_clock_cont_s	: std_logic;	
	signal clock_hdmi_s		: std_logic;
	signal clock_hdmi_n_s	: std_logic;
	signal clock_div_q		: unsigned(3 downto 0) 				:= (others => '0');				
	signal membrane_div_q	: unsigned(4 downto 0) 				:= (others => '0');	
	signal turbo_s				: std_logic_vector(1 downto 0);
	signal clock_turbo_s		: std_logic;
	signal clock_buf_s		: std_logic;

	-- Resets
	signal poweron_cnt_s		: unsigned( 3 downto 0)				:= (others => '1');
	signal poweron_s			: std_logic								:= '0';
	signal hard_reset_s		: std_logic;
	signal soft_reset_s		: std_logic;
	signal int_soft_reset_s	: std_logic;
	signal reset_s				: std_logic;
	signal core_reload_s		: std_logic;
	signal btn_reset_db_s	: std_logic;

	-- Memory buses
	signal vram_addr_s		: std_logic_vector(20 downto 0);
	signal vram_dout_s		: std_logic_vector( 7 downto 0);
	signal vram_cs_s			: std_logic;
	signal vram_oe_s			: std_logic;
	signal ram_addr_s			: std_logic_vector(20 downto 0);
	signal ram_din_s			: std_logic_vector( 7 downto 0);
	signal ram_dout_s			: std_logic_vector( 7 downto 0);
	signal ram_cs_s			: std_logic;
	signal ram_oe_s			: std_logic;
	signal ram_we_s			: std_logic;
	signal rom_addr_s			: std_logic_vector(13 downto 0);		-- 16K
	signal rom_data_s			: std_logic_vector( 7 downto 0);
	signal bootrom_s			: std_logic;
	
	signal intram_cs_s		: std_logic;
	signal intram_we_s		: std_logic;
	signal intram_dout_s		: std_logic_vector( 7 downto 0);
	signal intvram_cs_s		: std_logic;
	signal intvram_dout_s	: std_logic_vector( 7 downto 0);
	signal memram_cs_s		: std_logic;
	signal memvram_cs_s		: std_logic;
	signal ramdout_mux_s		: std_logic_vector( 7 downto 0);
	signal vramdout_mux_s	: std_logic_vector( 7 downto 0);
	
	signal mux_addr_s			: std_logic_vector(20 downto 0);
	signal mux_din_s			: std_logic_vector( 7 downto 0);
	signal mux_dout_s			: std_logic_vector( 7 downto 0);
	signal mux_cs_s			: std_logic;
	signal mux_oe_s			: std_logic;
	signal mux_wr_s			: std_logic;

	-- Audio
	signal spk_s				: std_logic;
	signal mic_s				: std_logic;
	signal psg_L_s				: unsigned(10 downto 0);
	signal psg_R_s				: unsigned(10 downto 0);
	signal sid_L_s				: unsigned(17 downto 0);
	signal sid_R_s				: unsigned(17 downto 0);
	signal covox_L_s			: unsigned( 8 downto 0);
	signal covox_R_s			: unsigned( 8 downto 0);
	signal pcm_out_L_s		: std_logic_vector(13 downto 0);
	signal pcm_out_R_s		: std_logic_vector(13 downto 0);

	-- Keyboard
	signal kb_rows_s			: std_logic_vector( 7 downto 0);
	signal kb_int_rows_s		: std_logic_vector( 7 downto 0);
	signal kb_columns_s		: std_logic_vector( 4 downto 0);
	signal kb_int_cols_s		: std_logic_vector( 4 downto 0);
	signal kb_mes_cols_s		: std_logic_vector( 4 downto 0);
	signal FKeys_s				: std_logic_vector(12 downto 1);
	signal emu_fnkeys_s		: std_logic_vector(12 downto 1);
	signal port254_rd_s	 	: std_logic;
	signal keymap_addr_s		: std_logic_vector( 8 downto 0);
	signal keymap_data_s		: std_logic_vector( 8 downto 0);
	signal keymap_we_s		: std_logic;
	signal keys_hard_s		: std_logic_vector( 1 downto 0);
	signal membrane_col_s	: std_logic_vector( 4 downto 0);
	
	-- Buttons
	signal btn_divmmc_db_n_s	: std_logic;
	signal btn_mf_db_n_s			: std_logic;
	signal btn_mf_filter_n_s	: std_logic;

	-- SPI
	signal sd_cs0_n_s			: std_logic;
	signal sd_cs1_n_s			: std_logic;
	signal RPi_SPI_cs0_n_s	: std_logic;
	signal RPi_SPI_cs1_n_s	: std_logic;
	signal RPi_miso_s			: std_logic;
	signal spi_mosi_s			: std_logic;
	signal spi_sclk_s			: std_logic;

	-- Joystick
	signal joy1_s				: std_logic_vector( 5 downto 0);
	signal joy2_s				: std_logic_vector( 5 downto 0);

	-- Video and scandoubler
	signal rgb_r_s				: std_logic_vector( 2 downto 0);
	signal rgb_g_s				: std_logic_vector( 2 downto 0);
	signal rgb_b_s				: std_logic_vector( 2 downto 0);
	signal rgb_hs_n_s			: std_logic;
	signal rgb_vs_n_s			: std_logic;
	signal rgb_cs_n_s			: std_logic;
	signal rgb_ula_s			: std_logic_vector( 8 downto 0);
	signal rgb_15_s			: std_logic_vector( 8 downto 0);
	signal rgb_31_s			: std_logic_vector( 8 downto 0);
	signal scandbl_en_s		: std_logic;
	signal scanlines_s		: std_logic_vector(1 downto 0);
	signal hsync_out_s		: std_logic;
	signal vsync_out_s		: std_logic;
	signal blank_s				: std_logic;
	signal mach_timing_s		: std_logic_vector( 2 downto 0) := "000";
	signal mach_video_timing_s		: std_logic_vector( 2 downto 0)  := "000";
	signal ha_value_s			: integer;
	signal hblank_s			: std_logic;
	signal vblank_s			: std_logic;
	signal ula50_60_s			: std_logic								:= '0';
	
	-- HDMI
	signal tdms_r_s			: std_logic_vector( 9 downto 0);
	signal tdms_g_s			: std_logic_vector( 9 downto 0);
	signal tdms_b_s			: std_logic_vector( 9 downto 0);
	signal hdmi_p_s			: std_logic_vector( 3 downto 0);
	signal hdmi_n_s			: std_logic_vector( 3 downto 0);

	-- Mouse
	signal mouse_x_s			: std_logic_vector( 7 downto 0);
	signal mouse_y_s			: std_logic_vector( 7 downto 0);
	signal mouse_bts_s		: std_logic_vector( 2 downto 0);
	signal mouse_wheel_s		: std_logic_vector( 7 downto 0);

	-- Bus
	signal bus_addr_s			: std_logic_vector(15 downto 0);
	signal bus_datai_s		: std_logic_vector( 7 downto 0);
	signal bus_datao_s		: std_logic_vector( 7 downto 0);
	signal bus_iorq_n_s		: std_logic;
	signal bus_mreq_n_s		: std_logic;
	signal bus_rd_n_s			: std_logic;
	signal bus_wr_n_s			: std_logic;
	signal bus_m1_n_s			: std_logic;

	-- Accelerator
	signal ac_border_s		: std_logic;
	signal ac_vram_s			: std_logic;
	signal ac_port_BF3B_s	: std_logic;
	signal ac_port_FF3B_s	: std_logic;

	-- latchs
	signal ac_latch_s 		: std_logic;
	signal ac_latch_a_s		: std_logic_vector(15 downto 0);
	signal ac_latch_d_s		: std_logic_vector( 7 downto 0);
	
	-- keyboard and mouse switcher
	signal ps2_mode_s		: std_logic := '0';
	signal k_data_in_s 	: std_logic;
	signal k_data_out_s 	: std_logic;
	signal k_clock_in_s 	: std_logic;
	signal k_clock_out_s : std_logic;
	signal m_data_in_s 	: std_logic;
	signal m_data_out_s 	: std_logic;
	signal m_clock_in_s 	: std_logic;
	signal m_clock_out_s : std_logic;
	
	signal ps2_data_out	: std_logic;
	signal ps2_clk_out  	: std_logic;
	signal ps2_mouse_data_out 	: std_logic;
	signal ps2_mouse_clk_out  	: std_logic;
	
	-- HDMI
	signal toHDMI_rgb_s		: std_logic_vector( 8 downto 0);
	signal toHDMI_hsync_s 	: std_logic;
	signal toHDMI_vsync_s 	: std_logic;
	signal toHDMI_blank_s	: std_logic;
	
	-- HDMI initial values
	signal h_visible_s	: integer := 0;
	signal hsync_start_s	: integer := 0;
	signal hsync_end_s	: integer := 0;
	signal hcnt_end_s		: integer := 0;
	signal v_visible_s	: integer := 0;
	signal vsync_start_s	: integer := 0;
	signal vsync_end_s	: integer := 0;
	signal vcnt_end_s		: integer := 0;

begin

	--------------------------------
	-- PLL
	--  50 MHz input
	--------------------------------
	pll: entity work.pll1
	port map (
		CLK_IN1	=> clock_50_i,
		CLK_OUT1		=> clock_28_s,			-- 28 MHz
		CLK_OUT2		=> clock_28_180o_s,	-- 28 Mhz inverted
		CLK_OUT3		=> clock_14_s,  		-- 14 MHz
		CLK_OUT4		=> clock_7_s,			-- 7 MHz
		CLK_OUT5		=> clock_hdmi_s, 		-- master * 5
		CLK_OUT6		=> clock_hdmi_n_s		-- master * 5 inverted
	);

	BUFG_inst1 : BUFG  
	port map	(
		I => clock_div_q(2),
		O => clock_3m5_s
	);

    BUFGMUX_inst1 : BUFGMUX
    port map (
       O => clock_buf_s,
       I0 => clock_7_s,
       I1 => clock_14_s,
       S => turbo_s(0)
    );
	
	 BUFGMUX_inst2 : BUFGMUX
    port map (
       O => clock_turbo_s,
       I0 => clock_buf_s,
       I1 => clock_28_s,
       S => turbo_s(1)
    );

	-- Geracao dos clocks
	process(clock_28_s)
	begin
		if rising_edge(clock_28_s) then 
			clock_div_q <= clock_div_q + 1;
		end if;
	end process;

	clock_psg_s	<= '1' when clock_div_q(3 downto 0) = "1110" else '0';

--	clock_3m5_s <= clock_div_q(2);				-- 3.5 MHz no contencion
	
--	clock_turbo_s	<= clock_7_s	when turbo_s = "00"		else --for 3.5
--							clock_14_s	when turbo_s = "01"		else --for 7
--							clock_28_s; -- for 14
	
	process (clock_turbo_s)
	begin
		if rising_edge(clock_turbo_s) then
			if clock_cpu_s = '1' and Cpu_clock_cont_s = '0' then		-- if there's no contention, the clock can go low
				clock_cpu_s <= '0';
			else
				clock_cpu_s <= '1';
			end if;
		end if;
	end process;
	
	-- The TBBlue
	tbblue1 : entity work.tbblue
	generic map (
		use_turbo		=> true,
		machine_id_g	=> machine_id_g,
		version_g		=> version_g,
		sub_version_g	=> sub_version_g,
		usar_kempjoy	=> '0',
		usar_keyjoy		=> '0',
		use_layer2_g	=> false,
		use_sprites_g	=> false,
		use_turbosnd_g	=> true,
		use_sid_g		=> false,
		use_dma_g		=> false,
		use_z80n_g		=> false,
		use_lightpen_g	=> false,
		use_copper_g	=> false
	)
	port map (
		-- Clock
		iClk_28				=> clock_28_s,
		iClk_28_180o		=> clock_28_180o_s,
		iClk_14				=> clock_14_s,
		iClk_7				=> clock_7_s,
		iClk_3M5				=> clock_3m5_s,				-- 3.5 MHz no contencion
		iClk_Cpu				=> clock_cpu_s,				-- CPU clock with contention
		iClk_Psg				=> clock_psg_s,				-- psg clock
		oCpu_clock_cont	=>	Cpu_clock_cont_s,			-- Signal to CPU clock contention
		oTurbo_sel			=> turbo_s,  					-- Selected turbo Speed

		-- Reset
		iPowerOn				=> poweron_s,
		iHardReset			=> hard_reset_s,
		iSoftReset			=> soft_reset_s,
		oSoftReset			=> int_soft_reset_s,
		-- Keys
		iKey50_60hz			=> FKeys_s(3) or emu_fnkeys_s(3),
		iKeyScanDoubler	=> FKeys_s(2) or emu_fnkeys_s(2),
		iKeyScanlines		=> FKeys_s(7) or emu_fnkeys_s(7),
		iKeyDivMMC			=> '0',
		iKeyMF				=> '0',
		iKeyTurbo			=> FKeys_s(8) or emu_fnkeys_s(8),
		iKeysHard			=> keys_hard_s,
		-- Keyboard
		oRows					=> kb_int_rows_s,
		iColumns				=> kb_columns_s,
		oPort254_cs			=> port254_rd_s,
		oKeymap_addr		=> keymap_addr_s,
		oKeymap_data		=> keymap_data_s,
		oKeymap_we			=> keymap_we_s,
		-- RGB
		oRGB_r				=> rgb_r_s,
		oRGB_g				=> rgb_g_s,
		oRGB_b				=> rgb_b_s,
		oRGB_hs_n			=> rgb_hs_n_s,
		oRGB_vs_n			=> rgb_vs_n_s,
		oRGB_cs_n			=> rgb_cs_n_s,
		oRGB_hb_n			=> hblank_s,
		oRGB_vb_n			=> vblank_s,
		oRGB_hcnt			=> open,
		oRGB_vcnt			=> open,
		oScandbl_en			=> scandbl_en_s,
		oScanlines			=> scanlines_s,
		oULA50_60			=> ula50_60_s,
		oMachTiming			=> mach_timing_s,
		oMachVideoTiming	=> mach_video_timing_s,

		-- VRAM
		oVram_a				=> vram_addr_s,
		iVram_dout			=> vramdout_mux_s,--vram_dout_s,
		oVram_cs				=> vram_cs_s,
		oVram_rd				=> vram_oe_s,

		-- Bootrom
		oBootrom_en			=> bootrom_s,
		oRom_a				=> rom_addr_s,
		iRom_dout			=> rom_data_s,
		oFlashboot			=> core_reload_s,

		-- RAM
		oRam_a				=> ram_addr_s,
		oRam_din				=> ram_din_s,
		iRam_dout			=> ramdout_mux_s,--ram_dout_s,
		oRam_cs				=> ram_cs_s,
		oRam_rd				=> ram_oe_s,
		oRam_wr				=> ram_we_s,

		-- SPI (SD and Flash)
		oSpi_mosi			=> spi_mosi_s,
		oSpi_sclk			=> spi_sclk_s,
		oSD_cs0_n			=> sd_cs0_n_s,
		oSD_cs1_n			=> sd_cs1_n_s,
		iSD_miso				=> sd_miso_i,
		oFlash_cs_n			=> flash_cs_n_o,
		iFlash_miso			=> flash_miso_i,
		iFT_miso				=> '1',
		oFT_cs_n				=> open,
		oRPI_cs0_n			=> open,
		oRPI_cs1_n			=> open,
		iRPi_miso			=> '1',

		-- Sound
		iEAR					=> ear_port_i,
		oSPK					=> spk_s,
		oMIC					=> mic_s,
		oPSG_L				=> psg_L_s,
		oPSG_R				=> psg_R_s,
		oSID_L				=> sid_L_s,
		oSID_R				=> sid_R_s,
		oCovox_L				=> covox_L_s,
		oCovox_R				=> covox_R_s,
		oDAC					=> open,
		oIntSnd				=> open,
		
		-- Joystick
		-- order: Fire2, Fire, Up, Down, Left, Right
		iJoy0					=> "00" & joy1_s,
		iJoy1					=> "00" & joy2_s,
		-- Mouse
		iMouse_en			=> '1',
		iMouse_x				=>	mouse_x_s,
		iMouse_y				=>	mouse_y_s,
		iMouse_bts			=>	mouse_bts_s,
		iMouse_wheel		=>	mouse_wheel_s(3 downto 0),
		oPS2mode				=> ps2_mode_s,
		
		-- Lightpen
		iLp_signal			=> '0',
		
		oLp_en				=> open,
		-- RTC
		ioRTC_sda			=> i2c_sda_io,
		ioRTC_scl			=> i2c_scl_io,
		
		-- Serial
		iRs232_rx			=> '0',
		oRs232_tx			=> open,
		iRs232_dtr			=> '0',
		oRs232_cts			=> open,
		
		-- BUS
		oCpu_a				=> bus_addr_s,
		oCpu_do				=> bus_datao_s,
		iCpu_di				=> bus_datai_s,
		oCpu_mreq			=> bus_mreq_n_s,
		oCpu_ioreq			=> bus_iorq_n_s,
		oCpu_rd				=> bus_rd_n_s,
		oCpu_wr				=> bus_wr_n_s,
		oCpu_m1				=> bus_m1_n_s,
		iCpu_Wait_n			=> bus_wait_n_i,
		iCpu_nmi				=> bus_nmi_n_i,
		iCpu_int_n			=> bus_int_n_i,
		iCpu_romcs			=> bus_romcs_i,
		iCpu_ramcs			=> bus_ramcs_i,
		iCpu_busreq_n		=> bus_busreq_n_i,
		oCpu_busack_n		=> bus_busack_n_o,
		oCpu_clock			=> bus_clk35_o,
		oCpu_halt_n			=> bus_halt_n_o,
		oCpu_rfsh_n			=> bus_rfsh_n_o,
		iCpu_iorqula		=> bus_iorqula_n_i,
		
		-- Layer 2
		oLayer2_addr		=> open,
		iLayer2_data		=> (others => '0'),
		oLayer2_cs			=> open,

		--Debug
		oD_leds				=> open,
		oD_reg_o				=> open,
		oD_others			=> open,
		iSW					=> (others=>'0')
	);

	-- Internal VRAM
--	intram_cs_s		<= '1' when ram_cs_s = '1'  and ram_addr_s(19 downto 14)  = "011101" else	'0';
--	intvram_cs_s	<= '1' when vram_cs_s = '1' and vram_addr_s(19 downto 14) = "011101" else	'0';
	
	intram_cs_s		<= '1' when ram_cs_s = '1'  and ram_addr_s(20 downto 14)  = "0010101" else	'0';
	intvram_cs_s	<= '1' when vram_cs_s = '1' and vram_addr_s(20 downto 14) = "0010101" else	'0';
	
	memram_cs_s		<= ram_cs_s 	when intram_cs_s = '0'	else '0';
	memvram_cs_s	<= vram_cs_s	when intvram_cs_s = '0'	else '0';
	intram_we_s		<= ram_we_s		when intram_cs_s = '1'	else '0';
	ramdout_mux_s	<= intram_dout_s	when intram_cs_s = '1'	else ram_dout_s;
	vramdout_mux_s	<= intvram_dout_s	when intvram_cs_s = '1'	else vram_dout_s;

	--
	intram : entity work.dpram2
	generic map (
		addr_width_g	=> 14,
		data_width_g	=> 8
	)
	port map (
		-- CPU port
		clk_a_i  => clock_28_s,
		we_i     => intram_we_s,
		addr_a_i => ram_addr_s(13 downto 0),
		data_a_i => ram_din_s,
		data_a_o => intram_dout_s,
		-- ULA port
		clk_b_i  => clock_28_s,
		addr_b_i => vram_addr_s(13 downto 0),
		data_b_o => intvram_dout_s
	);

	mux_addr_s	<= vram_addr_s;--		when layer2_cs_s = '0' else layer2_addr_s;
	mux_cs_s		<= memvram_cs_s;--	when layer2_cs_s = '0' else layer2_cs_s;
	mux_oe_s		<= vram_oe_s;--		when layer2_cs_s = '0' else layer2_cs_s; -- pixel_clock_s; -- not pixel_clock_s;
	vram_dout_s <= mux_dout_s;
	--layer2_data_s <= mux_dout_s;


	-- SRAM
	ram : entity work.dpSRAM_4x512x16
	port map(
		clk_i				=> clock_28_s,
		-- Port 0 (VRAM)
		port0_addr_i	=> mux_addr_s,
		port0_ce_i		=> mux_cs_s,
		port0_oe_i		=> mux_oe_s,
		port0_we_i		=> '0',
		port0_data_i	=> (others => '0'),
		port0_data_o	=> mux_dout_s,
		-- Port 1 (Upper RAM)
		port1_addr_i	=> ram_addr_s,
		port1_ce_i		=> memram_cs_s,
		port1_oe_i		=> ram_oe_s,
		port1_we_i		=> ram_we_s,
		port1_data_i	=> ram_din_s,
		port1_data_o	=> ram_dout_s,
		-- Outputs to SRAM on board
		sram_addr_o		=> ram_addr_o,
		sram_data_io	=> ram_data_io,
		sram_ce_n_o		=> ram_ce_n_o,
		sram_oe_n_o		=> ram_oe_n_o,
		sram_we_n_o		=> ram_we_n_o
	);

	-- Audio
	audio : entity work.Audio_DAC
	port map (
		clock_i	=> clock_28_s,
		reset_i	=> reset_s,
		ear_i		=> ear_port_i,
		spk_i		=> spk_s,
		mic_i		=> mic_s,
		psg_L_i	=> psg_L_s,
		psg_R_i	=> psg_R_s,
		sid_L_i	=> sid_L_s,
		sid_R_i	=> sid_R_s,
		covox_L_i=> covox_L_s,
		covox_R_i=> covox_R_s,
		dac_r_o	=> audioext_r_o,
		dac_l_o	=> audioext_l_o,
		pcm_L_o	=> pcm_out_L_s,
		pcm_R_o	=> pcm_out_R_s
	);
	
	
		 -- PS/2 keyboard 
	 k_data_in_s <= ps2_data_io when ps2_mode_s = '0' else ps2_pin2_io;
	 k_clock_in_s <= ps2_clk_io  when ps2_mode_s = '0' else ps2_pin6_io;
	 
	 ps2_data_io <= k_data_out_s when (ps2_data_out = '1' and ps2_mode_s = '0') else '0' when m_data_out_s = '0' and ps2_mode_s = '1' else 'Z';
	 ps2_clk_io  <= k_clock_out_s when (ps2_clk_out  = '1' and ps2_mode_s = '0') else '0' when m_clock_out_s = '0' and ps2_mode_s = '1' else 'Z';

	 -- PS/2 Mouse
	 m_data_in_s <= ps2_pin2_io when ps2_mode_s = '0' else ps2_data_io;
	 m_clock_in_s <= ps2_pin6_io  when ps2_mode_s = '0' else ps2_clk_io;
	 
	 ps2_pin2_io <= '0' when m_data_out_s = '0' and ps2_mode_s = '0' else k_data_out_s when (ps2_data_out = '1' and ps2_mode_s = '1') else 'Z';
	 ps2_pin6_io <= '0' when m_clock_out_s = '0' and ps2_mode_s = '0' else k_clock_out_s when (ps2_clk_out  = '1' and ps2_mode_s = '1') else 'Z';
	 

	-- PS/2 emulating speccy keyboard
	kb: entity work.ps2keyb
	port map (
		enable_i			=> '1',
		use_ps2_alt_i	=> '0',	
		clock_i			=> clock_28_s,
		clock_180o_i	=> clock_28_180o_s,
		clock_ps2_i		=> membrane_div_q(3),
		reset_i			=> poweron_s,
		--
		ps2_clk_i		=> k_clock_in_s,
		ps2_data_i		=> k_data_in_s,
		ps2_clk_o		=> k_clock_out_s,
		ps2_data_o		=> k_data_out_s,
		ps2_data_out	=> ps2_data_out,
		ps2_clk_out		=> ps2_clk_out,
		--
		rows_i			=> kb_rows_s,
		cols_o			=> kb_int_cols_s,
		functionkeys_o	=> FKeys_s,
		core_reload_o	=> open,
		keymap_addr_i	=> keymap_addr_s,
		keymap_data_i	=> keymap_data_s,
		keymap_we_i		=> keymap_we_s
	);


	mousectrl: ps2mouse 
	port map
	(
		clk 		=> membrane_div_q(4), -- need a slower clock to avoid loosing data
		reset 	=> FKeys_s(12), 		--reset_s,		   	-- EXPERIMENTAL: reset on F12 
		--
		ps2mdat_i 	=> m_data_in_s,	-- mouse PS/2 data (in)
		ps2mclk_i 	=> m_clock_in_s,	-- mouse PS/2 clk (in)
		
		ps2mdat_o 	=> m_data_out_s,	-- mouse PS/2 data (out)
		ps2mclk_o 	=> m_clock_out_s,	-- mouse PS/2 clk (out)
		--
		xcount	=> mouse_x_s, 			-- mouse X counter		
		ycount	=> mouse_y_s, 			-- mouse Y counter
		zcount	=> mouse_wheel_s, 	-- mouse Z counter
		mleft		=> mouse_bts_s(0),	-- left mouse button output
		mright	=> mouse_bts_s(1),	-- right mouse button output
		mthird	=> mouse_bts_s(2),	-- third(middle) mouse button output
		--
		sof 		=> '0',					-- mouse joystick emulation enable bit
		mou_emu 	=> (others=>'0'), 	-- mouse with joystick input
		--
		test_load=> '0',					-- load test value to mouse counter
		test_data=> (others=>'0')		-- mouse counter test value
  );

	-- DivMMC button
	db2: entity work.debounce
	port map (
		clk_i				=> clock_28_s,
		button_i			=> btn_divmmc_n_i,
		result_o			=> btn_divmmc_db_n_s
	);

	-- Multiface button
	db3: entity work.debounce
	port map (
		clk_i				=> clock_28_s,
		button_i			=> btn_multiface_n_i,
		result_o			=> btn_mf_db_n_s
	);
	
	btnrst: entity work.debounce
	port map 
	(
		clk_i				=> clock_14_s,
		button_i			=> btn_reset_n_i,
		result_o			=> btn_reset_db_s
	);
	
	-- Keyboard
--	kb_mes_cols_s	<= kb_int_cols_s and keyb_col_i and bus_data_io(4 downto 0) when port254_rd_s = '1' else  kb_int_cols_s and keyb_col_i ;
	kb_mes_cols_s	<= kb_int_cols_s and membrane_col_s and bus_data_io(4 downto 0) when port254_rd_s = '1' else  kb_int_cols_s and membrane_col_s ;

	-- Emulate fnkeys
	emufnk: entity work.emu_fnkeys
	generic map (
		clkfreq_g		=> 28000
	)
	port map (
		clock_i				=> clock_28_s,
		rows_i				=> kb_int_rows_s,
		rows_filter_o		=> kb_rows_s,
		cols_i				=> kb_mes_cols_s,
		cols_filter_o		=> kb_columns_s,
		btn_mf_n_i			=> btn_mf_db_n_s,
		btn_mf_filter_n_o	=> btn_mf_filter_n_s,
		btn_reset_n_i		=> btn_reset_db_s,
		fnkeys_o				=> emu_fnkeys_s
	);
	
	-- Geracao dos clocks
	process(clock_3m5_s)
	begin
		if rising_edge(clock_3m5_s) then 
			membrane_div_q <= membrane_div_q + 1;
		end if;
	end process;

	
	membrane : entity work.membrane
	port map
	(
		clock_i		=> membrane_div_q(4) , --clock_cpu_s,
		reset_i		=> reset_s,
		rows_i		=> kb_rows_s, 

		keyb5			=> keyb_col_i,

		-- Out
		cols_o		=> membrane_col_s,
		keyb8_o		=> keyb_row_o

	);

	-----------------------------------------------------------------
	-- video scan converter required to display video on VGA hardware
	-----------------------------------------------------------------
	-- take note: the values below are relative to the CLK period not standard VGA clock period
	scandbl: entity work.scan_convert
	generic map (
		-- mark active area of input video
		cstart		=>  38*2,  -- composite sync start
		clength		=> 352*2,  -- composite sync length
		-- output video timing
		hB				=>  32*2,	-- h sync
		hC				=>  40*2,	-- h back porch
		hD				=> 352*2,	-- visible video
		vB				=>   2*2,	-- v sync
		vC				=>   5*2,	-- v back porch
		vD				=> 284*2,	-- visible video
		hpad			=>   0*2,	-- create H black border
		vpad			=>   0*2		-- create V black border
	)
	port map (
		CLK			=> clock_14_s,
		CLK_x2		=> clock_28_s,
		--
		hA				=> ha_value_s,	-- h front porch
		I_VIDEO		=> rgb_ula_s,
		I_HSYNC		=> rgb_hs_n_s,
		I_VSYNC		=> rgb_vs_n_s,
		I_SCANLIN	=> scanlines_s,
		I_BLANK_N	=> hblank_s and vblank_s,
		--
		O_VIDEO_15	=> rgb_15_s,		-- scanlines processed
		O_VIDEO_31	=> rgb_31_s,		-- scanlines processed
		O_HSYNC		=> hsync_out_s,
		O_VSYNC		=> vsync_out_s,
		O_BLANK		=> blank_s
	);
	ha_value_s <= 24*2 when mach_timing_s(1) = '0' else 32*2;		-- ZX 48K = 00 or 01, ZX128K = 10 or 11

	-- Boot ROM
	boot_rom: entity work.bootrom
	port map (
		clk		=> clock_28_s,
		addr		=> rom_addr_s(12 downto 0),
		data		=> rom_data_s
	);

	-- Multiboot
	mb: entity work.flashboot
	port map (
		reset_i		=> poweron_s,
		clock_i		=> clock_14_s,
		start_i		=> core_reload_s,
		spiaddr_i	=> X"6B080000"
	);

	-- Glue logic
	-- Power-on counter
	process (clock_28_s)
	begin
		if rising_edge(clock_28_s) then
			if poweron_cnt_s /= 0 then
				poweron_cnt_s <= poweron_cnt_s - 1;
			end if;
		end if;
	end process;

	-- Resets
	poweron_s		<= '1' when poweron_cnt_s /= 0															else '0';
	hard_reset_s	<= '1' when FKeys_s(1) = '1' or emu_fnkeys_s(1) = '1'								else '0';
	
	soft_reset_s	<= '1' when FKeys_s(4) = '1' or bus_rst_n_io = '0' or btn_reset_db_s = '0'	else '0';
	
	reset_s			<= poweron_s or hard_reset_s or soft_reset_s;

	bus_rst_n_io	<= '0' when int_soft_reset_s = '1' else 'Z';

	keys_hard_s			<= (not btn_divmmc_db_n_s) & (not btn_mf_db_n_s);

	-- SD and Flash
	sd_mosi_o		<= spi_mosi_s;
	sd_sclk_o		<= spi_sclk_s;
	sd_cs0_n_o		<= sd_cs0_n_s;
	sd_cs1_n_o		<= sd_cs1_n_s;
	flash_mosi_o	<= spi_mosi_s;
	flash_sclk_o	<= spi_sclk_s;
	flash_wp_o		<= '0';
	flash_hold_o	<=	'1';

	-- Audios
	audioint_o	<= spk_s;
	mic_port_o	<= mic_s;

	-- Joystick
	-- order: Fire2, Fire, Up, Down, Left, Right
	joy1_s	<= (others => '0');
	joy2_s	<= (others => '0');


	-- Open-collector
	process (kb_rows_s)
	begin
		keyb_row_o <= (others => 'Z');
		for I in kb_rows_s'low to kb_rows_s'high loop
			if kb_rows_s(I) = '0' then
				keyb_row_o(I) <= '0';
			end if;
		end loop;
	end process;

	-- VGA
	rgb_ula_s <= rgb_r_s & rgb_g_s & rgb_b_s;

	rgb_r_o	<= rgb_31_s(8 downto 6)		when scandbl_en_s = '1'		else rgb_15_s(8 downto 6);
	rgb_g_o	<= rgb_31_s(5 downto 3)		when scandbl_en_s = '1'		else rgb_15_s(5 downto 3);
	rgb_b_o	<= rgb_31_s(2 downto 0) 	when scandbl_en_s = '1'		else rgb_15_s(2 downto 0);
	hsync_o	<= hsync_out_s					when scandbl_en_s = '1'		else rgb_hs_n_s;
	vsync_o	<= vsync_out_s					when scandbl_en_s = '1'		else rgb_vs_n_s;
	csync_o	<= rgb_cs_n_s;

		process (ula50_60_s)
	begin
		if ula50_60_s = '0' then --50hz

			h_visible_s		<= 720 - 1;
			hsync_start_s	<= 732 - 1;
			hsync_end_s		<= 796 - 1;
			hcnt_end_s		<= 864 - 1;
			--
			v_visible_s		<= 576 - 1;
			vsync_start_s	<= 581 - 1;
			vsync_end_s		<= 586 - 1;
			vcnt_end_s		<= 625 - 2;
			
		else -- 60hz
		
			h_visible_s		<= 720 - 1;
			hsync_start_s	<= 736 - 1;
			hsync_end_s		<= 798 - 1;
			hcnt_end_s		<= 858 - 1;
			--
			v_visible_s		<= 480 - 1;
			vsync_start_s	<= 489 - 1;
			vsync_end_s		<= 495 - 1;
			vcnt_end_s		<= 525 - 2;
			
		end if;
	end process;
	
	-- HDMI	
	hdmi_frame: entity work.hdmi_frame 
	port map (
	
		clock_i 		=> clock_14_s, --14mhz
		clock2x_i 	=> clock_28_s, --28mhz
		reset_i		=> reset_s,
		scanlines_i	=> scanlines_s,
		rgb_i			=> rgb_ula_s,
		hsync_i 		=> rgb_hs_n_s,
		vsync_i 		=> rgb_vs_n_s,
		hblank_n_i	=> hblank_s,
		vblank_n_i	=> vblank_s,
		timing_i		=> mach_video_timing_s,
		
		--outputs
		rgb_o 		=> toHDMI_rgb_s,
		hsync_o 		=> toHDMI_hsync_s,
		vsync_o 		=> toHDMI_vsync_s,
		blank_o		=> toHDMI_blank_s,
		
		-- config values 
		h_visible	=> h_visible_s,
		hsync_start	=> hsync_start_s,
		hsync_end	=> hsync_end_s,
		hcnt_end		=> hcnt_end_s,
		--
		v_visible	=> v_visible_s,
		vsync_start	=> vsync_start_s,
		vsync_end	=> vsync_end_s,
		vcnt_end		=> vcnt_end_s
	);
	

	hdmi: entity work.hdmi
	generic map (
		FREQ	=> 27000000,	-- pixel clock frequency
		FS		=> 48000,		-- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
		CTS	=> 27000,		-- CTS = Freq(pixclk) * N / (128 * Fs)
		N		=> 6144			-- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
	)
	port map (
		I_CLK_PIXEL		=> clock_28_s,
		I_R				=> toHDMI_rgb_s(8 downto 6) & toHDMI_rgb_s(8 downto 6) & toHDMI_rgb_s(8 downto 7),
		I_G				=> toHDMI_rgb_s(5 downto 3) & toHDMI_rgb_s(5 downto 3) & toHDMI_rgb_s(5 downto 4),
		I_B				=> toHDMI_rgb_s(2 downto 0) & toHDMI_rgb_s(2 downto 0) & toHDMI_rgb_s(2 downto 1),
		I_BLANK			=> toHDMI_blank_s,
		I_HSYNC			=> toHDMI_hsync_s,
		I_VSYNC			=> toHDMI_vsync_s,
		-- PCM audio
		I_AUDIO_ENABLE	=> '1',
		I_AUDIO_PCM_L 	=> pcm_out_L_s & "00",
		I_AUDIO_PCM_R	=> pcm_out_R_s & "00",
		-- TMDS parallel pixel synchronous outputs (serialize LSB first)
		O_RED				=> tdms_r_s,
		O_GREEN			=> tdms_g_s,
		O_BLUE			=> tdms_b_s
	);

	hdmio: entity work.hdmi_out_xilinx
	port map (
		clock_pixel_i		=> clock_28_s,
		clock_tdms_i		=> clock_hdmi_s,
		clock_tdms_n_i		=> clock_hdmi_n_s,
		red_i					=> tdms_r_s,
		green_i				=> tdms_g_s,
		blue_i				=> tdms_b_s,
		tmds_out_p			=> hdmi_p_o,
		tmds_out_n			=> hdmi_n_o
	);


	-- BUS
	bus_addr_o			<= bus_addr_s;
	bus_iorq_n_o		<= bus_iorq_n_s;
	bus_m1_n_o			<= bus_m1_n_s;
	bus_mreq_n_o		<= bus_mreq_n_s;
	bus_rd_n_o			<= bus_rd_n_s;
	bus_wr_n_o			<= bus_wr_n_s;
	bus_datai_s			<= bus_data_io;
	bus_data_io			<= (others => 'Z') when bus_rd_n_s = '0' or port254_rd_s = '1' else bus_datao_s;





	-- BUS
	bus_addr_o			<= bus_addr_s;
	bus_iorq_n_o		<= bus_iorq_n_s;
	bus_m1_n_o			<= bus_m1_n_s;
	bus_mreq_n_o		<= bus_mreq_n_s;
	bus_rd_n_o			<= bus_rd_n_s;
	bus_wr_n_o			<= bus_wr_n_s;
	bus_datai_s			<= bus_data_io;
	bus_data_io			<= (others => 'Z') when bus_rd_n_s = '0' or port254_rd_s = '1' else bus_datao_s;


	--------------------------------------------------
	
	accel_io_3  <= 'Z';
	accel_io_2 <= 'Z';
	accel_io_6  <= 'Z';
	accel_io_5  <= 'Z';
	accel_io_4  <= 'Z';
	accel_io_1  <= 'Z';
	accel_io_0  <= 'Z';
	accel_io_23 <= 'Z';
	accel_io_22 <= 'Z';
	accel_io_21 <= 'Z';
	accel_io_20 <= 'Z';
	accel_io_19 <= 'Z';
	accel_io_18 <= 'Z';
	accel_io_17 <= 'Z';
	accel_io_16 <= 'Z';
	accel_io_15 <= 'Z';
	accel_io_14 <= 'Z';
	accel_io_13 <= 'Z';
	accel_io_12 <= 'Z';
	accel_io_24 <= 'Z';
	accel_io_25 <= 'Z';
	accel_io_26 <= 'Z';
	accel_io_27 <= 'Z';

end architecture;
