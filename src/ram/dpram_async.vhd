
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_unsigned.all;

entity dpram_async is
    port 
	 (
	 
			address_0 :in    	std_logic_vector (20 downto 0);  	-- address_0 Input
			data_i_0  :in 		std_logic_vector (7 downto 0);  		-- data_0 in
			data_o_0  :out 	std_logic_vector (7 downto 0);  		-- data_0 out
			cs_0      :in    	std_logic;                          -- Chip Select
			we_0      :in    	std_logic;                          -- Write Enable/Read Enable
			oe_0      :in    	std_logic;                          -- Output Enable
			
			address_1 :in    	std_logic_vector (20 downto 0); 		-- address_1 Input
			data_i_1  :in 		std_logic_vector (7 downto 0);  		-- data_1 in
			data_o_1  :out 	std_logic_vector (7 downto 0);  		-- data_1 out
			cs_1      :in    	std_logic;                          -- Chip Select
			we_1      :in    	std_logic;                          -- Write Enable/Read Enable
			oe_1      :in    	std_logic;                          -- Output Enable
		
			-- Output to SRAM
			sram_addr_o		: out   std_logic_vector(18 downto 0);
			sram_data_io	: inout std_logic_vector(7 downto 0);
			sram_ce_n_o		: out   std_logic_vector(3 downto 0)	:= "1111";
			sram_oe_n_o		: out   std_logic								:= '1';
			sram_we_n_o		: out   std_logic								:= '1'
			
    );
end entity;
architecture rtl of dpram_async is

	signal sram_a_s	: std_logic_vector(18 downto 0);
	signal sram_d_s	: std_logic_vector(7 downto 0);
	signal sram_we_s	: std_logic;
	signal sram_oe_n_s	: std_logic;
	
begin

	sram_addr_o		<= sram_a_s(18 downto 0);
	sram_oe_n_o		<= sram_oe_n_s;
	sram_we_n_o		<= sram_we_s;

	sram_data_io	<= sram_d_s when sram_we_s = '0' else (others=>'Z');

    sram_ce_n_o <= 	"1110" when sram_addr_o(20 downto 19) = "00" else
					"1101" when sram_addr_o(20 downto 19) = "01" else
					"1011" when sram_addr_o(20 downto 19) = "10" else
					"0111";

    process (oe_0, cs_0, we_0, oe_1, cs_1, we_1, address_0, data_i_0, address_1, data_i_1, sram_a_s, sram_data_io) begin


			-- Write
			if (cs_0 = '1' and we_0 = '1') then
			
					sram_a_s <= address_0(18 downto 0);
					sram_d_s <= data_i_0;
					sram_we_s <= '0';
				
			elsif  (cs_1 = '1' and we_1 = '1') then
				
					sram_a_s <= address_1(18 downto 0);
					sram_d_s <= data_i_1;
					sram_we_s <= '0';

			else
			
					sram_we_s <= '1';
				
			end if;
		  

			-- Read
			if (cs_0 = '1' and we_0 = '0' and oe_0 = '1') then
			
				sram_a_s <= address_0(18 downto 0);
				sram_oe_n_s <= '0';
				sram_we_s <= '1';
							
				data_o_0	<= sram_data_io;

					
			elsif (cs_1 = '1' and we_1 = '0' and oe_1 = '1') then
		  
				sram_a_s <= address_1(18 downto 0);
				sram_oe_n_s <= '0';
				sram_we_s <= '1';
					
				data_o_1	<= sram_data_io;
				
			else
		  
				sram_oe_n_s <= '1';
				sram_we_s <= '1';
				sram_ce_n_o <= "1111";
				
			end if;
		  
    end process;

end architecture;