--
-- TBBlue / ZX Spectrum Next project
-- Copyright (c) 2015 - Fabio Belavenuto & Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
-- ID 6 = VTrucco
--
-- EP2C5T144C8 = 86 free pins
--

-- altera message_off 10540 10541

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Ligar /CE da SRAM no GND
--
-- RGB:
--
-- 7 - R (MSB) - Ligar resistor de  500 ohms (510)
-- 6 - R       - Ligar resistor de 1000 ohms
-- 5 - R (LSB) - Ligar resistor de 2000 ohms (2200)

-- 4 - G (MSB) - Ligar resistor de  500 ohms (510)
-- 3 - G       - Ligar resistor de 1000 ohms
-- 2 - G (LSB) - Ligar resistor de 2000 ohms (2200)

-- 1 - B (MSB) - Ligar resistor de  500 ohms (510)
-- 0 - B (LSB) - Ligar resistor de  666 ohms (680)
--


entity vtrucco_top is
	generic (

		machine_id_g	   : unsigned(7 downto 0)	:= X"0A";	-- 10 = ZX Spectrum Next
		version_g			: unsigned(7 downto 0)	:= X"1A";	-- 1.10
		sub_version_g		: unsigned(7 downto 0)	:= X"0b"		-- .11
	);
	port (
		-- Clocks
		clock_50				: in    std_logic;								-- Entrada 50 MHz

		-- VGA
		vga_rgb				: out   std_logic_vector(7 downto 0);		-- Saida RRRGGGBB
		vga_hsync			: out   std_logic;								-- H-Sync
		vga_vsync			: out   std_logic;								-- V-Sync

		-- Audio DAC TDA1543
		dac_bclk				: out   std_logic;
		dac_ws				: out   std_logic;
		dac_dout				: out   std_logic;

		-- SRAM (AS7C34096)
		sram_addr			: out   std_logic_vector(18 downto 0);		-- 19 bits = 512K
		sram_data			: inout std_logic_vector(7 downto 0);
		sram_oe_n			: out   std_logic;								-- SRAM /OE
		sram_we_n			: out   std_logic;								-- SRAM /WE

		-- PS2
		ps2_clk				: inout std_logic;								-- Teclado PS/2
		ps2_data				: inout std_logic;								-- Teclado PS/2

		-- Cassete port
		ear_key				: out    std_logic;								-- interrupcao para o teclado
		mic_port				: out   std_logic;								-- Saida MIC

		-- SD Card
		spi_cs0				: out   std_logic;								-- /CS do cartao SD
		spi_sclk				: out   std_logic;								-- Clock do cartao SD
		spi_miso				: in    std_logic;								-- Entrada serial vinda do cartao SD
		spi_mosi				: out   std_logic;								-- Sai­da serial para o cara£o SD

		-- CPU
		CPU_A					: out   std_logic_vector(15 downto 0);
		CPU_D					: inout std_logic_vector(7 downto 0);
		CPU_MREQ				: out   std_logic;
		CPU_IORQ				: out   std_logic;
		CPU_RD				: out   std_logic;
		CPU_WR				: out   std_logic;
		CPU_M1				: out   std_logic;

		-- Entradas
		CPU_RST				: in    std_logic;
		CPU_INT				: in    std_logic;
		CPU_NMI				: in    std_logic;
		CPU_ROMCS			: in    std_logic;

		NMI_MULTIFACE		: in    std_logic;
		NMI_DIVMMC			: in    std_logic;

		LightPen				: in    std_logic

	);
end entity;

architecture behavior of vtrucco_top is

	-- ASMI (Altera specific component)
	component cyclone_asmiblock
	port (
		dclkin      : in    std_logic;      -- DCLK
		scein       : in    std_logic;      -- nCSO
		sdoin       : in    std_logic;      -- ASDO
		oe          : in    std_logic;      --(1=disable(Hi-Z))
		data0out    : out   std_logic       -- DATA0
	);
	end component;
	
	component ps2mouse 
	port
	(
		clk 		: in std_logic;		    					-- bus clock
		reset 	: in std_logic;			   				-- reset 
		ps2mdat 	: inout std_logic;			 				-- mouse PS/2 data
		ps2mclk 	: inout std_logic;			 				-- mouse PS/2 clk
		mou_emu 	: in std_logic_vector(5 downto 0); 		-- mouse with joystick input
		sof 		: in std_logic;								-- mouse joystick emulation enable bit
		zcount	: out std_logic_vector(7 downto 0);  	-- mouse Z counter
		ycount	: out std_logic_vector(7 downto 0);		-- mouse Y counter
		xcount	: out std_logic_vector(7 downto 0);		-- mouse X counter
		mleft		: out std_logic;								-- left mouse button output
		mthird	: out std_logic;								-- third(middle) mouse button output
		mright	: out std_logic;								-- right mouse button output
		test_load: in std_logic;								-- load test value to mouse counter
		test_data: in std_logic_vector(15 downto 0)		-- mouse counter test value
  );
  end component;

	-- Master clock
	signal clock_28_s			: std_logic;
	signal clock_28_180o_s	: std_logic;
	signal clock_14_s			: std_logic;
	signal clock_7_s			: std_logic;
	signal clock_3m5_s		: std_logic;
	signal clock_psg_s		: std_logic;
	signal clock_cpu_s		: std_logic;
	signal clock_div_q		: unsigned(3 downto 0) 				:= (others => '0');	
	signal cpu_clock_cont_s	: std_logic;	
	signal clock_turbo_s		: std_logic;
	signal turbo_s				: std_logic_vector(1 downto 0);
	signal pll_locked			: std_logic;					-- PLL travado quando 1

	-- Resets
	signal poweron_s			: std_logic;
	signal hard_reset_s		: std_logic;
	signal soft_reset_s		: std_logic;
	signal int_soft_reset_s	: std_logic;
	signal reset_s				: std_logic;
	signal s_db_reset			: std_logic;
	signal s_db_divmmc_n		: std_logic;
	signal s_db_m1_n			: std_logic;

	-- Memory buses
	signal vram_a				: std_logic_vector(20 downto 0);
	signal vram_dout			: std_logic_vector( 7 downto 0);
	signal vram_cs				: std_logic;
	signal vram_oe				: std_logic;
	signal ram_a				: std_logic_vector(20 downto 0);
	signal ram_din				: std_logic_vector( 7 downto 0);
	signal ram_dout			: std_logic_vector( 7 downto 0);
	signal ram_cs				: std_logic;
	signal ram_oe				: std_logic;
	signal ram_we				: std_logic;
	signal rom_a				: std_logic_vector(13 downto 0);		-- 16K
	signal rom_dout			: std_logic_vector( 7 downto 0);

	-- Audio
	signal s_ear				: std_logic;
	signal s_spk				: std_logic;
	signal s_mic				: std_logic;
	signal s_psg_L				: unsigned( 9 downto 0);
	signal s_psg_R				: unsigned( 9 downto 0);	
	signal s_covox_L			: unsigned( 8 downto 0);
	signal s_covox_R			: unsigned( 8 downto 0);	
	signal s_dac				: std_logic 							:= '0'; -- 0 = I2S, 1 = JAP

	-- Keyboard
	signal kb_rows				: std_logic_vector( 7 downto 0);
	signal kb_columns			: std_logic_vector( 4 downto 0);
	signal FKeys_s				: std_logic_vector(12 downto 1);
	signal port254_cs_s		: std_logic;
	signal s_ear_key			: std_logic;
	signal s_ps2				: std_logic;
	signal s_columns			: std_logic_vector( 4 downto 0);
	signal keymap_addr_s		: std_logic_vector( 8 downto 0);
	signal keymap_data_s		: std_logic_vector( 8 downto 0);
	signal keymap_we_s		: std_logic;

	-- SPI e EPCS
	signal spi_mosi_s			: std_logic;
	signal spi_sclk_s			: std_logic;
	signal flash_miso_s		: std_logic;
	signal flash_cs_n_s		: std_logic;
	signal spi_cs_n			: std_logic			:= '1';

	-- Video and scandoubler
	signal rgb_r				: std_logic_vector( 2 downto 0);
	signal rgb_g				: std_logic_vector( 2 downto 0);
	signal rgb_b				: std_logic_vector( 2 downto 0);
	signal rgb_i				: std_logic;
	signal rgb_hs_n			: std_logic;
	signal rgb_vs_n			: std_logic;
	signal ulap_en				: std_logic;
	signal rgb_ulap			: std_logic_vector( 8 downto 0);
	signal rgb_comb			: std_logic_vector( 8 downto 0);
	signal rgb_out				: std_logic_vector( 8 downto 0);
	signal scandbl_en			: std_logic;
	signal scandbl_sl			: std_logic;
	signal hsync_out			: std_logic;
	signal vsync_out			: std_logic;
	signal s_scanlines		: std_logic := '0';

	-- Mouse
	signal mouse_x				: std_logic_vector( 7 downto 0);
	signal mouse_y				: std_logic_vector( 7 downto 0);
	signal mouse_bts			: std_logic_vector( 2 downto 0);
	signal mouse_wheel		: std_logic_vector( 3 downto 0);

	-- Sinais da CPU
	signal s_cpu_a				: std_logic_vector(15 downto 0);
	signal s_cpu_do			: std_logic_vector( 7 downto 0);
	signal s_cpu_di			: std_logic_vector( 7 downto 0);
	signal s_cpu_iorq			: std_logic;
	signal s_cpu_mreq			: std_logic;
	signal s_cpu_rd			: std_logic;
	signal s_cpu_wr			: std_logic;
	signal s_cpu_m1			: std_logic;
	
--	signal counter				: unsigned(1 downto 0);				-- Contador para dividir o clock master

begin

	--------------------------------
	-- PLL
	--  50 MHz input
	--  28 MHz master clock output
	--------------------------------
	pll: entity work.pll1
	port map (
		inclk0	=> clock_50,
		c0			=> clock_28_s,
--		c1			=> clock_sram,
		locked	=> pll_locked
	);
	
	-- Geracao dos clocks
	process(clock_28_s)
	begin
		if rising_edge(clock_28_s) then 
			clock_div_q <= clock_div_q + 1;
		end if;
	end process;
	

	clock_28_180o_s <= not clock_28_s;
	clock_14_s <= clock_div_q(0); 
	clock_7_s <= clock_div_q(1); 
	clock_3m5_s <= clock_div_q(2); 
	clock_psg_s	<= '1' when clock_div_q(3 downto 0) = "1110" else '0';
	
	clock_turbo_s	<= clock_7_s	when turbo_s = "00"		else --for 3.5
							clock_14_s	when turbo_s = "01"		else --for 7
							clock_28_s; -- for 14
	
	process (clock_turbo_s)
	begin
		if rising_edge(clock_turbo_s) then
			if clock_cpu_s = '1' and Cpu_clock_cont_s = '0' then		-- if there's no contention, the clock can go low
				clock_cpu_s <= '0';
			else
				clock_cpu_s <= '1';
			end if;
		end if;
	end process;

	tbblue1 : entity work.tbblue
	generic map (
		use_turbo		=> true,
		machine_id_g	=> machine_id_g,
		version_g		=> version_g,
		sub_version_g	=> sub_version_g,
		usar_kempjoy	=> '0',
		usar_keyjoy		=> '0',
		use_layer2_g	=> false,
		use_sprites_g	=> false,
		use_turbosnd_g	=> false,
		use_sid_g		=> false,
		use_z80n_g		=> false,
		use_dma_g		=> false,
		use_lightpen_g => true,
		use_UART_g		=> false,
		use_loRes_g 	=> false,
		use_covox_g	 	=> false,
		use_copper_g	=> false
	)
	port map (
		-- Clock
		iClk_28				=> clock_28_s,
		iClk_28_180o		=> clock_28_180o_s,
		iClk_14				=> clock_14_s,
		iClk_7				=> clock_7_s,
		iClk_3M5				=> clock_3m5_s,				-- 3.5 MHz no contencion
		iClk_Cpu				=> clock_cpu_s,				-- CPU clock with contention
		iClk_Psg				=> clock_psg_s,				-- psg clock
		oCpu_clock_cont	=>	Cpu_clock_cont_s,			-- Signal to CPU clock contention
		oTurbo_sel			=> turbo_s,  					-- Selected turbo Speed

		-- Reset
		iPowerOn				=> poweron_s,
		iHardReset			=> hard_reset_s,
		iSoftReset			=> soft_reset_s,
		oSoftReset			=> int_soft_reset_s,

		-- Keys
		iKey50_60hz			=> FKeys_s(3),
		iKeyScanDoubler	=> FKeys_s(2),
		iKeyScanlines		=> FKeys_s(7),
		iKeyDivMMC			=> FKeys_s(10) or not s_db_divmmc_n,
		iKeyMF				=> FKeys_s(9)  or not s_db_m1_n,
		iKeyTurbo			=> FKeys_s(8),
		iKeysHard			=> (others => '0'),

		-- Keyboard
		oRows					=> kb_rows,
		iColumns				=> s_columns,
		oPort254_cs			=> port254_cs_s,
		oKeymap_addr		=> keymap_addr_s,
		oKeymap_data		=> keymap_data_s,
		oKeymap_we			=> keymap_we_s,

		-- RGB
		oRGB_r				=> rgb_r,
		oRGB_g				=> rgb_g,
		oRGB_b				=> rgb_b,
		oRGB_hs_n			=> rgb_hs_n,
		oRGB_vs_n			=> rgb_vs_n,
		oRGB_cs_n			=> open,
		oRGB_hb_n			=> open,
		oRGB_vb_n			=> open,
		oScandbl_en			=> scandbl_en,
		oScandbl_sl			=> s_scanlines,
		oMachTiming			=> open,
		oNTSC_PAL			=> open,

		-- VRAM
		oVram_a				=> vram_a,
		iVram_dout			=> vram_dout,
		oVram_cs				=> vram_cs,
		oVram_rd				=> vram_oe,

		-- Bootrom
		oBootrom_en			=> open,
		oRom_a				=> rom_a,
		iRom_dout			=> rom_dout,
		oFlashboot			=> open,

		-- RAM
		oRam_a				=> ram_a,
		oRam_din				=> ram_din,
		iRam_dout			=> ram_dout,
		oRam_cs				=> ram_cs,
		oRam_rd				=> ram_oe,
		oRam_wr				=> ram_we,

		-- SPI (SD and Flash)
		oSpi_mosi			=> spi_mosi_s,
		oSpi_sclk			=> spi_sclk_s,
		oSD_cs0_n			=> spi_cs0,
		oSD_cs1_n			=> open,
		iSD_miso				=> spi_miso,
		oFlash_cs_n			=> flash_cs_n_s,
		iFlash_miso			=> flash_miso_s,

		-- Sound
		iEAR					=>	s_ear,
		oSPK					=>	s_spk,
		oMIC					=>	s_mic,
		oPSG_L				=>	s_psg_L,
		oPSG_R				=>	s_psg_R,
		oSID_L				=> open,
		oSID_R				=> open,
		oCovox_L				=>	s_covox_L,
		oCovox_R				=>	s_covox_R,
		oDAC					=> s_dac,

		-- Joystick
		-- order: Fire2, Fire, Up, Down, Left, Right
		iJoy0					=> (OTHERS => '0'),
		iJoy1					=> (OTHERS => '0'),

		-- Mouse
		iMouse_en			=> s_ps2,
		iMouse_x				=>	mouse_x,
		iMouse_y				=>	mouse_y,
		iMouse_bts			=>	mouse_bts,
		iMouse_wheel		=>	mouse_wheel,
		oPS2mode				=> s_ps2,

		-- Lightpen
		iLp_signal			=> LightPen,
		oLp_en				=> open,

		-- RTC
		ioRTC_sda			=> 'Z',
		ioRTC_scl			=> 'Z',

		-- Serial
		iRs232_rx			=> '0',
		oRs232_tx			=> open,
		iRs232_dtr			=> '0',
		oRs232_cts			=> open,

		-- BUS
		oCpu_a				=> s_cpu_a,
		oCpu_do				=> s_cpu_do,
		iCpu_di				=> s_cpu_di,
		oCpu_mreq			=> s_cpu_mreq,
		oCpu_ioreq			=> s_cpu_iorq,
		oCpu_rd				=> s_cpu_rd,
		oCpu_wr				=> s_cpu_wr,
		oCpu_m1				=> s_cpu_m1,
		iCpu_Wait_n			=> '1',
		iCpu_nmi				=> CPU_NMI,
		iCpu_int_n			=> '1',
		iCpu_romcs			=> CPU_ROMCS,
		iCpu_ramcs			=> '0',
		iCpu_busreq_n		=> '1',
		oCpu_busack_n		=> open,
		oCpu_clock			=> open,
		oCpu_halt_n			=> open,
		oCpu_rfsh_n			=> open,
		iCpu_iorqula		=> '0',

		-- Layer2
		oLayer2_addr		=> open,
		iLayer2_data		=> (others => '0'),
		oLayer2_cs		=> open,

		-- Debug
		oD_leds				=> open,
		oD_reg_o				=> open,
		oD_others			=> open,
		iSW					=> (others=>'0')
	);

	-- SRAM AS7C34096-12 (-15)
	ram : entity work.dpSRAM_5128
	port map(
		clk				=> clock_28_s,
		-- Porta 0 = VRAM
		porta0_addr		=> vram_a(18 downto 0),
		porta0_ce		=> vram_cs,
		porta0_oe		=> vram_oe,
		porta0_we		=> '0',
		porta0_din		=> (others => '0'),
		porta0_dout		=> vram_dout,
		-- Porta 1 = Upper RAM
		porta1_addr		=> ram_a(18 downto 0),
		porta1_ce		=> ram_cs,
		porta1_oe		=> ram_oe,
		porta1_we		=> ram_we,
		porta1_din		=> ram_din,
		porta1_dout		=> ram_dout,
		-- Outputs to SRAM on board
		sram_addr		=> sram_addr,
		sram_data		=> sram_data,
		sram_ce_n		=> open,
		sram_oe_n		=> sram_oe_n,
		sram_we_n		=> sram_we_n
	);

	--
	sound: entity work.Audio_TDA1543
	port map (
		clock			=> clock_7_s, --clock 7Mhz
		ear			=> s_ear,
		spk			=> s_spk,
		mic			=>	s_mic,
		psg_L			=>	std_logic_vector(s_psg_L(7 downto 0)),
		psg_R			=>	std_logic_vector(s_psg_R(7 downto 0)),
		covox_L		=>	std_logic_vector(s_covox_L),
		covox_R		=>	std_logic_vector(s_covox_R),
		i2s_bclk		=> dac_bclk,
		i2s_ws		=> dac_ws,
		i2s_data		=> dac_dout,
		format		=> s_dac
	);

	-- PS/2 emulating speccy keyboard
	kb: entity work.ps2keyb
	port map 
	(
	
		enable_i			=> not s_ps2,
		use_ps2_alt_i	=> '0',	
		clock_i			=> clock_28_s,
		clock_180o_i	=> clock_28_180o_s,
		clock_ps2_i		=> clock_div_q(3),
		reset_i			=> poweron_s,
	
		--
		ps2_clk_io		=> ps2_clk,
		ps2_data_io		=> ps2_data,
		--
		rows_i			=> kb_rows,
		cols_o			=> kb_columns,
		functionkeys_o	=> FKeys_s,
		core_reload_o	=> open,
		keymap_addr_i	=> keymap_addr_s,
		keymap_data_i	=> keymap_data_s,
		keymap_we_i		=> keymap_we_s
	);

	-- debounce para botao de reset
	db: entity work.debounce
	port map (
		clk_i				=> clock_28_s,
		button_i			=> CPU_RST,				-- input signal to be debounced
		result_o			=> s_db_reset			-- debounced signal
	);

	-- debounce para botao da divmmc
	db_2: entity work.debounce
	port map (
		clk_i				=> clock_28_s,
		button_i			=> NMI_DIVMMC,			-- input signal to be debounced
		result_o			=> s_db_divmmc_n			-- debounced signal
	);

		-- debounce para botao da multiface
	db_3: entity work.debounce
	port map (
		clk_i				=> clock_28_s,
		button_i			=> NMI_MULTIFACE,	-- input signal to be debounced
		result_o			=> s_db_m1_n			-- debounced signal
	);


--	mousectrl : entity work.mouse_ctrl
--	generic map
--	(
--		clkfreq 		=> 28000,
--		SENSIBILITY	=> 1 -- Valores maiores, mais lento
--	)
--	port map
--	(
--		enable		=> s_ps2,				-- 1 para habilitar
--		clock			=> clock_master,
--		reset			=> reset_s,
--		ps2_data		=> ps2_data,
--		ps2_clk		=> ps2_clk,
--		mouse_x 		=> mouse_x,
--		mouse_y		=> mouse_y,
--		mouse_bts	=> mouse_bts,
--		mouse_wheel => mouse_wheel
--	);
	
	mousectrl: ps2mouse 
	port map
	(
		clk 		=> clock_div_q(3), -- need a slower clock to avoid loosing data
		reset 	=> FKeys_s(12), 	--reset_s,		   	-- EXPERIMENTAL: reset on F12 
		ps2mclk 	=> ps2_clk,			-- mouse PS/2 clk
		ps2mdat 	=> ps2_data,		-- mouse PS/2 data
		--
		xcount	=> mouse_x, 		-- mouse X counter		
		ycount	=> mouse_y, 		-- mouse Y counter
		zcount	=> open, 			-- mouse Z counter
		mleft		=> mouse_bts(0),	-- left mouse button output
		mright	=> mouse_bts(1),	-- right mouse button output
		mthird	=> mouse_bts(2),	-- third(middle) mouse button output
		--
		sof 		=> '0',				-- mouse joystick emulation enable bit
		mou_emu 	=> (others=>'0'), -- mouse with joystick input
		--
		test_load=> '0',				-- load test value to mouse counter
		test_data=> (others=>'0')	-- mouse counter test value
  );

	-- Scandoubler with scanlines
	scandbl: entity work.scandoubler
	generic map (
		hSyncLength	=> 61,								-- 29 for 14MHz and 61 for 28MHz
		vSyncLength	=> 13,
		ramBits		=> 11									-- 10 for 14MHz and 11 for 28MHz
	)
	port map(
		clk					=> clock_28_s,			-- minimum 2x pixel clock
		hSyncPolarity		=> '0',
		vSyncPolarity		=> '0',
		enable_in			=> scandbl_en,
		scanlines_in		=> s_scanlines,
		video_in				=> rgb_comb,
		hsync_in				=> rgb_hs_n,
		vsync_in				=> rgb_vs_n,
		video_out			=> rgb_out,
		vsync_out			=> vsync_out,
		hsync_out			=> hsync_out
	);

	-- Boot ROM
	boot_rom: entity work.bootrom
	port map (
		clk		=> clock_28_s,
		addr		=> rom_a(12 downto 0),
		data		=> rom_dout
	);

	-- EPCS4
	epcs4: cyclone_asmiblock
	port map (
		oe          => '0',
		scein       => flash_cs_n_s,
		dclkin      => spi_sclk_s,
		sdoin       => spi_mosi_s,
		data0out    => flash_miso_s
	);

	-- glue
	poweron_s		<= '1' when pll_locked = '0'								else '0';
	hard_reset_s	<= '1' when FKeys_s(1) = '1' 								else '0';
	soft_reset_s	<= '1' when int_soft_reset_s = '1' or FKeys_s(4) = '1' or s_db_reset = '0' 	else '0';
	reset_s			<= poweron_s or hard_reset_s or soft_reset_s;

	-- SD
	spi_mosi	<= spi_mosi_s;
	spi_sclk	<= spi_sclk_s;

	-- VGA (ULA and ULA+ mixer)
	--rgb_comb <=
	--	rgb_r & (rgb_i and rgb_r) & (rgb_i and rgb_r) &
	--	rgb_g & (rgb_i and rgb_g) & (rgb_i and rgb_g) &
	--	rgb_b & (rgb_i and rgb_b)									when ulap_en = '0' else
	--	rgb_ulap;


	rgb_comb <= rgb_r & rgb_g & rgb_b;

	vga_rgb			<= rgb_out(8 downto 1);
	vga_hsync		<= hsync_out;
	vga_vsync		<= vsync_out;

	-- EAR e MIC
	s_ear		<= not CPU_D(6) when s_ear_key = '0' else '0'; -- not ear_port;
	mic_port	<= s_mic;


	-- Sinais da CPU
	CPU_A 	<= s_cpu_a;
	CPU_D  <= (OTHERS=>'Z') when s_cpu_rd = '0' or port254_cs_s = '1' else s_cpu_do; --CPU_D 	<= (OTHERS=>'Z') when s_cpu_rd = '0' else s_cpu_do;
	
	s_cpu_di <= CPU_D;
	CPU_IORQ <= s_cpu_iorq;
	CPU_MREQ <= s_cpu_mreq;
	CPU_RD 	<= s_cpu_rd;
	CPU_WR 	<= s_cpu_wr;
	CPU_M1 	<= s_cpu_m1;

	-- Reads keyboard matrix via BUS
	s_ear_key <= not port254_cs_s;

	-- pino externo para o latch
	ear_key <= s_ear_key;

	-- kb_cols
	s_columns <= kb_columns and CPU_D(4 downto 0) when s_ear_key = '0' else kb_columns;

end architecture;

