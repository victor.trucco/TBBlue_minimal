--
-- TBBlue / ZX Spectrum Next project
--
-- Copper
-- Theorical Model - Mike Dailly 
-- VHDL - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.ALL;

entity copper is
	port (
		clock_i				: in  std_logic;
		reset_n_i			: in  std_logic;
			
		copper_en_i			: in std_logic_vector(1 downto 0) := "00";
		
		copper_contention_i	: in  std_logic;

		hcount_i			: in unsigned(8 downto 0);
		vcount_i			: in unsigned(8 downto 0);
		
		copper_list_addr_o	: out std_logic_vector(9 downto 0);
		copper_list_data_i	: in std_logic_vector(15 downto 0);
		
		copper_dout_o		: out std_logic := '0';
		copper_data_o		: out std_logic_vector(14 downto 0)
		

	);
end entity;

architecture rtl of copper is
	
	constant LAST_ADDRESS : integer := 1023;

	signal copper_list_addr_s 	: std_logic_vector(10 downto 0);
	signal copper_dout_s		: std_logic := '0';
	
	--signal hcount_s			: unsigned(8 downto 0);

	signal last_state_s		: std_logic_vector(1 downto 0) := "00";
	
begin

	--hcount_s <= hcount_i - 12; --ULA delay

	process (clock_i, reset_n_i)
		variable line_v : unsigned(8 downto 0);
	begin
	
		if rising_edge(clock_i) then		
			
			if reset_n_i = '0' then
				
				copper_list_addr_s <= (others=>'0');
				
			else
			
				-- check if we are entering on "01" or "11" mode and reset the address to 0000
				if last_state_s /= copper_en_i then
				
						last_state_s <= copper_en_i;
						
						if copper_en_i = "01" or copper_en_i = "11" then
							copper_list_addr_s <= (others=>'0');
						end if;
					
				else
				
					
			
			
						if copper_en_i = "11" and vcount_i = 0 and hcount_i = 0 then --restart at frame start
							copper_list_addr_s <= (others=>'0');
						end if;
					
						
					
						--if copper_en_i	= "01" and copper_list_addr_s > LAST_ADDRESS then
						--	copper_dout_s <= '0';
										
						--els
						if copper_en_i	/= "00" then
						
								if copper_dout_s = '1' then --if we are on MOVE, clear the output for the next cycle
									copper_dout_s <= '0';
						
								-- command WAIT
								elsif copper_list_data_i(15) = '1' then
								
									if vcount_i = unsigned(copper_list_data_i(8 downto 0)) and hcount_i >= unsigned(copper_list_data_i(14 downto 9)&"000") + 12 then 
										copper_list_addr_s <= copper_list_addr_s + 1; -- if it's time, load the next copper command
									end if;
								
									copper_dout_s <= '0';
									
								else -- command MOVE
								
									copper_data_o <= copper_list_data_i(14 downto 0);
									
									if copper_list_data_i(14 downto 0) /= "00000000" then -- dont generate the write pulse if its a NOP (MOVE 0,0)
										copper_dout_s <= '1';
									end if;
									
									copper_list_addr_s <= copper_list_addr_s + 1;
									
								end if;
										
							--	if copper_list_addr_s > LAST_ADDRESS then
							--			if copper_en_i	= "10" then --wrap around the adress
							--				copper_list_addr_s <= (others=>'0');
							--			end if;
							--	end if;
								
						else
								copper_dout_s <= '0';
						end if;
				

						
				end if;
						
			end if;
		end if;
	end process;
	
	copper_list_addr_o <= copper_list_addr_s(9 downto 0);
	copper_dout_o <= copper_dout_s;

end architecture;