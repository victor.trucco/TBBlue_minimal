--
-- TBBlue / ZX Spectrum Next project
--
-- Membrane state machine - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--

library ieee;
use ieee.std_logic_1164.all;

entity membrane is
port
(
	clock_i		: in    std_logic;
	reset_i		: in    std_logic;
	rows_i		: in    std_logic_vector(7 downto 0);
	keyb5			: in    std_logic_vector(4 downto 0);
	--
	cols_o		: out  std_logic_vector(4 downto 0);
	keyb8_o		: out  std_logic_vector(7 downto 0)
);
end membrane;

architecture membrane_arch of membrane is

	type key_matrix_t is array (7 downto 0) of std_logic_vector(4 downto 0);
	signal matrix_q			: key_matrix_t;
	
	signal 	k1_s, k2_s, k3_s, k4_s,
			k5_s, k6_s, k7_s, k8_s	: std_logic_vector(4 downto 0);

	signal cols_s		:   std_logic_vector(4 downto 0);
	
	signal row_v : integer range 0 to 7;
	signal col_v : integer range 0 to 7;
begin

	-- Matrix
	k1_s <= matrix_q(0) when rows_i(0) = '0' else (others => '1');
	k2_s <= matrix_q(1) when rows_i(1) = '0' else (others => '1');
	k3_s <= matrix_q(2) when rows_i(2) = '0' else (others => '1');
	k4_s <= matrix_q(3) when rows_i(3) = '0' else (others => '1');
	k5_s <= matrix_q(4) when rows_i(4) = '0' else (others => '1');
	k6_s <= matrix_q(5) when rows_i(5) = '0' else (others => '1');
	k7_s <= matrix_q(6) when rows_i(6) = '0' else (others => '1');
	k8_s <= matrix_q(7) when rows_i(7) = '0' else (others => '1');
	cols_o <= k1_s and k2_s and k3_s and k4_s and k5_s and k6_s and k7_s and k8_s;
	
	
	process( reset_i, clock_i )
		type states_t is ( ROW0, ROW1, ROW2, ROW3, ROW4, ROW5, ROW6, ROW7 );
		variable state_v		: states_t := ROW1;
		variable keyb_valid_edge_v	: std_logic_vector(1 downto 0)	:= "00";

		variable caps_v : std_logic;
		variable symb_v : std_logic;
	begin
	
		if reset_i = '1' then

			matrix_q(0) <= (others => '1');
			matrix_q(1) <= (others => '1');
			matrix_q(2) <= (others => '1');
			matrix_q(3) <= (others => '1');
			matrix_q(4) <= (others => '1');
			matrix_q(5) <= (others => '1');
			matrix_q(6) <= (others => '1');
			matrix_q(7) <= (others => '1');


		elsif rising_edge(clock_i) then
		
			keyb8_o <= (others=>'Z');
			
			
			matrix_q(row_v)(0) <= keyb5(0);
			matrix_q(row_v)(1) <= keyb5(1);
			matrix_q(row_v)(2) <= keyb5(2);
			matrix_q(row_v)(3) <= keyb5(3);
			matrix_q(row_v)(4) <= keyb5(4);		
			
			case state_v is

				when ROW0 =>
					row_v <= 0;
					keyb8_o(0) <= '0';
					state_v := ROW1;

				when ROW1 =>
					row_v <= 1;
					keyb8_o(1) <= '0';
					state_v := ROW2;

				when ROW2 =>
					row_v <= 2;
					keyb8_o(2) <= '0';
					state_v := ROW3;

				when ROW3 =>
					row_v <= 3;
					keyb8_o(3) <= '0';
					state_v := ROW4;
					
				when ROW4 =>
					row_v <= 4;
					keyb8_o(4) <= '0';
					state_v := ROW5;

				when ROW5 =>
					row_v <= 5;
					keyb8_o(5) <= '0';
					state_v := ROW6;
					
				when ROW6 =>
					row_v <= 6;
					keyb8_o(6) <= '0';
					state_v := ROW7;

				when ROW7 =>
					row_v <= 7;
					keyb8_o(7) <= '0';
					state_v := ROW0;
				
			end case;

		end if;
		
	end process;
	

	
	

end membrane_arch;
