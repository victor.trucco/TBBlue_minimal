--
-- TBBlue / ZX Spectrum Next project
--
-- Soundrive/Covox/Specdrum - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--


-- SpecDrum PORT 0xDF

--SOUNDRIVE 1.05 PORTS - mode 1
--  0x0F = left channel A (stereo covox channel 1)
--  0x1F = left channel B
--  0x4F = right channel C (stereo covox channel 2) 
--  0x5F = right channel D
--
--  SOUNDRIVE 1.05 PORTS - mode 2
--  0xF1 = left channel A
--  0xF3 = left channel B (GS covox - port 0xB3)
--  0xF9 = right channel C
--  0xFB = right channel D (covox - port 0xFB)

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.all;
 
entity soundrive is
	port 
	( 
		reset_i			: in std_logic;
		clk_i				: in std_logic;
		enable_i			: in std_logic;
		cpu_a_i			: in std_logic_vector(7 downto 0);
		cpu_d_i			: in std_logic_vector(7 downto 0);
		cpu_wr_n_i		: in std_logic;
		cpu_ioreq_n_i	: in std_logic;
		
		mirror_ena_i	: in std_logic;
		mirror_data_i	: in std_logic_vector(7 downto 0);
			
		soundrive_L_o	: out unsigned(8 downto 0);
		soundrive_R_o	: out unsigned(8 downto 0)
	);
		
end soundrive;
 
architecture soundrive_unit of soundrive is

	signal channel_A	: std_logic_vector (7 downto 0);
	signal channel_B	: std_logic_vector (7 downto 0);
	signal channel_C	: std_logic_vector (7 downto 0);
	signal channel_D	: std_logic_vector (7 downto 0);
	
	
begin
	process (clk_i, reset_i, enable_i)
		--variable edge_v : std_logic_vector(1 downto 0);
	begin
		if reset_i = '1' or enable_i = '0' then
		
			channel_A <= (others => '0');
			channel_B <= (others => '0');
			channel_C <= (others => '0');
			channel_D <= (others => '0');
			
		elsif rising_edge(clk_i) then
		
			--edge_v := edge_v(0) & mirror_ena_i;
			
			-- mode 1
			if    cpu_a_i(7 downto 0) = X"0F" and cpu_ioreq_n_i = '0' and cpu_wr_n_i = '0' then
				channel_A <= cpu_d_i;
			
			elsif cpu_a_i(7 downto 0) = X"1F" and cpu_ioreq_n_i = '0' and cpu_wr_n_i = '0' then
				channel_B <= cpu_d_i;
			
			elsif cpu_a_i(7 downto 0) = X"4F" and cpu_ioreq_n_i = '0' and cpu_wr_n_i = '0' then
				channel_C <= cpu_d_i;
			
			elsif cpu_a_i(7 downto 0) = X"5F" and cpu_ioreq_n_i = '0' and cpu_wr_n_i = '0' then
				channel_D <= cpu_d_i;
				
			-- mode 2
			elsif cpu_a_i(7 downto 0) = X"F1" and cpu_ioreq_n_i = '0' and cpu_wr_n_i = '0' then
				channel_A <= cpu_d_i;
			
			elsif cpu_a_i(7 downto 0) = X"F3" and cpu_ioreq_n_i = '0' and cpu_wr_n_i = '0' then
				channel_B <= cpu_d_i;
			
			elsif cpu_a_i(7 downto 0) = X"F9" and cpu_ioreq_n_i = '0' and cpu_wr_n_i = '0' then
				channel_C <= cpu_d_i;
			
			elsif cpu_a_i(7 downto 0) = X"FB" and cpu_ioreq_n_i = '0' and cpu_wr_n_i = '0' then
				channel_D <= cpu_d_i;
				
			-- PROFI
			elsif cpu_a_i(7 downto 0) = X"3F" and cpu_ioreq_n_i = '0' and cpu_wr_n_i = '0' then
				channel_A <= cpu_d_i;
				
			-- SpecDrum
			elsif (cpu_a_i(7 downto 0) = X"DF"                and cpu_ioreq_n_i = '0' and cpu_wr_n_i = '0') then
					channel_A <= cpu_d_i;
					--channel_B <= (others => '0');
					channel_C <= cpu_d_i;
					--channel_D <= (others => '0');
			
			
			-- SpecDrum mirror
			--if (edge_v = "01") then -- detect rising edge		
			elsif mirror_ena_i = '1' then 			
					channel_A <= mirror_data_i;
					--channel_B <= (others => '0');
					channel_C <= mirror_data_i;
					--channel_D <= (others => '0');
					
			end if;
			
		end if;
	end process;
	
	
	soundrive_L_o <= unsigned('0' & channel_A) + unsigned('0' & channel_B);
	soundrive_R_o <= unsigned('0' & channel_C) + unsigned('0' & channel_D);
	
end soundrive_unit;