--
-- TBBlue / ZX Spectrum Next project
-- Copyright (c) 2013-2017 - Fabio Belavenuto & Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, ORcopper
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
-- ID 11 = Multicore
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_UNSIGNED.all;
 
entity multicore_top is
	generic (
		hdmi_output_g		: boolean	:= false;
		
		machine_id_g	   : unsigned(7 downto 0)	:= X"0B";	-- 11 = Multicore
		version_g			: unsigned(7 downto 0)	:= X"1A";	-- 1.10
		sub_version_g		: unsigned(7 downto 0)	:= X"0a"		-- .10
	);
	port (
		-- Clocks
		clock_50_i			: in    std_logic;

		-- Buttons
		btn_n_i				: in    std_logic_vector(4 downto 1);
		btn_oe_n_i			: in    std_logic;
		btn_clr_n_i			: in    std_logic;

		-- SRAM (AS7C34096)
		sram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		sram_data_io		: inout std_logic_vector(7 downto 0)	:= (others => 'Z');
		sram_we_n_o			: out   std_logic								:= '1';
		sram_ce_n_o			: out   std_logic_vector(1 downto 0)	:= (others => '1');
		sram_oe_n_o			: out   std_logic								:= '1';

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_mouse_clk_io  : inout std_logic								:= 'Z';
		ps2_mouse_data_io : inout std_logic								:= 'Z';

		-- SD Card
		sd_cs_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Joystick
		joy1_up_i			: in    std_logic;
		joy1_down_i			: in    std_logic;
		joy1_left_i			: in    std_logic;
		joy1_right_i		: in    std_logic;
		joy1_p6_i			: in    std_logic;
		joy1_p7_o			: out   std_logic								:= '1';
		joy1_p9_i			: in    std_logic;
		joy2_up_i			: in    std_logic;
		joy2_down_i			: in    std_logic;
		joy2_left_i			: in    std_logic;
		joy2_right_i		: in    std_logic;
		joy2_p6_i			: in    std_logic;
		joy2_p7_o			: out   std_logic								:= '1';
		joy2_p9_i			: in    std_logic;

		-- Audio
		dac_l_o				: out   std_logic								:= '0';
		dac_r_o				: out   std_logic								:= '0';
		ear_i					: in    std_logic;
		mic_o					: out   std_logic								:= '0';

		-- VGA
		vga_r_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_g_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_b_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
		vga_hsync_n_o		: out   std_logic								:= '1';
		vga_vsync_n_o		: out   std_logic								:= '1';

		-- HDMI
--		hdmi_d_o				: out   std_logic_vector(2 downto 0)	:= (others => '0');
--		hdmi_clk_o			: out   std_logic								:= '0';
--		hdmi_cec_o			: out   std_logic								:= '0';

		-- Debug
		leds_n_o				: out   std_logic_vector(7 downto 0)	:= (others => '1')
	);
end entity;

architecture behavior of multicore_top is

	-- ASMI (Altera specific component)
	component cyclone_asmiblock
	port (
		dclkin      : in    std_logic;      -- DCLK
		scein       : in    std_logic;      -- nCSO
		sdoin       : in    std_logic;      -- ASDO
		oe          : in    std_logic;      --(1=disable(Hi-Z))
		data0out    : out   std_logic       -- DATA0
	);
	end component;
	
	component ps2mouse 
	port
	(
		clk 		: in std_logic;		    					-- bus clock
		reset 	: in std_logic;			   				-- reset 
		ps2mdat 	: inout std_logic;			 				-- mouse PS/2 data
		ps2mclk 	: inout std_logic;			 				-- mouse PS/2 clk
		mou_emu 	: in std_logic_vector(5 downto 0); 		-- mouse with joystick input
		sof 		: in std_logic;								-- mouse joystick emulation enable bit
		zcount	: out std_logic_vector(7 downto 0);  	-- mouse Z counter
		ycount	: out std_logic_vector(7 downto 0);		-- mouse Y counter
		xcount	: out std_logic_vector(7 downto 0);		-- mouse X counter
		mleft		: out std_logic;								-- left mouse button output
		mthird	: out std_logic;								-- third(middle) mouse button output
		mright	: out std_logic;								-- right mouse button output
		test_load: in std_logic;								-- load test value to mouse counter
		test_data: in std_logic_vector(15 downto 0)		-- mouse counter test value
  );
  end component;
	
	-- Clocks
	signal clock_28_s			: std_logic;
	signal clock_28_180o_s	: std_logic;
	signal clock_14_s			: std_logic;
	signal clock_7_s			: std_logic;
	signal clock_3m5_s		: std_logic;
	signal clock_psg_s		: std_logic;
	signal clock_cpu_s		: std_logic;
	signal cpu_clock_cont_s	: std_logic;	
	signal clock_hdmi_s		: std_logic;
	signal clock_div_q		: unsigned(3 downto 0) 				:= (others => '0');				
	signal turbo_s				: std_logic_vector(1 downto 0);
	signal clock_turbo_s		: std_logic;

	-- Altera PLL
	signal pll_locked_s		: std_logic;	
		
	-- Resets
	signal poweron_s			: std_logic;
	signal hard_reset_s		: std_logic;
	signal soft_reset_s		: std_logic;
	signal int_soft_reset_s	: std_logic;
	signal reset_s				: std_logic;

	-- Memory buses
	signal vram_addr_s		: std_logic_vector(20 downto 0);
	signal vram_dout_s		: std_logic_vector( 7 downto 0);
	signal vram_cs_s			: std_logic;
	signal vram_oe_s			: std_logic;
	signal ram_addr_s			: std_logic_vector(20 downto 0);		
	signal ram_din_s			: std_logic_vector( 7 downto 0);
	signal ram_dout_s			: std_logic_vector( 7 downto 0);
	signal ram_cs_s			: std_logic;
	signal ram_oe_s			: std_logic;
	signal ram_we_s			: std_logic;
	signal rom_addr_s			: std_logic_vector(13 downto 0);		
	signal rom_data_s			: std_logic_vector( 7 downto 0);

	signal intram_cs_s		: std_logic;
	signal intram_we_s		: std_logic;
	signal intram_dout_s		: std_logic_vector( 7 downto 0);
	signal intvram_cs_s		: std_logic;
	signal intvram_dout_s	: std_logic_vector( 7 downto 0);
	signal memram_cs_s		: std_logic;
	signal memvram_cs_s		: std_logic;
	signal ramdout_mux_s		: std_logic_vector( 7 downto 0);
	signal vramdout_mux_s	: std_logic_vector( 7 downto 0);
	
	-- Audio
	signal spk_s				: std_logic;
	signal mic_s				: std_logic;
	signal psg_L_s				: unsigned( 9 downto 0);
	signal psg_R_s				: unsigned( 9 downto 0);
	signal sid_L_s				: unsigned(17 downto 0);
	signal sid_R_s				: unsigned(17 downto 0);
	signal covox_L_s			: unsigned( 8 downto 0);
	signal covox_R_s			: unsigned( 8 downto 0);
	signal tapein_s			: std_logic_vector( 7 downto 0);
	signal pcm_out_L_s		: std_logic_vector(13 downto 0);
	signal pcm_out_R_s		: std_logic_vector(13 downto 0);

	-- Keyboard
	signal kb_rows_s			: std_logic_vector( 7 downto 0);
	signal kb_columns_s		: std_logic_vector( 4 downto 0);
	signal FKeys_s				: std_logic_vector(12 downto 1);
	signal keymap_addr_s		: std_logic_vector( 8 downto 0);
	signal keymap_data_s		: std_logic_vector( 8 downto 0);
	signal keymap_we_s		: std_logic;
	signal use_ps2_alt_s		: std_logic := '0';

	-- Mouse
	signal mouse_x_s			: std_logic_vector( 7 downto 0);	
	signal mouse_y_s			: std_logic_vector( 7 downto 0);			
	signal mouse_bts_s		: std_logic_vector( 2 downto 0);		
	signal mouse_wheel_s		: std_logic_vector( 3 downto 0);

	-- SPI e EPCS
	signal spi_mosi_s			: std_logic;
	signal spi_sclk_s			: std_logic;
	signal flash_miso_s		: std_logic;
	signal flash_cs_n_s		: std_logic;

	-- Joystick
	signal joy1_s				: std_logic_vector( 5 downto 0);
	signal joy2_s           : std_logic_vector( 5 downto 0);

	-- Video and scandoubler
	signal rgb_r_s				: std_logic_vector( 2 downto 0);
	signal rgb_g_s				: std_logic_vector( 2 downto 0);
	signal rgb_b_s				: std_logic_vector( 2 downto 0);
	signal rgb_i_s				: std_logic;
	signal rgb_hs_n_s			: std_logic;
	signal rgb_vs_n_s			: std_logic;
	signal ulap_en_s			: std_logic;
	signal rgb_ulap_s			: std_logic_vector( 7 downto 0);
	signal rgb_comb_s			: std_logic_vector( 8 downto 0);
	signal rgb_out_s			: std_logic_vector( 8 downto 0);
	signal scandbl_en_s		: std_logic;
	signal hsync_out_s		: std_logic;
	signal vsync_out_s		: std_logic;
	signal scanlines_s		: std_logic								:= '0';
	signal ula50_60_s			: std_logic								:= '0';
	signal mach_hdmi_s		: std_logic_vector( 2 downto 0)	:= "000";
	signal mach_timing_s		: std_logic_vector( 2 downto 0);
	signal ha_value_s			: integer;
	
	signal tdms_r_s			: std_logic_vector( 9 downto 0);
	signal tdms_g_s			: std_logic_vector( 9 downto 0);
	signal tdms_b_s			: std_logic_vector( 9 downto 0);
	signal hdmi_p_s			: std_logic_vector( 3 downto 0);
	signal hdmi_n_s			: std_logic_vector( 3 downto 0);

	-- layer2
	signal layer2_addr_s	: std_logic_vector(20 downto 0);
	signal layer2_data_s	: std_logic_vector( 7 downto 0);	
	signal layer2_cs_s		: std_logic;
	signal pixel_clock_s		: std_logic;
	
	signal blank_s				: std_logic;
	signal hblank_s			: std_logic;
	signal vblank_s			: std_logic;	
	--
	signal mux_addr_s			: std_logic_vector(20 downto 0);
	signal mux_din_s			: std_logic_vector( 7 downto 0);	
	signal mux_dout_s			: std_logic_vector( 7 downto 0);	
	signal mux_cs_s			: std_logic;
	signal mux_oe_s			: std_logic;
	signal mux_wr_s			: std_logic;
	
	signal sram_ce_s			: std_logic;
	
	signal hcount_s				: unsigned(8 downto 0)				:= (others => '0');
	signal vcount_s				: unsigned(8 downto 0)				:= (others => '0');
	
	-- HDMI initial values
	signal h_visible_s	: integer := 0;
	signal hsync_start_s	: integer := 0;
	signal hsync_end_s	: integer := 0;
	signal hcnt_end_s		: integer := 0;
	signal v_visible_s	: integer := 0;
	signal vsync_start_s	: integer := 0;
	signal vsync_end_s	: integer := 0;
	signal vcnt_end_s		: integer := 0;

begin

	--------------------------------
	-- PLL
	--  50 MHz input
	--------------------------------
	pll: entity work.pll1
	port map (
		inclk0	=> clock_50_i,
		c0			=> clock_28_s,
		c1			=> clock_hdmi_s,
		c2 		=> clock_28_180o_s,
		c3			=> clock_14_s,
		c4			=> clock_7_s,
		locked	=> pll_locked_s
	);
	
	-- Geracao dos clocks
	process(clock_28_s)
	begin
		if rising_edge(clock_28_s) then 
			clock_div_q <= clock_div_q + 1;
		end if;
	end process;
	-- clock_div_q(0) = /2	= 14 MHz
	-- clock_div_q(1) = /4	= 7 MHz
	-- clock_div_q(2) = /8	= 3.5 MHz
	-- clock_div_q(3) = /16	= 1.75 MHz
	-- clock_div_q(4) = /32	= 875 KHz
	-- clock_div_q(5) = /64	= 437.5 KHz
--	clk_vid	<= clock_div_q(0);
	clock_psg_s	<= '1' when clock_div_q(3 downto 0) = "1110" else '0';

	clock_3m5_s <= clock_div_q(2);				-- 3.5 MHz no contencion
	
	clock_turbo_s	<= clock_7_s	when turbo_s = "00"		else --for 3.5
							clock_14_s	when turbo_s = "01"		else --for 7
							clock_28_s; -- for 14
	
	process (clock_turbo_s)
	begin
		if rising_edge(clock_turbo_s) then
			if clock_cpu_s = '1' and Cpu_clock_cont_s = '0' then		-- if there's no contention, the clock can go low
				clock_cpu_s <= '0';
			else
				clock_cpu_s <= '1';
			end if;
		end if;
	end process;
	
	
	-- The TBBlue
	tbblue1 : entity work.tbblue
	generic map (
		use_turbo		=> true,
		machine_id_g	=> machine_id_g,
		version_g		=> version_g,
		sub_version_g	=> sub_version_g,
		usar_kempjoy	=> '1',
		usar_keyjoy		=> '1',
		use_layer2_g	=> true,
		use_sprites_g	=> false,
		use_turbosnd_g	=> false,
		use_sid_g		=> false,
		use_dma_g		=> false,
		use_z80n_g		=> true,
		use_lightpen_g	=> false,
		use_copper_g	=> false    --multicore cant implement copper list as BRAM
		
	)
	port map (
		-- Clock
		iClk_28				=> clock_28_s,
		iClk_28_180o		=> clock_28_180o_s,
		iClk_14				=> clock_14_s,
		iClk_7				=> clock_7_s,
		iClk_3M5				=> clock_3m5_s,				-- 3.5 MHz no contencion
		iClk_Cpu				=> clock_cpu_s,				-- CPU clock with contention
		iClk_Psg				=> clock_psg_s,				-- psg clock
		oCpu_clock_cont	=>	Cpu_clock_cont_s,			-- Signal to CPU clock contention
		oTurbo_sel			=> turbo_s,  					-- Selected turbo Speed
		
		-- Reset
		iPowerOn				=> poweron_s,
		iHardReset			=> hard_reset_s,
		iSoftReset			=> soft_reset_s,
		oSoftReset			=> int_soft_reset_s,

		-- Keys
		iKey50_60hz			=> FKeys_s(3),
		iKeyScanDoubler	=> FKeys_s(2),
		iKeyScanlines		=> FKeys_s(7),
		iKeyDivMMC			=> FKeys_s(10) or not btn_n_i(2),
		iKeyMF				=> FKeys_s(9) or not btn_n_i(3),
		iKeyTurbo			=> FKeys_s(8),
		iKeysHard			=> "00",

		-- Keyboard
		oRows					=> kb_rows_s,
		iColumns				=> kb_columns_s,
		oPort254_cs			=> open,
		oKeymap_addr		=> keymap_addr_s,
		oKeymap_data		=> keymap_data_s,
		oKeymap_we			=> keymap_we_s,
		oAltPs2 				=> use_ps2_alt_s,

		-- RGB
		oRGB_r				=> rgb_r_s,
		oRGB_g				=> rgb_g_s,
		oRGB_b				=> rgb_b_s,
		oRGB_hs_n			=> rgb_hs_n_s,
		oRGB_vs_n			=> rgb_vs_n_s,
		oRGB_cs_n			=> open,
		oRGB_hb_n			=> hblank_s,
		oRGB_vb_n			=> vblank_s,
		oRGB_hcnt			=> open,
		oRGB_vcnt			=> open,
		oScandbl_en			=> scandbl_en_s,
		oScandbl_sl			=> scanlines_s,
		oULA50_60			=> ula50_60_s,
		oMachTiming			=> mach_timing_s,
		oMachVideoTiming	=> mach_hdmi_s,
		oNTSC_PAL			=> open,

		-- VRAM
		oVram_a				=> vram_addr_s,
		iVram_dout			=> vramdout_mux_s,--vram_dout_s,
		oVram_cs				=> vram_cs_s,
		oVram_rd				=> vram_oe_s,

		-- Bootrom
		oBootrom_en			=> open,
		oRom_a				=> rom_addr_s,
		iRom_dout			=> rom_data_s,
		oFlashboot			=> open,

		-- RAM
		oRam_a				=> ram_addr_s,
		oRam_din				=> ram_din_s,
		iRam_dout			=> ramdout_mux_s,--ram_dout_s,
		oRam_cs				=> ram_cs_s,
		oRam_rd				=> ram_oe_s,
		oRam_wr				=> ram_we_s,

		-- SPI (SD and Flash)
		oSpi_mosi			=> spi_mosi_s,
		oSpi_sclk			=> spi_sclk_s,
		oSD_cs0_n			=> sd_cs_n_o,
		oSD_cs1_n			=> open,
		iSD_miso				=> sd_miso_i,
		oFlash_cs_n			=> flash_cs_n_s,
		iFlash_miso			=> flash_miso_s,

		-- Sound
		iEAR					=> ear_i,
		oSPK					=> spk_s,
		oMIC					=> mic_s,
		oPSG_L				=> psg_L_s,
		oPSG_R				=> psg_R_s,
		oSID_L				=> sid_L_s,
		oSID_R				=> sid_R_s,
		oCovox_L				=> covox_L_s,
		oCovox_R				=> covox_R_s,
		oDAC					=> open,
		oIntSnd				=> open,

		-- Joystick
		-- order: Fire2, Fire, Up, Down, Left, Right
		iJoy0					=> joy1_s,
		iJoy1					=> joy2_s,

		-- Mouse
		iMouse_en			=> '1',
		iMouse_x				=>	mouse_x_s,
		iMouse_y				=>	mouse_y_s,
		iMouse_bts			=>	mouse_bts_s,
		iMouse_wheel		=>	mouse_wheel_s,
		oPS2mode				=> open,

		-- Lightpen
		iLp_signal			=> '0',
		oLp_en				=> open,

		-- RTC
		ioRTC_sda			=> 'Z',
		ioRTC_scl			=> 'Z',

		-- Serial
		iRs232_rx			=> '0',
		oRs232_tx			=> open,
		iRs232_dtr			=> '0',
		oRs232_cts			=> open,

		-- BUS
		oCpu_a				=> open,
		oCpu_do				=> open,
		iCpu_di				=> (others => '1'),
		oCpu_mreq			=> open,
		oCpu_ioreq			=> open,
		oCpu_rd				=> open,
		oCpu_wr				=> open,
		oCpu_m1				=> open,
		iCpu_Wait_n			=> '1',
		iCpu_nmi				=> '1',
		iCpu_int_n			=> '1',
		iCpu_romcs			=> '0',
		iCpu_ramcs			=> '0',
		iCpu_busreq_n		=> '1',
		oCpu_busack_n		=> open,
		oCpu_clock			=> open,
		oCpu_halt_n			=> open,
		oCpu_rfsh_n			=> open,
		iCpu_iorqula		=> '0',
		
		-- Overlay
		oLayer2_addr		=> layer2_addr_s,
		iLayer2_data		=> layer2_data_s,
		oLayer2_cs			=> layer2_cs_s,
		
--		-- Timing
--		h_visible_o			=> h_visible_s,	
--		hsync_start_o		=> hsync_start_s,	
--		hsync_end_o			=> hsync_end_s,	
--		hcnt_end_o			=> hcnt_end_s,	
--		v_visible_o			=> v_visible_s,
--		vsync_start_o		=> vsync_start_s,
--		vsync_end_o			=> vsync_end_s,
--		vcnt_end_o			=> vcnt_end_s,
		
		--Debug
		oD_leds				=> open,
		oD_reg_o				=> open,
		oD_others			=> open,
		iSW					=> (others=>'0')
	);
	
	-- Internal VRAM
	intram_cs_s		<= '1' when ram_cs_s = '1'  and ram_addr_s(20 downto 14)  = "0010101" else	'0'; -- 10101
	intvram_cs_s	<= '1' when vram_cs_s = '1' and vram_addr_s(20 downto 14) = "0010101" else	'0'; -- 10101

	--maps the shadow area on the bram (need a change on bram addr selection too)
--	intram_cs_s		<= '1' when ram_cs_s = '1'  and (ram_addr_s(20 downto 14)  = "0010101" or ram_addr_s(20 downto 14)   = "0010111") else	'0'; 
--	intvram_cs_s	<= '1' when vram_cs_s = '1' and (vram_addr_s(20 downto 14) = "0010101" or vram_addr_s(20 downto 14)  = "0010111") else	'0'; 

	
	memram_cs_s		<= ram_cs_s 		when intram_cs_s = '0'	else '0';
	memvram_cs_s	<= vram_cs_s		when intvram_cs_s = '0'	else '0';
	intram_we_s		<= ram_we_s			when intram_cs_s = '1'	else '0';
	ramdout_mux_s	<= intram_dout_s	when intram_cs_s = '1'	else ram_dout_s;
	vramdout_mux_s	<= intvram_dout_s	when intvram_cs_s = '1'	else vram_dout_s;

	intram : entity work.dpram2
	generic map (
		addr_width_g	=> 14,
		data_width_g	=> 8
	)
	port map (
		-- CPU port
		clk_a_i  	=> clock_28_s,
		we_i     	=> intram_we_s,
		addr_a_i 	=> ram_addr_s(13 downto 0),
		--addr_a_i 	=> ram_addr_s(15) & ram_addr_s(13 downto 0),
		data_a_i 	=> ram_din_s,
		data_a_o 	=> intram_dout_s,
		-- ULA port
		clk_b_i  	=> clock_28_s,
		addr_b_i		=> vram_addr_s(13 downto 0),
--		addr_b_i		=> vram_addr_s(15) & vram_addr_s(13 downto 0),
		data_b_o 	=> intvram_dout_s
	);
	
	
	
	mux_addr_s		<= vram_addr_s		when layer2_cs_s = '0' else layer2_addr_s;
	mux_cs_s			<= memvram_cs_s	when layer2_cs_s = '0' else layer2_cs_s;
	mux_oe_s			<= vram_oe_s		when layer2_cs_s = '0' else layer2_cs_s;--clock_7_s; --pixel_clock_s; -- not pixel_clock_s;
	vram_dout_s 	<= mux_dout_s;
	layer2_data_s <= mux_dout_s;			

	--
 	ram : entity work.dpSRAM_4x512x8
 	port map(
 		clk				=> clock_28_180o_s, --clock_28_180o_s,
 
 		-- Porta0 (VRAM)
 		porta0_addr		=> mux_addr_s,	--vram_addr_s,
 		porta0_ce		=> mux_cs_s,						--memvram_cs_s,
 		porta0_oe		=> mux_oe_s,						--vram_oe_s,
 		porta0_we		=> '0',								--'0',
 		porta0_din		=> (others => '0'),				--(others => '0'),
 		porta0_dout		=> mux_dout_s,						--vram_dout_s,
 
 		-- Porta1 (Upper RAM)
 		porta1_addr		=> ram_addr_s,
 		porta1_ce		=> memram_cs_s,
 		porta1_oe		=> ram_oe_s,
 		porta1_we		=> ram_we_s,
 		porta1_din		=> ram_din_s,
 		porta1_dout		=> ram_dout_s,
 
 		-- Outputs to SRAM on board
 		sram_addr		=> sram_addr_o,
 		sram_data		=> sram_data_io,
 		sram_ce_n(1 downto 0) => sram_ce_n_o,
 		sram_ce_n(3 downto 2) => open,
 		sram_oe_n		=> sram_oe_n_o,
 		sram_we_n		=> sram_we_n_o
 	);
	
	
--	ram : entity work.dpram_async 
--    port map
--	 (
--	 
--			address_0 => mux_addr_s, 		-- address_0 Input
--			data_i_0  => (others => '0'),	-- data_0 in
--			data_o_0  => mux_dout_s,		-- data_0 out
--			cs_0      => mux_cs_s,    		-- Chip Select
--			we_0      => '0',    			-- Write Enable/Read Enable
--			oe_0      => mux_oe_s,   		-- Output Enable
--		
--			address_1 => ram_addr_s,	-- address_1 Input
--			data_i_1  => ram_din_s,		-- data_1 in
--			data_o_1  => ram_dout_s,	-- data_1 out
--			cs_1      => memram_cs_s, 	-- Chip Select
--			we_1      => ram_we_s,    	-- Write Enable/Read Enable
--			oe_1      => ram_oe_s,    	-- Output Enable
--		
--			-- Output to SRAM
--			sram_addr_o		=> sram_addr_o,
--			sram_data_io	=> sram_data_io,
--			sram_ce_n_o(1 downto 0) => sram_ce_n_o,
--			sram_ce_n_o(3 downto 2) => open,
--			sram_oe_n_o		=> sram_oe_n_o,
--			sram_we_n_o		=> sram_we_n_o
--			
--    );



	-- PS/2 emulating speccy keyboard
	kb: entity work.ps2keyb
	port map (
		enable_i			=> '1',
		use_ps2_alt_i  => use_ps2_alt_s,
		clock_i			=> clock_28_s,
		clock_180o_i	=> clock_28_180o_s,
		clock_ps2_i		=> clock_div_q(3),
		reset_i			=> poweron_s,
		--
		ps2_clk_io		=> ps2_clk_io,
		ps2_data_io		=> ps2_data_io,
		--
		rows_i			=> kb_rows_s,
		cols_o			=> kb_columns_s,
		functionkeys_o	=> FKeys_s,
		core_reload_o	=> open,
		keymap_addr_i	=> keymap_addr_s,
		keymap_data_i	=> keymap_data_s,
		keymap_we_i		=> keymap_we_s
	);
	
	
--	-- Mouse control
--	mousectrl: entity work.mouse_ctrl
--	generic map (
--		clkfreq 		=> 28000,
--		SENSIBILITY	=> 1						-- Bigger values, less speed
--	)
--	port map (
--		enable		=> '1',
--		clock			=> clock_28_s,
--		reset			=> reset_s,
--		ps2_clk		=> ps2_mouse_clk_io,
--		ps2_data		=> ps2_mouse_data_io,
--		mouse_x 		=> mouse_x_s,
--		mouse_y		=> mouse_y_s,
--		mouse_bts	=> mouse_bts_s,
--		mouse_wheel => mouse_wheel_s
--	);

--	 mouse_unit: entity work.mouse(arch)
--	 	generic map (
--		SENSIBILITY	=> 1						-- Bigger values, less speed
--	)
--      port map(
--		clk=>clock_28_s, 
--		reset=>reset_s,
--               ps2d=>ps2_mouse_data_io, 
--					ps2c=>ps2_mouse_clk_io,
--               xm=>mouse_x_s, 
--					ym=>mouse_y_s, 
--					btnm=>mouse_bts_s,
--               m_done_tick=>open 
--					);
	
	mousectrl: ps2mouse 
	port map
	(
		clk 		=> clock_28_s, 		-- bus clock
		reset 	=> reset_s,		   	-- reset 
		ps2mdat 	=> ps2_mouse_data_io,-- mouse PS/2 data
		ps2mclk 	=> ps2_mouse_clk_io,	-- mouse PS/2 clk
		--
		xcount	=> mouse_x_s, 			-- mouse X counter		
		ycount	=> mouse_y_s, 			-- mouse Y counter
		zcount	=> open, 				-- mouse Z counter
		mleft		=> mouse_bts_s(0),	-- left mouse button output
		mright	=> mouse_bts_s(1),	-- right mouse button output
		mthird	=> mouse_bts_s(2),	-- third(middle) mouse button output
		--
		sof 		=> '0',					-- mouse joystick emulation enable bit
		mou_emu 	=> (others=>'0'), 	-- mouse with joystick input
		--
		test_load=> '0',					-- load test value to mouse counter
		test_data=> (others=>'0')		-- mouse counter test value
  );

	

	-- Boot ROM
	boot_rom: entity work.bootrom
	port map (
		clk		=> clock_28_s,
		addr		=> rom_addr_s(12 downto 0),
		data		=> rom_data_s
	);

	-- Audio
	audio : entity work.Audio_DAC
	port map (
		clock_i		=> clock_28_s,
		reset_i		=> reset_s,
		ear_i			=> ear_i,
		spk_i			=> spk_s,
		mic_i			=> mic_s,
		psg_L_i		=> psg_L_s,
		psg_R_i		=> psg_R_s,
		sid_L_i		=> sid_L_s,
		sid_R_i		=> sid_R_s,
		covox_L_i	=> covox_L_s,
		covox_R_i	=> covox_R_s,
		dac_r_o		=> dac_r_o,
		dac_l_o		=> dac_l_o,
		pcm_L_o		=> pcm_out_L_s,
		pcm_R_o		=> pcm_out_R_s
	);

	-- EPCS4
	epcs4: cyclone_asmiblock
	port map (
		oe          => '0',
		scein       => flash_cs_n_s,
		dclkin      => spi_sclk_s,
		sdoin       => spi_mosi_s,
		data0out    => flash_miso_s
	);

	-- glue
	poweron_s		<= '1' when pll_locked_s = '0'							else '0';
	hard_reset_s	<= '1' when FKeys_s(1) = '1'	else '0';-- or btn_n_i(1) = '0'
	soft_reset_s	<= '1' when int_soft_reset_s = '1' or FKeys_s(4) = '1'  	else '0';--or btn_n_i(4) = '0'
	reset_s			<= poweron_s or hard_reset_s or soft_reset_s;

	-- SD
	sd_mosi_o		<= spi_mosi_s;
	sd_sclk_o		<= spi_sclk_s;

	-- Audio
	mic_o				<= mic_s;

	-- Joystick
	-- order: Fire2, Fire, Up, Down, Left, Right
	joy1_s	<= not (joy1_p9_i & joy1_p6_i & joy1_up_i & joy1_down_i & joy1_left_i & joy1_right_i);
	joy2_s	<= not (joy2_p9_i & joy2_p6_i & joy2_up_i & joy2_down_i & joy2_left_i & joy2_right_i);

	rgb_comb_s <= rgb_r_s & rgb_g_s & rgb_b_s;

	uh: if hdmi_output_g generate
	
	
	



			-- Defaults
			-- Modeline "720x576x50hz" 	27 	720  	732  	796  	864   	576  	581  	586  	625 
			-- ModeLine "720x480@60hz" 	27 	720 	736 	798 	858 		480 	489 	495 	525 
			process (ula50_60_s)
			begin
				if ula50_60_s = '0' then --50hz

					h_visible_s		<=720 - 1;
					hsync_start_s	<=732 - 1;
					hsync_end_s		<=796 - 1;
					hcnt_end_s		<=864 - 1;
					
					v_visible_s		<=576 - 1;
					vsync_start_s	<=581 - 1;
					vsync_end_s		<=586 - 1;
					vcnt_end_s		<=625 - 2;
					
				else -- 60hz

					h_visible_s		<=720 - 1;
					hsync_start_s	<=736 - 1;
					hsync_end_s		<=798 - 1;
					hcnt_end_s		<=858 - 1;

					v_visible_s		<=480 - 1;
					vsync_start_s	<=489 - 1;
					vsync_end_s		<=495 - 1;
					vcnt_end_s		<=525 - 2;
					
				end if;
			end process;

			
			hdmi_frame: entity work.hdmi_frame
			port map 
			(
				clock_i 		=> clock_14_s, --14mhz
				clock2x_i 	=> clock_28_s, --28mhz
				reset_i		=> reset_s,
				scanlines_i	=> scanlines_s,
				rgb_i			=> rgb_comb_s,
				hsync_i 		=> rgb_hs_n_s,
				vsync_i 		=> rgb_vs_n_s,
				hblank_n_i	=> hblank_s,
				vblank_n_i	=> vblank_s,
				timing_i		=> mach_hdmi_s,
				
				--outputs
				rgb_o 		=> rgb_out_s,
				hsync_o 		=> hsync_out_s,
				vsync_o 		=> vsync_out_s,
				blank_o		=> blank_s,

				-- config values 
				h_visible	=> h_visible_s,
				hsync_start	=> hsync_start_s,
				hsync_end	=> hsync_end_s,
				hcnt_end		=> hcnt_end_s,
				--
				v_visible	=> v_visible_s,
				vsync_start	=> vsync_start_s,
				vsync_end	=> vsync_end_s,
				vcnt_end		=> vcnt_end_s
			);
			
		-- HDMI
		hdmi: entity work.hdmi
		generic map (
			FREQ	=> 27000000,	-- pixel clock frequency 
			FS		=> 48000,		-- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
			CTS	=> 27000,		-- CTS = Freq(pixclk) * N / (128 * Fs)
			N		=> 6144			-- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
		)
		port map (
			I_CLK_PIXEL		=> clock_28_s,
			I_R				=> rgb_out_s (8 downto 6) & rgb_out_s (8 downto 6) & rgb_out_s (8 downto 7),
			I_G				=> rgb_out_s (5 downto 3) & rgb_out_s (5 downto 3) & rgb_out_s (5 downto 4),
			I_B				=> rgb_out_s (2 downto 0) & rgb_out_s (2 downto 0) & rgb_out_s (2 downto 1),
			I_BLANK			=> blank_s,
			I_HSYNC			=> hsync_out_s,
			I_VSYNC			=> vsync_out_s,
			-- PCM audio
			I_AUDIO_ENABLE	=> '1',
			I_AUDIO_PCM_L 	=> pcm_out_L_s & "00",
			I_AUDIO_PCM_R	=> pcm_out_R_s & "00",
			-- TMDS parallel pixel synchronous outputs (serialize LSB first)
			O_RED				=> tdms_r_s,
			O_GREEN			=> tdms_g_s,
			O_BLUE			=> tdms_b_s
		);

		hdmio: entity work.hdmi_out_altera
		port map (
			clock_pixel_i		=> clock_28_s,
			clock_tdms_i		=> clock_hdmi_s,
			red_i					=> tdms_r_s,
			green_i				=> tdms_g_s,
			blue_i				=> tdms_b_s,
			tmds_out_p			=> hdmi_p_s,
			tmds_out_n			=> hdmi_n_s
		);

		vga_r_o(1)		<= hdmi_p_s(3);	-- CLK+	113
		vga_r_o(2)		<= hdmi_n_s(3);	-- CLK-	112
		vga_hsync_n_o	<= hdmi_p_s(2);	-- 2+		10
		vga_vsync_n_o	<= hdmi_n_s(2);	-- 2-		11
		vga_b_o(2)		<= hdmi_p_s(1);	-- 1+		144	
		vga_b_o(1)		<= hdmi_n_s(1);	-- 1-		143
		vga_r_o(0)		<= hdmi_p_s(0);	-- 0+		133
		vga_g_o(2)		<= hdmi_n_s(0);	-- 0-		132
	end generate;
		
	nuh: if not hdmi_output_g generate
	
 	vga: entity work.vga 
 	port map (
 	
 		clock_i 		=> clock_14_s, --14mhz
 		clock2x_i 	=> clock_28_s, --28mhz
 		scanlines_i	=> scanlines_s,
 		rgb_i			=> rgb_comb_s,
 		hsync_i 		=> rgb_hs_n_s,
 		vsync_i 		=> rgb_vs_n_s,
 		
 		--outputs
 		rgb_o 		=> rgb_out_s,
 		hsync_o 		=> hsync_out_s,
 		vsync_o 		=> vsync_out_s
 
 	);
		
--	-----------------------------------------------------------------
--	-- video scan converter required to display video on VGA hardware
--	-----------------------------------------------------------------
--	-- take note: the values below are relative to the CLK period not standard VGA clock period
--	scandbl: entity work.scan_convert
--	generic map (
--	-- mark active area of input video
--		cstart		=>  38*2,  -- composite sync start
--		clength		=> 352*2,  -- composite sync length
--	-- output video timing
--		hB				=>  32*2,	-- h sync
--		hC				=>  40*2,	-- h back porch
--		hD				=> 352*2,	-- visible video (256 + both borders)
--		hpad			=>   0*2,	-- create H black border
--
--		vB				=>   2*2,	-- v sync
--		vC				=>   5*2,	-- v back porch
--		vD				=> 284*2,	-- visible video
--		vpad			=>   0*2		-- create V black border
--	)
--	port map (
--		CLK			=> clock_14_s,
--		CLK_x2		=> clock_28_s,
--
--		hA				=> ha_value_s,	-- h front porch
--		I_VIDEO		=> rgb_comb_s,
--		I_HSYNC		=> rgb_hs_n_s,
--		I_VSYNC		=> rgb_vs_n_s,
--		I_SCANLIN	=> scanlines_s,
--
--		O_VIDEO_15	=> open,		-- scanlines processed
--		O_VIDEO_31	=> rgb_out_s,		-- scanlines processed
--		O_HSYNC		=> hsync_out_s,
--		O_VSYNC		=> vsync_out_s,
--		O_BLANK		=> blank_s
--	);
--	ha_value_s <= 24*2 when mach_timing_s = "010" or mach_timing_s = "011" else 32*2;		-- ZX 48K = 000 or 001, ZX128K = 010 or 011

		vga_r_o	<= rgb_out_s(8 downto 6)	when scandbl_en_s = '1'	else rgb_comb_s(8 downto 6);
		vga_g_o	<= rgb_out_s(5 downto 3)	when scandbl_en_s = '1'	else rgb_comb_s(5 downto 3);
		vga_b_o	<= rgb_out_s(2 downto 0)	when scandbl_en_s = '1'	else rgb_comb_s(2 downto 0);
		vga_hsync_n_o	<= hsync_out_s			when scandbl_en_s = '1'	else rgb_hs_n_s;
		vga_vsync_n_o	<= vsync_out_s			when scandbl_en_s = '1'	else rgb_vs_n_s;
	end generate;

end architecture;