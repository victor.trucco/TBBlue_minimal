--
-- TBBlue / ZX Spectrum Next project
-- Copyright (c) 2015 - Fabio Belavenuto & Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
-- ID 7 = WXEDA
--

-- altera message_off 10540 10541

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.ALL;

entity wxeda_top is
	generic (
		sdram64mb_g				: boolean	:= true
	);
	port (
		-- Clock (48MHz)
		clock_48M_i				: in    std_logic;
		-- SDRAM (32MB 16x16bit)
		sdram_clock_o			: out   std_logic;
		sdram_cke_o    	  	: out   std_logic									:= '0';
		sdram_addr_o			: out   std_logic_vector(12 downto 0)		:= (others => '0');
		sdram_dq_io				: inout std_logic_vector(15 downto 0);
		sdram_ba_o				: out   std_logic_vector( 1 downto 0);
		sdram_dqml_o			: out   std_logic;
		sdram_dqmh_o			: out   std_logic;
		sdram_cs_n_o   	  	: out   std_logic									:= '1';
		sdram_we_n_o			: out   std_logic									:= '1';
		sdram_cas_n_o			: out   std_logic									:= '1';
		sdram_ras_n_o			: out   std_logic									:= '1';
		-- SPI FLASH (W25Q32)
		flash_clk_o				: out   std_logic;
		flash_data_i			: in    std_logic;
		flash_data_o			: out   std_logic;
		flash_cs_n_o			: out   std_logic									:= '1';
		-- VGA 5:6:5
		vga_r_o					: out   std_logic_vector(4 downto 0)		:= (others => '0');
		vga_g_o					: out   std_logic_vector(5 downto 0)		:= (others => '0');
		vga_b_o					: out   std_logic_vector(4 downto 0)		:= (others => '0');
		vga_hs_o					: out   std_logic									:= '1';
		vga_vs_o					: out   std_logic									:= '1';
		-- UART
		uart_tx_o				: out   std_logic									:= '1';
		uart_rx_i				: in    std_logic;
		-- External I/O
		keys_n_i					: in    std_logic_vector(3 downto 0);
		buzzer_o					: out   std_logic									:= '1';
		-- ADC
		adc_clock_o				: out   std_logic;
		adc_data_i				: in    std_logic;
		adc_cs_n_o				: out   std_logic									:= '1';
		-- PS/2 Keyboard
		ps2_clk_io				: inout std_logic									:= 'Z';
		ps2_dat_io		 		: inout std_logic									:= 'Z';
--		-- IrDA
		irda_o					: out   std_logic;
		-- Display 7-seg
--		d7s_segments_o			: out   std_logic_vector(7 downto 0)		:= (others => '1');
--		d7s_en_o					: out   std_logic_vector(3 downto 0)		:= (others => '1')
		sd_sclk_o				: out   std_logic									:= '0';
		sd_mosi_o				: out   std_logic									:= '0';
		sd_miso_i				: in    std_logic;
		sd_cs_n_o				: out   std_logic									:= '1';
		audio_dac_l_o			: out   std_logic									:= '0';
		audio_dac_r_o			: out   std_logic									:= '0'
	);
end;

architecture behavior of wxeda_top is

	-- ASMI (Altera specific component)
	component cyclone_asmiblock
	port (
		dclkin      : in    std_logic;      -- DCLK
		scein       : in    std_logic;      -- nCSO
		sdoin       : in    std_logic;      -- ASDO
		oe          : in    std_logic;      --(1=disable(Hi-Z))
		data0out    : out   std_logic       -- DATA0
	);
	end component;

	-- Clocks
	signal clock_master_s	: std_logic;
	signal clock_sdram_s		: std_logic;
--	signal clock_video_s		: std_logic;
	signal pll_locked_s		: std_logic;

	-- Resets
	signal poweron_s			: std_logic;
	signal hard_reset_s		: std_logic;
	signal soft_reset_s		: std_logic;
	signal int_soft_reset_s	: std_logic;
	signal reset_s				: std_logic;

	-- Memory buses
	signal vram_addr_s		: std_logic_vector(19 downto 0);
	signal vram_dout_s		: std_logic_vector( 7 downto 0);
	signal vram_cs_s			: std_logic;
	signal vram_oe_s			: std_logic;
	signal ram_addr_s			: std_logic_vector(19 downto 0);
	signal ram_din_s			: std_logic_vector( 7 downto 0);
	signal ram_dout_s			: std_logic_vector( 7 downto 0);
	signal ram_cs_s			: std_logic;
	signal ram_oe_s			: std_logic;
	signal ram_we_s			: std_logic;
	signal rom_addr_s			: std_logic_vector(13 downto 0);		-- 16K
	signal rom_data_s			: std_logic_vector( 7 downto 0);

	-- Audio
	signal ear_s				: std_logic;
	signal spk_s				: std_logic;
	signal mic_s				: std_logic;
	signal psg_L_s				: unsigned( 9 downto 0);
	signal psg_R_s				: unsigned( 9 downto 0);
	signal sid_L_s				: unsigned(17 downto 0);
	signal sid_R_s				: unsigned(17 downto 0);
	signal covox_L_s			: unsigned( 8 downto 0);
	signal covox_R_s			: unsigned( 8 downto 0);
	signal tapein_s			: std_logic_vector( 7 downto 0);

	-- Keyboard
	signal kb_rows_s			: std_logic_vector( 7 downto 0);
	signal kb_columns_s		: std_logic_vector( 4 downto 0);
	signal FKeys_s				: std_logic_vector(12 downto 1);
	signal keymap_addr_s		: std_logic_vector( 8 downto 0);
	signal keymap_data_s		: std_logic_vector( 8 downto 0);
	signal keymap_we_s		: std_logic;

	-- SPI e EPCS
	signal spi_mosi_s			: std_logic;
	signal spi_sclk_s			: std_logic;
	signal flash_miso_s		: std_logic;
	signal flash_cs_n_s		: std_logic;

	-- Video and scandoubler
	signal rgb_r_s				: std_logic_vector( 2 downto 0);
	signal rgb_g_s				: std_logic_vector( 2 downto 0);
	signal rgb_b_s				: std_logic_vector( 2 downto 0);
	signal rgb_hs_n_s			: std_logic;
	signal rgb_vs_n_s			: std_logic;
	signal rgb_comb_s			: std_logic_vector( 8 downto 0);
	signal rgb_out_s			: std_logic_vector( 8 downto 0);
	signal scandbl_en_s		: std_logic;
	signal hsync_out_s		: std_logic;
	signal vsync_out_s		: std_logic;
	signal scanlines_s		: std_logic := '0';

begin

	--------------------------------
	-- PLL
	--  48 MHz input
	--  28 MHz master clock output
	--------------------------------
	pll: entity work.pll1
	port map (
		inclk0	=> clock_48M_i,			-- Clock 48 MHz externo
		c0			=> clock_master_s,		-- Saida 0 =  28 MHz
		c1			=> clock_sdram_s,			-- Saida 1 = 112 MHz 0º
		c2			=> sdram_clock_o,			-- Saida 2 = 112 MHz -90º
		locked	=> pll_locked_s			-- Sinal de travamento (1 = PLL travado e pronto)
	);

	-- The TBBlue
	tbblue1 : entity work.tbblue
	generic map (
		usar_turbo		=> false,
		num_maquina		=> X"07",		-- 7 = ZR Tech WXEDA
		versao			=> X"18",		-- 1.08
		usar_kempjoy	=> '0',
		usar_keyjoy		=> '0',
		use_overlay_g	=> false,
		use_sprites_g	=> false,
		use_turbosnd_g	=> false,
		use_sid_g		=> false,
		use_1024kb_g	=> false,
		use_turbo28_g	=> false
	)
	port map (
		-- Clock
		iClk_master			=> clock_master_s,
		oClk_vid				=> open,--clock_video_s,

		-- Reset
		iPowerOn				=> poweron_s,
		iHardReset			=> hard_reset_s,
		iSoftReset			=> soft_reset_s,
		oSoftReset			=> int_soft_reset_s,

		-- Keys
		iKey50_60hz			=> FKeys_s(3),
		iKeyScanDoubler	=> FKeys_s(2),
		iKeyScanlines		=> FKeys_s(7),
		iKeyDivMMC			=> FKeys_s(10) or not keys_n_i(1),
		iKeyMF				=> FKeys_s(9)  or not keys_n_i(2),
		iKeyTurbo			=> FKeys_s(8),
		iKeysHard			=> (others => '0'),

		-- Keyboard
		oRows					=> kb_rows_s,
		iColumns				=> kb_columns_s,
		oPort254_cs			=> open,
		oKeymap_addr		=> keymap_addr_s,
		oKeymap_data		=> keymap_data_s,
		oKeymap_we			=> keymap_we_s,

		-- RGB
		oRGB_r				=> rgb_r_s,
		oRGB_g				=> rgb_g_s,
		oRGB_b				=> rgb_b_s,
		oRGB_hs_n			=> rgb_hs_n_s,
		oRGB_vs_n			=> rgb_vs_n_s,
		oRGB_cs_n			=> open,
		oRGB_hb_n			=> open,
		oRGB_vb_n			=> open,
		oScandbl_en			=> scandbl_en_s,
		oScandbl_sl			=> scanlines_s,
		oMachTiming			=> open,
		oNTSC_PAL			=> open,

		-- VRAM
		oVram_a				=> vram_addr_s,
		iVram_dout			=> vram_dout_s,
		oVram_cs				=> vram_cs_s,
		oVram_rd				=> vram_oe_s,

		-- Bootrom
		oBootrom_en			=> open,
		oRom_a				=> rom_addr_s,
		iRom_dout			=> rom_data_s,
		oMultiboot			=> open,

		-- RAM
		oRam_a				=> ram_addr_s,
		oRam_din				=> ram_din_s,
		iRam_dout			=> ram_dout_s,
		oRam_cs				=> ram_cs_s,
		oRam_rd				=> ram_oe_s,
		oRam_wr				=> ram_we_s,

		-- SPI (SD and Flash)
		oSpi_mosi			=> spi_mosi_s,
		oSpi_sclk			=> spi_sclk_s,
		oSD_cs_n				=> sd_cs_n_o,
		iSD_miso				=> sd_miso_i,
		oFlash_cs_n			=> flash_cs_n_s,
		iFlash_miso			=> flash_miso_s,

		-- Sound
		iEAR					=>	ear_s,
		oSPK					=>	spk_s,
		oMIC					=>	mic_s,
		oPSG_L				=>	psg_L_s,
		oPSG_R				=>	psg_R_s,
		oSID_L				=> sid_L_s,
		oSID_R				=> sid_R_s,
		oCovox_L				=> covox_L_s,
		oCovox_R				=> covox_R_s,
		oDAC					=> open,

		-- Joystick
		-- order: Fire2, Fire, Up, Down, Left, Right
		iJoy0					=> (others => '0'),
		iJoy1					=> (others => '0'),

		-- Mouse
		iMouse_en			=> '0',
		iMouse_x				=>	(others => '0'),
		iMouse_y				=>	(others => '0'),
		iMouse_bts			=>	(others => '0'),
		iMouse_wheel		=>	(others => '0'),
		oPS2mode				=> open,

		-- Lightpen
		iLp_signal			=> '0',
		oLp_en				=> open,

		-- RTC
		ioRTC_sda			=> 'Z',
		ioRTC_scl			=> 'Z',

		-- Serial
		iRs232_rx			=> '0',
		oRs232_tx			=> open,
		iRs232_dtr			=> '0',
		oRs232_cts			=> open,

		-- BUS
		oCpu_a				=> open,
		oCpu_do				=> open,
		iCpu_di				=> (others => '1'),
		oCpu_mreq			=> open,
		oCpu_ioreq			=> open,
		oCpu_rd				=> open,
		oCpu_wr				=> open,
		oCpu_m1				=> open,
		iCpu_Wait_n			=> '1',
		iCpu_nmi				=> '1',
		iCpu_int_n			=> '1',
		iCpu_romcs			=> '0',
		iCpu_ramcs			=> '0',
		iCpu_busreq_n		=> '1',
		oCpu_busack_n		=> open,
		oCpu_clock			=> open,
		oCpu_halt_n			=> open,
		oCpu_rfsh_n			=> open,
		iCpu_iorqula 		=> '0',

		-- Overlay
		oOverlay_addr 		=> open,
		iOverlay_data		=> (others => '0'),
		pixel_clock_o     => open,
	
		-- Debug
		oD_leds				=> open,
		oD_reg_o				=> open,
		oD_others			=> open
	);

	sdram256mb: if not sdram64mb_g generate
		-- SDRAM
		ram : entity work.dpSDRAM256Mb
		generic map (
			freq_g	=> 96
		)
		port map (
			clock_i			=> clock_sdram_s,
			reset_i			=> reset_s,
			refresh_i		=> '1',
			-- Porta 0
			port0_cs_i		=> vram_cs_s,
			port0_oe_i		=> vram_oe_s,
			port0_we_i		=> '0',
			port0_addr_i	=> "00000" & vram_addr_s,
			port0_data_i	=> (others => '0'),
			port0_data_o	=> vram_dout_s,
			-- Porta 1
			port1_cs_i		=> ram_cs_s,
			port1_oe_i		=> ram_oe_s,
			port1_we_i		=> ram_we_s,
			port1_addr_i	=> "00000" & ram_addr_s,
			port1_data_i	=> ram_din_s,
			port1_data_o	=> ram_dout_s,
			-- SD-RAM ports
			mem_cke_o		=> sdram_cke_o,
			mem_cs_n_o		=> sdram_cs_n_o,
			mem_ras_n_o		=> sdram_ras_n_o,
			mem_cas_n_o		=> sdram_cas_n_o,
			mem_we_n_o		=> sdram_we_n_o,
			mem_udq_o		=> sdram_dqmh_o,
			mem_ldq_o		=> sdram_dqml_o,
			mem_ba_o			=> sdram_ba_o,
			mem_addr_o		=> sdram_addr_o,
			mem_data_io		=> sdram_dq_io
		);
	end generate;

	sdram64mb: if sdram64mb_g generate
		-- SDRAM
		ram : entity work.dpSDRAM64Mb
		generic map (
			freq_g	=> 96
		)
		port map (
			clock_i			=> clock_sdram_s,
			reset_i			=> reset_s,
			refresh_i		=> '1',
			-- Porta 0
			port0_cs_i		=> vram_cs_s,
			port0_oe_i		=> vram_oe_s,
			port0_we_i		=> '0',
			port0_addr_i	=> "000" & vram_addr_s,
			port0_data_i	=> (others => '0'),
			port0_data_o	=> vram_dout_s,
			-- Porta 1
			port1_cs_i		=> ram_cs_s,
			port1_oe_i		=> ram_oe_s,
			port1_we_i		=> ram_we_s,
			port1_addr_i	=> "000" & ram_addr_s,
			port1_data_i	=> ram_din_s,
			port1_data_o	=> ram_dout_s,
			-- SD-RAM ports
			mem_cke_o		=> sdram_cke_o,
			mem_cs_n_o		=> sdram_cs_n_o,
			mem_ras_n_o		=> sdram_ras_n_o,
			mem_cas_n_o		=> sdram_cas_n_o,
			mem_we_n_o		=> sdram_we_n_o,
			mem_udq_o		=> sdram_dqmh_o,
			mem_ldq_o		=> sdram_dqml_o,
			mem_ba_o			=> sdram_ba_o,
			mem_addr_o		=> sdram_addr_o(11 downto 0),
			mem_data_io		=> sdram_dq_io
		);
	end generate;

	-- PS/2 emulating speccy keyboard
	kb: entity work.ps2keyb
	generic map (
		clkfreq_g		=> 28000
	)
	port map (
		enable_i			=> '1',
		clock_i			=> clock_master_s,
		reset_i			=> poweron_s,
		--
		ps2_clk_io		=> ps2_clk_io,
		ps2_data_io		=> ps2_dat_io,
		--
		rows_i			=> kb_rows_s,
		cols_o			=> kb_columns_s,
		functionkeys_o	=> FKeys_s,
		core_reload_o	=> open,
		keymap_addr_i	=> keymap_addr_s,
		keymap_data_i	=> keymap_data_s,
		keymap_we_i		=> keymap_we_s
	);

	-- Tape In
	tapein: entity work.tlc549
	generic map (
		frequency_g			=> 28,
		sample_cycles_g	=> 28
	)
	port map (
		clock_i	 		=> clock_master_s,
		reset_i	 		=> reset_s,
		clock_o			=> open,
		data_o		 	=> tapein_s,
		adc_data_i		=> adc_data_i,
		adc_cs_n_o	 	=> adc_cs_n_o,
		adc_clk_o	  	=> adc_clock_o
	);

	-- Scandoubler with scanlines
	scandbl: entity work.scandoubler
	generic map (
		hSyncLength	=> 61,								-- 29 for 14MHz and 61 for 28MHz
		vSyncLength	=> 13,
		ramBits		=> 11									-- 10 for 14MHz and 11 for 28MHz
	)
	port map(
		clk					=> clock_master_s,		-- minimum 2x pixel clock
		hSyncPolarity		=> '0',
		vSyncPolarity		=> '0',
		enable_in			=> scandbl_en_s,
		scanlines_in		=> scanlines_s,
		video_in				=> rgb_comb_s,
		hsync_in				=> rgb_hs_n_s,
		vsync_in				=> rgb_vs_n_s,
		video_out			=> rgb_out_s,
		vsync_out			=> vsync_out_s,
		hsync_out			=> hsync_out_s
	);

	-- Boot ROM
	boot_rom: entity work.bootrom
	port map (
		clk		=> clock_master_s,
		addr		=> rom_addr_s(12 downto 0),
		data		=> rom_data_s
	);

	-- EPCS4
	epcs4: cyclone_asmiblock
	port map (
		oe          => '0',
		scein       => flash_cs_n_s,
		dclkin      => spi_sclk_s,
		sdoin       => spi_mosi_s,
		data0out    => flash_miso_s
	);

	-- glue
	poweron_s		<= '1' when pll_locked_s = '0'							else '0';
	hard_reset_s	<= '1' when FKeys_s(1) = '1' 								else '0';
	soft_reset_s	<= '1' when int_soft_reset_s = '1' or FKeys_s(4) = '1' or keys_n_i(0) = '0' 	else '0';
	reset_s			<= poweron_s or hard_reset_s or soft_reset_s;

	-- SD
	sd_mosi_o	<= spi_mosi_s;
	sd_sclk_o	<= spi_sclk_s;

	-- Audio
	audio : entity work.Audio_DAC
	port map (
		clock_i	=> clock_master_s,
		reset_i	=> reset_s,
		ear_i		=> ear_s,
		spk_i		=> spk_s,
		mic_i		=> mic_s,
		psg_L_i	=> psg_L_s,
		psg_R_i	=> psg_R_s,
		sid_L_i	=> sid_L_s,
		sid_R_i	=> sid_R_s,
		covox_L_i=> covox_L_s,
		covox_R_i=> covox_R_s,
		dac_r_o	=> audio_dac_r_o,
		dac_l_o	=> audio_dac_l_o
	);

	process (clock_master_s)
		constant HYST_C  : integer := 4;
		constant LEVEL_C : integer := 40;
	begin
		if rising_edge(clock_master_s) then
			if    ear_s = '1' and tapein_s < LEVEL_C - HYST_C then
				ear_s <= '0';
			elsif ear_s = '0' and tapein_s > LEVEL_C + HYST_C then
				ear_s <= '1';
			end if;
		end if;
	end process;

	buzzer_o	<= spk_s;
	irda_o <= ear_s;

	-- VGA (ULA and ULA+ mixer)
	rgb_comb_s <= rgb_r_s & rgb_g_s & rgb_b_s;

	vga_r_o <= rgb_out_s(8 downto 6) & rgb_out_s(8) & '0';
	vga_g_o <= rgb_out_s(5 downto 3) & rgb_out_s(5) & "00";
	vga_b_o <= rgb_out_s(2 downto 0) & rgb_out_s(2) & '0';
	vga_hs_o <= hsync_out_s;
	vga_vs_o <= vsync_out_s;

end architecture;