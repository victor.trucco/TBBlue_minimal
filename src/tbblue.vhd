--
-- TBBlue / ZX Spectrum Next project
--
-- TBBlue - Victor Trucco & Fabio Belavenuto
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_unsigned.ALL;
use work.Z80N_pack.all;


entity tbblue is
	generic (
		use_turbo		: boolean					:= false;
		machine_id_g   : unsigned(7 downto 0)	:= "00000000";
		version_g		: unsigned(7 downto 0)	:= "00000000";
		sub_version_g	: unsigned(7 downto 0)	:= "00000000";
		usar_kempjoy	: std_logic					:= '0';
		usar_keyjoy		: std_logic					:= '0';
		use_layer2_g	: boolean					:= false;
		use_sprites_g	: boolean					:= false;
		use_turbosnd_g	: boolean					:= false;
		use_sid_g		: boolean					:= false;
		use_dma_g		: boolean					:= false;
		use_z80n_g		: boolean					:= false;
		use_lightpen_g	: boolean					:= false;		
		use_UART_g		: boolean					:= true;
		use_loRes_g		: boolean					:= true;
		use_covox_g		: boolean					:= true;
		use_copper_g	: boolean					:= false
	);
	port (
		-- Clock
		iClk_28				: in std_logic;								-- 28 MHz
		iClk_28_180o		: in std_logic;								-- 28 MHz inverted
		iClk_14				: in std_logic;								-- 14 MHz
		iClk_7				: in std_logic;								-- 7 MHz
		iClk_3M5				: in std_logic;								-- 3.5 MHz no contencion
		iClk_Cpu				: in std_logic;								-- CPU clock with contention
		iClk_Psg				: in std_logic;								-- psg clock
		oCpu_clock_cont	: out  std_logic;							 -- Signal to CPU clock contention
		oTurbo_sel			: out std_logic_vector(1 downto 0); -- Selected turbo Speed
		
		-- Reset
		iPowerOn				: in  std_logic;
		iHardReset			: in  std_logic;
		iSoftReset			: in  std_logic;
		oSoftReset			: out std_logic;

		-- Keys
		iKey50_60hz			: in  std_logic;
		iKeyScanDoubler	: in  std_logic;
		iKeyScanlines		: in  std_logic;
		iKeyDivMMC			: in  std_logic;
		iKeyMF				: in  std_logic;
		iKeyTurbo			: in  std_logic;
		iKeysHard			: in  std_logic_vector(1 downto 0) := "00";

		-- Keyboard
		oRows					: out std_logic_vector(7 downto 0);
		iColumns				: in  std_logic_vector(4 downto 0);
		oPort254_cs			: out std_logic;
		oKeymap_addr		: out std_logic_vector(8 downto 0);
		oKeymap_data		: out std_logic_vector(8 downto 0);
		oKeymap_we			: out std_logic;

		-- RGB
		oRGB_r				: out std_logic_vector(2 downto 0);
		oRGB_g				: out std_logic_vector(2 downto 0);
		oRGB_b				: out std_logic_vector(2 downto 0);
		oRGB_hs_n			: out std_logic;
		oRGB_vs_n			: out std_logic;
		oRGB_cs_n			: out std_logic;
		oRGB_hb_n			: out std_logic;
		oRGB_vb_n			: out std_logic;
		oRGB_hcnt			: out unsigned(8 downto 0)				:= (others => '0');
		oRGB_vcnt			: out unsigned(8 downto 0)				:= (others => '0');
		oScandbl_en			: out std_logic;
		oScanlines			: out std_logic_vector(1 downto 0);
		oULA50_60			: out std_logic;
		oMachTiming			: out std_logic_vector(2 downto 0);
		oMachVideoTiming	: out std_logic_vector(2 downto 0);
		oNTSC_PAL			: out std_logic;

		-- VRAM
		oVram_a				: out std_logic_vector(20 downto 0);
		iVram_dout			: in  std_logic_vector(7 downto 0);
		oVram_cs				: out std_logic;
		oVram_rd				: out std_logic;

		-- Bootrom
		oBootrom_en			: out std_logic;
		oRom_a				: out std_logic_vector(13 downto 0);
		iRom_dout			: in  std_logic_vector(7 downto 0);
		oFlashboot			: out std_logic;

		-- RAM
		oRam_a				: out std_logic_vector(20 downto 0);
		oRam_din				: out std_logic_vector(7 downto 0);
		iRam_dout			: in  std_logic_vector(7 downto 0);
		oRam_cs				: out std_logic;
		oRam_rd				: out std_logic;
		oRam_wr				: out std_logic;

		-- SPI (SD and Flash)
		oSpi_mosi			: out std_logic;
		oSpi_sclk			: out std_logic;
		oSD_cs0_n			: out std_logic;
		oSD_cs1_n			: out std_logic;
		iSD_miso				: in  std_logic;
		oFlash_cs_n			: out std_logic;
		iFlash_miso			: in  std_logic;
		oFT_cs_n				: out std_logic;
		iFT_miso				: in  std_logic;
		oRPi_cs0_n			: out std_logic;
		oRPi_cs1_n			: out std_logic;
		iRPi_miso			: in  std_logic;

		-- Sound
		iEAR					: in  std_logic;
		oSPK					: out std_logic;
		oMIC					: out std_logic;
		oPSG_L				: out unsigned(10 downto 0);
		oPSG_R				: out unsigned(10 downto 0);
		oSID_L				: out unsigned(17 downto 0);
		oSID_R				: out unsigned(17 downto 0);
		oCovox_L				: out unsigned( 8 downto 0);
		oCovox_R				: out unsigned( 8 downto 0);
		oDAC					: out std_logic;
		oIntSnd				: out std_logic;

		-- Joystick
		-- order: Fire2, Fire, Up, Down, Left, Right
		iJoy0					: in  std_logic_vector(7 downto 0);
		iJoy1					: in  std_logic_vector(7 downto 0);

		-- Mouse
		iMouse_en			: in std_logic								:= '0';
		iMouse_x				: in std_logic_vector(7 downto 0);
		iMouse_y				: in std_logic_vector(7 downto 0);
		iMouse_bts			: in std_logic_vector(2 downto 0);
		iMouse_wheel		: in std_logic_vector(3 downto 0);
		oPS2mode				: out std_logic;

		-- Lightpen
		iLp_signal			: in  std_logic;
		oLp_en				: out std_logic;

		-- I2C
		ioRTC_sda			: inout std_logic;
		ioRTC_scl			: inout std_logic;
		ioPi_sda				: inout std_logic;
		ioPi_scl				: inout std_logic;

		-- Serial
		iRs232_rx			: in  std_logic							:= '1';
		oRs232_tx			: out std_logic;
		iRs232_dtr			: in  std_logic							:= '0';
		oRs232_cts			: out std_logic;

		-- BUS
		oCpu_a				: out std_logic_vector(15 downto 0);
		oCpu_do				: out std_logic_vector( 7 downto 0);
		iCpu_di				: in  std_logic_vector( 7 downto 0);
		oCpu_mreq			: out std_logic;
		oCpu_ioreq			: out std_logic;
		oCpu_rd				: out std_logic;
		oCpu_wr				: out std_logic;
		oCpu_m1				: out std_logic;
		iCpu_Wait_n			: in  std_logic;
		iCpu_nmi				: in  std_logic								:= '1';
		iCpu_int_n			: in  std_logic								:= '1';
		iCpu_romcs			: in  std_logic								:= '0';
		iCpu_ramcs			: in  std_logic								:= '0';
		iCpu_busreq_n		: in  std_logic								:= '1';
		oCpu_busack_n		: out std_logic;
		oCpu_clock			: out std_logic;
		oCpu_halt_n			: out std_logic;
		oCpu_rfsh_n			: out std_logic;
		iCpu_iorqula		: in  std_logic								:= '0';

		-- layer2
		oLayer2_addr 		: out std_logic_vector(20 downto 0);
		iLayer2_data		: in  std_logic_vector( 7 downto 0);
		oLayer2_cs			: out std_logic;
		
		-- timing
--		h_visible_o			: out std_logic_vector(9 downto 0) := (others=>'0');	
--		hsync_start_o		: out std_logic_vector(9 downto 0) := (others=>'0');	
--		hsync_end_o			: out std_logic_vector(9 downto 0) := (others=>'0');	
--		hcnt_end_o			: out std_logic_vector(9 downto 0) := (others=>'0');	
--		v_visible_o			: out std_logic_vector(9 downto 0) := (others=>'0');	
--		vsync_start_o		: out std_logic_vector(9 downto 0) := (others=>'0');	
--		vsync_end_o			: out std_logic_vector(9 downto 0) := (others=>'0');	
--		vcnt_end_o			: out std_logic_vector(9 downto 0) := (others=>'0');	
		
		-- Debug
		oD_leds				: out std_logic_vector( 7 downto 0);
		oD_reg_o				: out std_logic_vector(15 downto 0);
		oD_others			: out std_logic_vector( 7 downto 0);
		iSW					: in std_logic_vector( 9 downto 0);
		
		iResetUlaCnt		: in  std_logic								:= '0'
	);
end entity;

architecture Behavior of tbblue is
	
	-- Clock control
--	signal iClk_cpu				: std_logic;							-- Clock  3.5 MHz para a CPU (contida pela ULA)
--	signal clk_vid				: std_logic;							-- Clock   14 MHz para a ULA
--	signal iClk_3M5				: std_logic;

	-- Portas config, reset e bootrom
	signal portE3_rd_s		: std_logic;
	signal port243B_rd_s		: std_logic;
	signal port243B_wr_s		: std_logic;
	signal port253B_rd_s		: std_logic;
	signal port253B_wr_s		: std_logic;
	signal register_q			: std_logic_vector(7 downto 0)	:= (others => '0');
	signal bootrom_s			: std_logic								:= '1';
	signal register_data_s	: std_logic_vector(7 downto 0)	:= (others => '0');
--	signal reg00_data_s		: std_logic_vector(7 downto 0)	:= (others => '0');
--	signal reg01_data_s		: std_logic_vector(7 downto 0)	:= (others => '0');
	signal reg02_data_s		: std_logic_vector(7 downto 0)	:= "00000100";
--	signal reg05_data_s		: std_logic_vector(7 downto 0)	:= (others => '0');
--	signal reg06_data_s		: std_logic_vector(7 downto 0)	:= (others => '0');
--	signal reg10_data_s		: std_logic_vector(7 downto 0)	:= (others => '0');
--	signal reg21_data_s		: std_logic_vector(7 downto 0);
--	signal reg22_data_s		: std_logic_vector(7 downto 0)	:= (others => '0');
--	signal reg23_data_s		: std_logic_vector(7 downto 0)	:= (others => '0');
--	signal reg30_data_s		: std_logic_vector(7 downto 0)	:= (others => '0');
--	signal reg31_data_s		: std_logic_vector(7 downto 0)	:= (others => '0');
--	signal reg34_data_s		: std_logic_vector(7 downto 0);

	signal sprite_border_q	: std_logic								:= '0';
	signal sprite_visible_q	: std_logic								:= '0';
	
	signal write_NextReg_en_s	: std_logic;

	-- Turbo
	signal turbo_s				: std_logic_vector(1 downto 0);
	signal turbo_v				: std_logic_vector(1 downto 0);
	signal s_ena_turbo		: std_logic;

	-- Reset signals
	signal poweron_n			: std_logic;
	signal softreset_s		: std_logic;
	signal hardreset_s		: std_logic;
	signal reset_s				: std_logic;
	signal reset_n				: std_logic;
	signal flashboot_q		: std_logic								:= '0';
	signal softreset_q		: std_logic;
	signal hardreset_q		: std_logic;
	signal softreset_p_q		: std_logic;
	signal hardreset_p_q		: std_logic;
	signal poweron_cnt		: unsigned(3 downto 0)				:= (others => '1');
	signal softreset_cnt		: unsigned(3 downto 0)				:= (others => '0');
	signal hardreset_cnt		: unsigned(3 downto 0)				:= (others => '0');

	-- CPU signals
	signal cpu_wait_n			: std_logic;								-- /WAIT
	signal cpu_irq_n			: std_logic;								-- /IRQ
	signal cpu_nmi_n			: std_logic;								-- /NMI
--	signal cpu_busreq_n		: std_logic;								-- /BUSREQ
	signal cpu_m1_n			: std_logic;								-- /M1
	signal cpu_mreq_n			: std_logic;								-- /MREQ
	signal cpu_ioreq_n		: std_logic;								-- /IOREQ
	signal cpu_rd_n			: std_logic;								-- /RD
	signal cpu_wr_n			: std_logic;								-- /WR
	signal cpu_a				: std_logic_vector(15 downto 0);		-- A
	signal cpu_di				: std_logic_vector(7 downto 0);		-- D in
	signal cpu_do				: std_logic_vector(7 downto 0);		-- D out
	signal cpu_d_s				: std_logic_vector(7 downto 0);
		
	-- ULA
	signal ula_din				: std_logic_vector(7 downto 0);
	signal ula_dout			: std_logic_vector(7 downto 0);
	signal ula_hd_s			: std_logic;								-- ULA has data to send
	signal vram_a				: std_logic_vector(15 downto 0);
	signal vram_dout			: std_logic_vector(7 downto 0);
	signal vram_oe				: std_logic;
	signal vram_cs				: std_logic;
	signal vram_shadow      : std_logic;
	signal ram_contention_s	: std_logic;
	signal ula_ear				: std_logic;
	signal ula_mic				: std_logic;
	signal ula_spk				: std_logic;
	signal ula_r				: std_logic_vector(2 downto 0);
	signal ula_g				: std_logic_vector(2 downto 0);
	signal ula_b				: std_logic_vector(2 downto 0);
	signal ula_attr_s			: std_logic_vector(8 downto 0);
	signal ula50_60hz			: std_logic;
	signal ula_hsync_n		: std_logic;
	signal ula_vsync_n		: std_logic;
	signal ula_int_n			: std_logic;
	signal s_ula_timex_en	: std_logic;
	signal timex_hires_s		: std_logic;
	signal ULAnext_value_s	: std_logic_vector(7 downto 0);

	-- ULA Line Interrupt control
	signal lineint_ctrl_s	: std_logic_vector(1 downto 0);
	signal lineint_line_s	: std_logic_vector(8 downto 0);
	
	signal disable_contention_q			: std_logic := '0';

	-- Joystick
	signal joy0_mode			: std_logic_vector(2 downto 0);
	signal joy1_mode			: std_logic_vector(2 downto 0);
	signal joy_columns		: std_logic_vector(4 downto 0);

	-- Sound
	signal psg_do				: std_logic_vector(7 downto 0)	:= "11111111";
	signal psg_dout			: std_logic;
	signal psg_mode			: std_logic_vector(1 downto 0)	:= "10";
	signal s_dac				: std_logic 							:= '0'; -- 0 = I2S, 1 = JAP
	signal s_turbosound		: std_logic								:= '0';
	signal exp_psg_s			: std_logic_vector(7 downto 0);
	signal covox_en_q			: std_logic;
	signal intsnd_en_q		: std_logic;
	signal stereo_mode_q		: std_logic								:= '0'; -- 0 = ABC, 1 = ACB

	-- Memory buses
	signal ram_addr			: std_logic_vector(20 downto 0);				-- RAM absolute address
	signal ram_din				: std_logic_vector(7 downto 0);
	signal ram_dout			: std_logic_vector(7 downto 0);
	signal rom_dout			: std_logic_vector(7 downto 0);				-- ROM boot

	-- Memory and I/Os enables
	-- RAM bank actually being accessed
	signal ram_page			: std_logic_vector(6 downto 0);				-- Bits altos do endereco absoluto da RAM
	signal iocs_en				: std_logic;
	signal iord_en				: std_logic;										-- Leitura em alguma porta de I/O
	signal iowr_en				: std_logic;										-- Escrita em alguma porta de I/O
	signal romboot_en			: std_logic;										-- BOOTROM acessada
	signal romram_en			: std_logic;										-- ROM emulada em RAM acessada
	signal romram_wr_en		: std_logic;										-- ROM emulada em RAM acessada (escrita somente modo config)
	signal ram_en				: std_logic;										-- RAM acessada (R/W)
	signal vram_en				: std_logic;										-- VRAM acessada na pagina 1 (0x4000 a 0x5FFF)
	signal ula_en				: std_logic;										-- Porta 254 da ULA acessada
	signal s_ntsc_pal			: std_logic								:= '0';	-- NTSC

	signal port7FFD_wr_s		: std_logic;										-- Escrita na porta 7FFD (controle do speccy 128)
	signal port7FFD_reg		: std_logic_vector(7 downto 0);				-- Registro que guarda o valor escrito na porta 7FFD
	signal port1FFD_wr_s		: std_logic;										-- Escrita na porta 1FFD (controle do speccy +3)
	signal port1FFD_reg		: std_logic_vector(7 downto 0);				-- Registro que guarda o valor escrito na porta 1FFD
	signal portDFFD_wr_s		: std_logic;										-- Escrita na porta DFFD (ZX PROFI 1024kb)
	signal portDFFD_reg		: std_logic_vector(7 downto 0);				-- Registro que guarda o valor escrito na porta DFFD

	signal port123b_wr_s		: std_logic;										
	signal port123b_rd_s		: std_logic;										
	signal port123b_reg_s	: std_logic_vector(7 downto 0);				

	-- Modo config
	signal romram_page		: std_logic_vector(4 downto 0);				-- Pagina de 16K mapeada na area da ROM (0000-3FFF)

	-- Speccy 128K
	signal s128_disable		: std_logic;										-- Bit 5 porta 7FFD - Desabilitar a escrita na porta 7FFD
	signal s128_rom_page		: std_logic;										-- Bit 4 porta 7FFD - Qual pagina da ROM do speccy128 mapear
	signal s128_shadow		: std_logic;										-- Bit 3 porta 7FFD - Qual pagina de video a ULA deve mostrar
	signal s128_ram_page		: std_logic_vector(2 downto 0);				-- Bits 2..0 porta 7FFD - Banco da RAM para ser mapeada na pagina 3 (0xC000 - 0xFFFF)

	-- Speccy +3e:
	signal plus3_page					: std_logic_vector(1 downto 0);		-- bits 2..1 da porta 1FFD
	signal plus3_special				: std_logic;								-- bit 0 da porta 1FFD

	-- Machine type and timing
	type machine_type_t is (s_config, s_speccy48, s_speccy128, s_speccy3e);
	signal machine        			: machine_type_t;
	signal machine_s					: std_logic_vector(2 downto 0);
	signal machine_timing			: std_logic_vector(2 downto 0);
	signal machine_video_timing	: std_logic_vector(2 downto 0) := "000";

	-- Divmmc
	signal s_divmmc_enabled			: std_logic		:= '0';
	signal divmmc_no_automap		: std_logic;
	signal divmmc_do					: std_logic_vector(7 downto 0);
	signal divmmc_ram_en				: std_logic;
	signal divmmc_disable_nmi		: std_logic;
	signal divmmc_bank				: std_logic_vector(5 downto 0);
	signal divmmc_rom_en				: std_logic;
	signal divmmc_dout				: std_logic;
	signal s_nmi						: std_logic;
	signal s_button_divmmc			: std_logic;
	signal wait_n_s					: std_logic;
	signal divmmc_e3_s				: std_logic_vector(7 downto 0);
	signal divmmc_conmem_s			: std_logic;

	-- SPI
	signal sd_cs0_n					: std_logic;
	signal sd_cs1_n					: std_logic;
	signal ft_cs_n					: std_logic;
	signal spi_cs_n					: std_logic;
	signal spi_mosi					: std_logic;
	signal spi_miso					: std_logic;
	signal spi_sclk					: std_logic;
	signal rpi_cs0_n					: std_logic;
	signal rpi_cs1_n					: std_logic;
	
	-- Scandoubler
	signal scanlines_s				: std_logic_vector(1 downto 0);
	signal scandbl_en					: std_logic;

	-- Debounce chaves externas
	signal db1							: std_logic_vector(1 downto 0);
	signal db2							: std_logic_vector(1 downto 0);
	signal db3							: std_logic_vector(1 downto 0);
	signal db4							: std_logic_vector(1 downto 0);

	-- Kempston
	signal kempjoy_enable_s			: std_logic;
	signal kempston_dout				: std_logic_vector(7 downto 0);
	signal joy_pins 					: std_logic_vector(7 downto 0);
	signal s_kj_out					: std_logic;

	-- Multiface One
	signal s_m1_enabled				: std_logic := '0';
	signal s_m1_rom_cs				: std_logic := '1';
	signal s_m1_ram_cs				: std_logic := '1';
	signal s_nmi_m1					: std_logic := 'Z';
	signal s_multiface_en			: std_logic := '0';
	signal s_button_m1				: std_logic := '1';
	signal s_m1_do						: std_logic_vector(7 downto 0);
	signal s_m1_dout					: std_logic := '0';

	-- Mouse
	signal s_mouse_d 					: std_logic_vector(7 downto 0);
	signal s_mouse_out				: std_logic :='0';
	signal s_ps2_dat					: std_logic;
	signal s_ps2_clk					: std_logic;
	signal s_ps2_mode					: std_logic				:= '0';

	-- Light Pen
	signal s_lp_d0						: std_logic;
	signal s_lp_out					: std_logic;
	signal s_lp_enabled				: std_logic				:= '0';

--	signal s_ay_select : std_logic				:= '1';

	-- BUS
	signal bus_int_n_s				: std_logic;
	signal bus_romcs_s				: std_logic;
	signal bus_ramcs_s				: std_logic;
	signal bus_busreq_n_s			: std_logic;
	signal bus_iorqula_s				: std_logic;

	-- RTC
	signal port103B_en_s				: std_logic;
	signal port113B_en_s				: std_logic;
	signal rtc_scl_s					: std_logic;
	signal rtc_sda_s					: std_logic;
	
	-- UART
	signal port143B_wr_s				: std_logic; -- RX
	signal port143B_rd_s				: std_logic; -- RX
	signal port133B_wr_s				: std_logic; -- TX
	signal port133B_rd_s				: std_logic; -- TX

	-- layer2
	signal transparency_q			: std_logic_vector(7 downto 0)	:= "11100011"; --E3
	signal transparency_fallback_s : std_logic_vector(7 downto 0)	:= (others => '0'); -- Black
	signal layer2_addr_s			: std_logic_vector(15 downto 0)	:= (others => '0');
	signal hcount_s				: unsigned(8 downto 0)				:= (others => '0');
	signal vcount_s				: unsigned(8 downto 0)				:= (others => '0');
	signal layer2_off_X_s			: std_logic_vector(7 downto 0)	:= (others => '0');
	signal layer2_off_Y_s			: std_logic_vector(7 downto 0)	:= (others => '0');
	signal layer2_R_s				: std_logic_vector(2 downto 0)	:= "000";
	signal layer2_G_s				: std_logic_vector(2 downto 0)	:= "000";
	signal layer2_B_s				: std_logic_vector(2 downto 0)	:= "000";
	signal layer2_subpage_s		: std_logic_vector(1 downto 0)	:= "00";
	signal layer2_write_shadow_s	: std_logic								:= '0';
	signal layer2_access_type_s	: std_logic								:= '0';
	signal layer2_visible_en_s	: std_logic								:= '0';
	signal layer2_access_en_s 	: std_logic								:= '0';
	signal ULA_rgb_transp_s 	: std_logic								:= '0';
	signal layer2_en				: std_logic;
	signal layer2_page_s			: std_logic_vector(5 downto 0)	:= "001000"; -- page 8
	signal layer2_page_shadow_s	: std_logic_vector(5 downto 0)	:= "001011"; -- page 11
	signal layer2_blank_s			: std_logic								:= '1';
	signal layer2_cs_s 				: std_logic;
	signal clip_layer2_x1			: std_logic_vector(7 downto 0);
	signal clip_layer2_x2			: std_logic_vector(7 downto 0);
	signal clip_layer2_y1			: std_logic_vector(7 downto 0);
	signal clip_layer2_y2			: std_logic_vector(7 downto 0);
	signal clip_layer2_idx_s  			: std_logic_vector(1 downto 0) := "00";
	signal clip_layer2_we_s 		: std_logic;
	signal ov_clk_cs_s				: std_logic := '0';
	
	-- Sprite
	signal sprite_hc_s				: unsigned( 8 downto 0);
	signal sprite_vc_s				: unsigned( 8 downto 0);
	signal sprite_colour_s			: std_logic_vector( 7 downto 0);
	signal sprite_RGB_s				: std_logic_vector( 8 downto 0);
	signal sprite_pixel_s			: std_logic								:= '0';
	signal sprite_hd_s				: std_logic								:= '0';
	signal sprite_data_s				: std_logic_vector( 7 downto 0);
	signal low_b_s						: std_logic;	
	signal clip_sprites_x1			: std_logic_vector(7 downto 0);
	signal clip_sprites_x2			: std_logic_vector(7 downto 0);
	signal clip_sprites_y1			: std_logic_vector(7 downto 0);
	signal clip_sprites_y2			: std_logic_vector(7 downto 0);
	signal clip_sprites_idx_s  			: std_logic_vector(1 downto 0) := "00";
	signal clip_sprites_we_s 		: std_logic;
	signal transparency_index_s	: std_logic_vector(7 downto 0)	:= "11100011"; --E3

	-- RGB
	signal rgb_r_s						: std_logic_vector(2 downto 0);
	signal rgb_g_s						: std_logic_vector(2 downto 0);
	signal rgb_b_s						: std_logic_vector(2 downto 0);
	signal hblank_n_s					: std_logic;
	signal vblank_n_s					: std_logic;
	signal layer_priorities_s		: std_logic_vector(2 downto 0);
	
	--Palette control
	signal palette_we_value_s			: std_logic_vector(7 downto 0);
	signal palette_index_s			: std_logic_vector(7 downto 0);
	
	signal palette_we_addr_s			: std_logic_vector(7 downto 0);
	signal palette_we_sel_s		: std_logic_vector(2 downto 0) := "000";	
	
	signal palette_no_auto_inc_s	: std_logic		:= '0';
	signal palette_we_s				: std_logic		:= '0';
	signal palette_weLB_s			: std_logic		:= '0';
	signal disable_flash_s			: std_logic		:= '0';

	signal palette_addr_s			: std_logic_vector(7 downto 0);
	signal palette_addr_ulanext_s	: std_logic_vector(7 downto 0);
	signal palette_addr_lores_s	: std_logic_vector(7 downto 0);
	signal palette_rgb_ula_s		: std_logic_vector(8 downto 0);
	signal palette_out_ula_s		: std_logic_vector(8 downto 0);
	
	signal palette_addr_lay2_s		: std_logic_vector(7 downto 0);
	signal palette_rgb_lay2_s		: std_logic_vector(8 downto 0);
	signal palette_out_lay2_s		: std_logic_vector(8 downto 0);

	signal palette_addr_spr_s		: std_logic_vector(7 downto 0);
	signal palette_rgb_spr_s		: std_logic_vector(8 downto 0);
	signal palette_out_spr_s		: std_logic_vector(8 downto 0);

	signal palette_sel_ula_s		: std_logic		:= '0';
	signal palette_sel_lay2_s		: std_logic		:= '0';
	signal palette_sel_spr_s		: std_logic		:= '0';
	
	signal palette_we_ula_s  		: std_logic		:= '0';
	signal palette_we_lay2_s 		: std_logic		:= '0';
	signal palette_we_spr_s  		: std_logic		:= '0';
	signal palette_reg44_sel_s	: std_logic		:= '0';
	signal palette_reg44_we_s		: std_logic		:= '0';
	

	

	-- Keymap
	signal keymap_addr_q				: unsigned(8 downto 0);
	signal keymap_data_q				: std_logic_vector(8 downto 0);
	signal keymap_we_s				: std_logic;

	-- DMA
	signal dma_busreq_s				: std_logic		:= '1';
	signal cpu_busack_n_s			: std_logic;

	signal dma_a_s						: std_logic_vector(15 downto 0);
	signal dma_d_s						: std_logic_vector(7 downto 0);
	signal dma_rd_n_s					: std_logic;
	signal dma_wr_n_s					: std_logic;
	signal dma_mreq_n_s				: std_logic;
	signal dma_ioreq_n_s				: std_logic;
	signal dma_out_s					: std_logic;
	signal dma_data_s					: std_logic_vector(7 downto 0);

	signal z80_a						: std_logic_vector(15 downto 0);
	signal z80_do						: std_logic_vector(7 downto 0);
	signal z80_rd_n					: std_logic;
	signal z80_wr_n					: std_logic;
	signal z80_mreq_n					: std_logic;
	signal z80_ioreq_n				: std_logic;
	
	-- extended Z80 functions
	signal Z80N_dout_s			: std_logic := '0';
	signal Z80N_data_s			: std_logic_vector(15 downto 0);
	signal Z80N_command_s		: Z80N_seq;
	
	--LoRes
	signal LoRes_en_s				: std_logic := '0';
	signal LoRes_visible_en_s 	: std_logic := '0';
	signal LoRes_off_X_s 		: std_logic_vector(7 downto 0) := (others=>'0');
	signal LoRes_off_Y_s			: std_logic_vector(7 downto 0) := (others=>'0');
	signal LoRes_addr_s			: std_logic_vector(15 downto 0) := (others=>'0');
	signal LoRes_data  			: std_logic_vector(7 downto 0) := (others=>'0');
	signal LoRes_R_s   			: std_logic_vector(2 downto 0) := "000";
	signal LoRes_G_s   			: std_logic_vector(2 downto 0) := "000";
	signal LoRes_B_s   			: std_logic_vector(2 downto 0) := "000";
	signal LoRes_blank_s			: std_logic := '0';
	signal clip_ULA_x1			: std_logic_vector(7 downto 0);
	signal clip_ULA_x2			: std_logic_vector(7 downto 0);
	signal clip_ULA_y1			: std_logic_vector(7 downto 0);
	signal clip_ULA_y2			: std_logic_vector(7 downto 0);
	signal clip_ULA_idx_s  		: std_logic_vector(1 downto 0) := "00";
	signal clip_ULA_we_s 		: std_logic;
	signal cliped_s 				: std_logic;
	
	--MMU
	signal MMU0_s			: std_logic_vector(7 downto 0) := X"FF"; -- page mapped from 0x0000 to 0x1fff
	signal MMU1_s			: std_logic_vector(7 downto 0) := X"FF"; -- page mapped from 0x2000 to 0x3fff
	signal MMU2_s			: std_logic_vector(7 downto 0) := X"0A"; -- page mapped from 0x4000 to 0x5fff
	signal MMU3_s			: std_logic_vector(7 downto 0) := X"0B"; -- page mapped from 0x6000 to 0x7fff
	signal MMU4_s			: std_logic_vector(7 downto 0) := X"04"; -- page mapped from 0x8000 to 0x9fff
	signal MMU5_s			: std_logic_vector(7 downto 0) := X"05"; -- page mapped from 0xa000 to 0xbfff
	signal MMU6_s			: std_logic_vector(7 downto 0) := X"00"; -- page mapped from 0xc000 to 0xdfff
	signal MMU7_s			: std_logic_vector(7 downto 0) := X"01"; -- page mapped from 0xe000 to 0xffff
	
	-- UART

	signal uart_speed_s			: natural;
	signal uart_tx_start_s		: std_logic := '0';	
	signal uart_rx_finished_s	: std_logic := '0';	
	signal uart_rx_byte_s		: std_logic_vector(7 downto 0);	
	signal uart_tx_byte_s		: std_logic_vector(7 downto 0);
	signal fifo_next_byte_s	: std_logic_vector(7 downto 0);
	signal uart_rx_ready_s		: std_logic := '0';
	signal fifo_empty_s		: std_logic := '0';
	signal fifo_full_s		: std_logic := '0';
	signal uart_tx_active_s		: std_logic := '0';
	signal head_s					: unsigned(7 downto 0);
	
	signal uart_prescaler_s 	: std_logic_vector(13 downto 0) := "00000011110011"; -- 243 - vga 0 timing at 115.200
	
	-- Copper
	signal copper_en_s			: std_logic_vector(1 downto 0) := (others=>'0');
 	signal copper_we_s			: std_logic := '0';
	signal copper_contention_s	: std_logic := '0';
 	signal copper_we_addr_s		: std_logic_vector(10 downto 0) := (others=>'0');	
 	signal copper_di_s			: std_logic_vector(7 downto 0);	
 	signal copper_addr_s			: std_logic_vector(9 downto 0);	
 	signal copper_do_s			: std_logic_vector(15 downto 0);	
	signal copper_dout_s			: std_logic := '0';
	signal copper_data_s			: std_logic_vector(14 downto 0);	
	
	signal copper_delayed_s			: std_logic := '0';
	signal copper_d1_s			: std_logic := '0';
	signal copper_delayed_data_s		: std_logic_vector(14 downto 0);
	
	-- Covox
	 signal covox_we_s			: std_logic := '0';
	 signal covox_data_s			: std_logic_vector(7 downto 0);	
 
	-- Timing
--	signal timing_hi_s			: std_logic := '0';
--	signal timing_idx_s  		: std_logic_vector(5 downto 0) := (others=>'0');	
	
		--for external timing.ini reading
	-- Timings
--	signal hblank_min_s	: std_logic_vector(8 downto 0) := (others=>'0');		
--	signal hsync_min_s	: std_logic_vector(8 downto 0) := (others=>'0');		
--	signal hsync_max_s	: std_logic_vector(8 downto 0) := (others=>'0');		
--	signal hblank_max_s	: std_logic_vector(8 downto 0) := (others=>'0');		
--	signal max_hc_s		: std_logic_vector(8 downto 0) := (others=>'0');		
--	signal vblank_min_s	: std_logic_vector(8 downto 0) := (others=>'0');		
--	signal vsync_min_s	: std_logic_vector(8 downto 0) := (others=>'0');		
--	signal vsync_max_s	: std_logic_vector(8 downto 0) := (others=>'0');		
--	signal vblank_max_s	: std_logic_vector(8 downto 0) := (others=>'0');		
--	signal max_vc_s		: std_logic_vector(8 downto 0) := (others=>'0');		
--		
--	type timing_t is array (0 to 35) of std_logic_vector(9 downto 0);
--	signal timing_q : timing_t := (
--	
--		
--		-- TV 50hz	
--		"1011001111", -- 720 - 1;
--		"1011011011", -- 732 - 1;
--		"1100011011", -- 796 - 1;
--		"1101011111", -- 864 - 1;
--		--			
--		"1000111111", -- 576 - 1;
--		"1001000100", -- 581 - 1;
--		"1001001001", -- 586 - 1;
--		"1001101111", -- 625 - 2;
--				
--		-- TV 60hz
--		"1011001111", -- 720 - 1;
--		"1011011111", -- 736 - 1;
--		"1100011101", -- 798 - 1;
--		"1101011010", -- 858 - 1;
--		--
--		"0111011111", -- 480 - 1;
--		"0111101000", -- 489 - 1;
--		"0111101110", -- 495 - 1;
--		"1000001011", -- 525 - 2;
--		
--		-- ULA 50Hz
--		"0101000011", --323
--		"0101001001", --329
--		"0101101001", --361
--		"0110001010", --394
--		"0110101111", --431
--		--
--		"0100000000", --256
--		"0100000000", --256
--		"0100000011", --259
--		"0100000111", --263
--		"0100110111", --311
--
--		-- ULA 60Hz
--		"0101000011", --323
--		"0101001011", --331
--		"0101101010", --362
--		"0110000111", --391
--		"0110101100", --428
--		--
--		"0011100111", --231
--		"0011101011", --235
--		"0011101110", --238
--		"0011101110", --238
--		"0100000101"  --261
--
--	);

	 signal register_temp			: std_logic_vector(7 downto 0);	
		signal data_temp			: std_logic_vector(7 downto 0);	
	
begin



	-- CPU
	cpu: if use_z80n_g generate
	cpu: entity work.T80na
	generic map (
		Mode 			=> 0
	)
	port map (
		RESET_n		=> reset_n,
		CLK_n			=> iClk_Cpu,
		WAIT_n		=> cpu_wait_n,
		INT_n			=> cpu_irq_n,
		NMI_n			=> cpu_nmi_n,
		BUSRQ_n		=> dma_busreq_s,--bus_busreq_n_s,
		M1_n			=> cpu_m1_n,
		MREQ_n		=> z80_mreq_n,
		IORQ_n		=> z80_ioreq_n,
		RD_n			=> z80_rd_n,
		WR_n			=> z80_wr_n,
		RFSH_n		=> oCpu_rfsh_n,
		HALT_n		=> oCpu_halt_n,
		BUSAK_n		=> cpu_busack_n_s,
		A				=> z80_a,
		D				=> cpu_d_s,
		
		-- extended functions
		Z80N_dout_o			=> Z80N_dout_s,
		Z80N_data_o			=> Z80N_data_s,
		Z80N_command_o		=> Z80N_command_s
	
	);
	end generate;
	
	
	gcpu2: if not use_z80n_g generate
	cpu: entity work.T80a
	generic map 
	(
		Mode 			=> 0
	)
	port map 
	(
		RESET_n		=> reset_n,
		CLK_n			=> iClk_Cpu,
		WAIT_n		=> cpu_wait_n,
		INT_n			=> cpu_irq_n,
		NMI_n			=> cpu_nmi_n,
		BUSRQ_n		=> dma_busreq_s,--bus_busreq_n_s,
		M1_n			=> cpu_m1_n,
		MREQ_n		=> z80_mreq_n,
		IORQ_n		=> z80_ioreq_n,
		RD_n			=> z80_rd_n,
		WR_n			=> z80_wr_n,
		RFSH_n		=> oCpu_rfsh_n,
		HALT_n		=> oCpu_halt_n,
		BUSAK_n		=> cpu_busack_n_s,
		A				=> z80_a,
		D				=> cpu_d_s
	);
	end generate;
	
	
	
	
	
	
	

	oCpu_busack_n <= cpu_busack_n_s;

	z80_do	<= cpu_d_s;
	cpu_d_s	<= cpu_di				when (cpu_mreq_n = '0' or cpu_ioreq_n = '0') and cpu_rd_n = '0'	else
					(others => 'Z')	when (cpu_mreq_n = '0' or cpu_ioreq_n = '0') and cpu_wr_n = '0'	else
					(others => '1')	when cpu_m1_n = '0' and cpu_ioreq_n = '0'									else	-- IM 2
					iCpu_di;

	cpu_wait_n	<= iCpu_Wait_n when machine /= s_config else '1';
--	cpu_wait_n	<= iCpu_Wait_n and wait_n_s when machine /= s_config else '1';

	cpu_nmi_n	<= '0' when iCpu_nmi = '0'                              and reset_s = '0' and machine /= s_config	else
						 '0' when s_nmi = '0'    and divmmc_no_automap = '0' and reset_s = '0' and machine /= s_config	else
						 '0' when s_nmi_m1 = '0' and divmmc_no_automap = '1' and reset_s = '0' and machine /= s_config  else
					    '1';
	bus_int_n_s <= iCpu_int_n when machine /= s_config else '1';
	cpu_irq_n	<= bus_int_n_s and ula_int_n;

	-- DMA
	udma: if use_dma_g generate
		dma: entity work.z80dma
		port map (
			reset_i			=> reset_s,
			mode_i			=> '1',
			clk_i				=> iClk_Cpu,--iClk_14,--iClk_Cpu,
			cpu_a_i			=> z80_a(7 downto 0),
			cpu_d_i			=> z80_do,
			cpu_rd_n_i		=> z80_rd_n,
			cpu_wr_n_i		=> z80_wr_n,
			cpu_mreq_n_i	=> z80_mreq_n,
			cpu_ioreq_n_i	=> z80_ioreq_n,
			cpu_m1_i			=> cpu_m1_n,
			wait_n_i			=> wait_n_s,

			cpu_busack_n_i	=> cpu_busack_n_s,
			cpu_busreq_n_o	=> dma_busreq_s,

			dma_a_o			=> dma_a_s,
			dma_d_o			=> dma_d_s,
			dma_d_i			=> cpu_di,
			dma_rd_n_o		=> dma_rd_n_s,
			dma_wr_n_o		=> dma_wr_n_s,
			dma_mreq_n_o	=> dma_mreq_n_s,
			dma_ioreq_n_o	=> dma_ioreq_n_s,

			cpu_d_o			=> dma_data_s,
			dma_out_o		=> dma_out_s
		);
		cpu_a 		<= dma_a_s 			when dma_busreq_s = '0' and cpu_busack_n_s = '0' else z80_a;
		cpu_rd_n 	<= dma_rd_n_s 		when dma_busreq_s = '0' and cpu_busack_n_s = '0' else z80_rd_n;
		cpu_wr_n 	<= dma_wr_n_s 		when dma_busreq_s = '0' and cpu_busack_n_s = '0' else z80_wr_n;
		cpu_mreq_n 	<= dma_mreq_n_s 	when dma_busreq_s = '0' and cpu_busack_n_s = '0' else z80_mreq_n;
		cpu_ioreq_n <= dma_ioreq_n_s 	when dma_busreq_s = '0' and cpu_busack_n_s = '0' else z80_ioreq_n;
		cpu_do		<= dma_d_s		 	when dma_busreq_s = '0' and cpu_busack_n_s = '0' else z80_do;
	end generate;

	nudma: if not use_dma_g generate
		cpu_a 		<= z80_a;
		cpu_rd_n 	<= z80_rd_n;
		cpu_wr_n 	<= z80_wr_n;
		cpu_mreq_n 	<= z80_mreq_n;
		cpu_ioreq_n <= z80_ioreq_n;
		cpu_do		<= z80_do;
	end generate;

	-----------
	-- Sound
	-----------
	explorer: if not use_turbosnd_g generate
		explorer: entity work.explorer
		port map (
			clk						=> iClk_28,
			clk_psg					=> iClk_Psg,
			rst_n						=> reset_n,
			cpu_a						=> cpu_a,
			cpu_di					=> cpu_do,
			cpu_do					=> psg_do,
			cpu_iorq_n				=> cpu_ioreq_n,
			cpu_rd_n					=> cpu_rd_n,
			cpu_wr_n					=> cpu_wr_n,
			cpu_m1_n					=> cpu_m1_n,

			-- audio
			out_audio_mix_L		=> oPSG_L,
			out_audio_mix_R		=> oPSG_R,

			-- controles
			enable					=> not psg_mode(1),
			selected					=> '1',
			psg_out  				=> psg_dout,					-- "1" se temos dados prontos para o barramento
			ctrl_aymode				=> psg_mode(0),				-- 0 = YM, 1 = AY
			stereo_mode				=> stereo_mode_q,				-- 0 = ABC, 1 = ACB

			-- pinos para controle de AY externo
			BDIR						=> open,
			BC1						=> open,
			-- Serial
			rs232_rx					=> '1',  --iRs232_rx,
			rs232_tx					=> open, --oRs232_tx,
			rs232_cts				=> oRs232_cts,
			rs232_dtr				=> iRs232_dtr
		);
	end generate;

	turbosound: if use_turbosnd_g generate
		turbosound: entity work.turbosound
		generic map (
			use_sid_g 	=> use_sid_g
		)
		port map (
			clk						=> iClk_28,
			clk_psg					=> iClk_Psg,
			rst_n						=> reset_n,
			cpu_a						=> cpu_a,
			cpu_di					=> cpu_do,
			cpu_do					=> psg_do,
			cpu_iorq_n				=> cpu_ioreq_n,
			cpu_rd_n					=> cpu_rd_n,
			cpu_wr_n					=> cpu_wr_n,
			cpu_m1_n					=> cpu_m1_n,

			-- audio
			audio_psg_L_o			=> oPSG_L,
			audio_psg_R_o			=> oPSG_R,
			audio_sid_L_o			=> oSID_L,
			audio_sid_R_o			=> oSID_R,

			-- controls
			enable					=> not psg_mode(1),     -- "1" to enable first AY
			enable_turbosound		=> s_turbosound,			-- "1" to enable second AY
			turbosound_out  		=> psg_dout,				-- "1" if we have data to collect
			ctrl_aymode				=> psg_mode(0),			-- 0 = YM, 1 = AY
			stereo_mode				=> stereo_mode_q,			-- 0 = ABC, 1 = ACB

			-- Serial
			rs232_rx					=> '1',--iRs232_rx,
			rs232_tx					=> open,--oRs232_tx,
			rs232_cts				=> oRs232_cts,
			rs232_dtr				=> iRs232_dtr
		);
	end generate;

	-- Soundrive
	gCovox: if use_covox_g generate
	covox: entity work.soundrive
	port map (
		reset_i			=> reset_s,
		clk_i				=> iClk_28,
		enable_i			=> covox_en_q,
		cpu_wr_n_i		=> cpu_wr_n,
		cpu_a_i			=> cpu_a(7 downto 0),
		cpu_d_i			=> cpu_do,
		cpu_ioreq_n_i	=> cpu_ioreq_n,
		
		-- i/o mirror
		mirror_ena_i	=> covox_we_s,
		mirror_data_i	=> covox_data_s,
		
		-- out
		soundrive_L_o	=> oCovox_L,
		soundrive_R_o	=> oCovox_R
	);
	end generate;

	----------
	-- The ULA
	----------

	ula_inst: entity work.zxula
	port map (
		--clock_28_i		=> iClk_28,
		clock_14_i		=> iClk_14,
		clock_7_i		=> iClk_7,
		reset_i			=> reset_s,
		mode_i			=> machine_timing,
		video_timing_i	=> machine_video_timing,
		timex_en_i		=> s_ula_timex_en,
		vf50_60_i		=> ula50_60hz,
		iocs_i			=> ula_en,
		cpu_addr_i		=> cpu_a,
		cpu_data_i		=> ula_din,
		cpu_data_o		=> ula_dout,
		has_data_o		=> ula_hd_s,
		cpu_mreq_n_i	=> cpu_mreq_n,
		cpu_iorq_n_i	=> cpu_ioreq_n,
		cpu_rd_n_i		=> cpu_rd_n,
		cpu_wr_n_i		=> cpu_wr_n,
		cpu_clock_cont_o	=> oCpu_clock_cont,
		cpu_int_n_o		=> ula_int_n,
		cpu_clock_i 	=> iClk_Cpu,
		
		-- vram
		vram_shadow_i	=> vram_shadow,
		ram_contention_i		=> ram_contention_s,
		mem_addr_o		=> vram_a,
		mem_data_i		=> vram_dout,
		mem_cs_o			=> vram_cs,
		mem_oe_o			=> vram_oe,
		s128_disable_i => s128_disable,
		
		-- I/O
		ear_i				=> ula_ear,
		speaker_o		=> ula_spk,
		mic_o				=> ula_mic,
		kb_columns_i	=> iColumns and joy_columns,
		
		-- Line Interrupt
		lint_ctrl_i		=> lineint_ctrl_s,
		lint_line_i		=> lineint_line_s,
		
		-- RGB
--		rgb_r_o			=> ula_r,
--		rgb_g_o			=> ula_g,
--		rgb_b_o			=> ula_b,
		rgb_hsync_o		=> ula_hsync_n,
		rgb_vsync_o		=> ula_vsync_n,
		rgb_hblank_o	=> hblank_n_s,
		rgb_vblank_o	=> vblank_n_s,
		
		hcount_o			=> hcount_s,
		vcount_o			=> vcount_s,
		timex_hires_o  => timex_hires_s,
	--	pixel_clock_o => pixel_clock_o,
		
		-- Sprites
		spt_hcount_o	=> sprite_hc_s,
		spt_vcount_o	=> sprite_vc_s,
		
		-- Palette control
		disable_flash_i=> disable_flash_s,
		ULAnext_value_i=> ULAnext_value_s,
		addr_ulanext_o	=> palette_addr_ulanext_s,
		
		-- clip window	
		clip_x1_i => unsigned(clip_ULA_x1),
		clip_x2_i => unsigned(clip_ULA_x2),
		clip_y1_i => unsigned(clip_ULA_y1),
		clip_y2_i => unsigned(clip_ULA_y2),
		cliped_o	=> cliped_s,
		--
		reset_conter_i => iResetUlaCnt
		
	);

	
--	timex_hires_o <= timex_hires_s;

	
	oRows				<= cpu_a(15 downto 8);


	
	-- Palette write control
   palette_we_addr_s <= palette_index_s when palette_weLB_s = '0' else palette_index_s-1;
	palette_we_ula_s  <= '1' when palette_we_sel_s(1 downto 0) = "00" and (palette_we_s = '1' or palette_weLB_s = '1') else '0';
	palette_we_lay2_s <= '1' when palette_we_sel_s(1 downto 0) = "01" and (palette_we_s = '1' or palette_weLB_s = '1') else '0';
	palette_we_spr_s  <= '1' when palette_we_sel_s(1 downto 0) = "10" and (palette_we_s = '1' or palette_weLB_s = '1') else '0';
	
	gSprites : if use_sprites_g generate
	
 	palette_Sprites: entity work.dpram2
 	generic map 
 	(
 			addr_width_g  => 9,
 			data_width_g  => 9
 	)
 	port map 
 	(
 		
 		clk_a_i  => iClk_28,
 		we_i     => palette_we_spr_s,
 		addr_a_i => palette_we_sel_s(2) & palette_we_addr_s,
 		data_a_i => palette_we_value_s & low_b_s,
 		data_a_o => palette_out_spr_s,
 		
 		clk_b_i  => iClk_28,
 		addr_b_i => palette_sel_spr_s & sprite_colour_s,
 		data_b_o => sprite_RGB_s
 
   );
	end generate;
 
 
	gLayer2 : if use_layer2_g generate
 	palette_Layer2: entity work.dpram2
 	generic map 
 	(
 			addr_width_g  => 9,
 			data_width_g  => 9
 	)
 	port map 
 	(
 		
 		clk_a_i  => iClk_28,
 		we_i     => palette_we_lay2_s,
 		addr_a_i => palette_we_sel_s(2) & palette_we_addr_s,
 		data_a_i => palette_we_value_s & low_b_s,
 		data_a_o => palette_out_lay2_s,
 		
 		clk_b_i  => iClk_28,
 		addr_b_i => palette_sel_lay2_s & palette_addr_lay2_s,
 		data_b_o => palette_rgb_lay2_s
 
   );
 
 	layer2_R_s <= palette_rgb_lay2_s(8 downto 6);
 	layer2_G_s <= palette_rgb_lay2_s(5 downto 3);
	layer2_B_s <= palette_rgb_lay2_s(2 downto 0);
	end generate;
	
	
	palette_ULA: entity work.dpram2
	generic map 
	(
			addr_width_g  => 9,
			data_width_g  => 9
	)
	port map 
	(
		
		clk_a_i  => iClk_28,
		we_i     => palette_we_ula_s,
		addr_a_i => palette_we_sel_s(2) & palette_we_addr_s,
		data_a_i => palette_we_value_s & low_b_s,
		data_a_o => palette_out_ula_s,
		
		clk_b_i  => iClk_28,
		addr_b_i => palette_sel_ula_s & palette_addr_s, --palette_sel_ula_s & palette_addr_ulanext_s,
		data_b_o => palette_rgb_ula_s
  );
  
	palette_addr_s <= palette_addr_lores_s when LoRes_en_s = '1' and LoRes_blank_s = '0' else palette_addr_ulanext_s;
  
   ula_r <= palette_rgb_ula_s(8 downto 6);
   ula_g <= palette_rgb_ula_s(5 downto 3);
   ula_b <= palette_rgb_ula_s(2 downto 0);
  
  	LoRes_R_s <= palette_rgb_ula_s(8 downto 6);
	LoRes_G_s <= palette_rgb_ula_s(5 downto 3);
	LoRes_B_s <= palette_rgb_ula_s(2 downto 0);
	
	-------------
	-- DivMMC  --
	-------------
	mmc: entity work.divmmc
	port map (
		clk_master_i	=> iClk_28,
		clock				=> not iClk_Cpu,				-- Entrada do clock da CPU
		reset_i			=> hardreset_s,				-- Reset Power-on
		cpu_a				=> cpu_a,						-- Barramento de enderecos da CPU
		cpu_wr_n			=> cpu_wr_n,					-- Sinal de escrita da CPU
		cpu_rd_n			=> cpu_rd_n,					-- Sinal de leitura da CPU
		cpu_mreq_n		=> cpu_mreq_n,					-- CPU acessando memoria
		cpu_ioreq_n		=> cpu_ioreq_n,
		cpu_m1_n			=> cpu_m1_n,					-- /M1 da CPU
		di					=> cpu_do,						-- Barramento de dados de entrada
		do					=> divmmc_do,					-- Barramento de dados de sai?da
		wait_n_o			=> wait_n_s,

		rpi_cs0			=> rpi_cs0_n,					-- Raspberry Pi SPI CE 0 
		rpi_cs1			=> rpi_cs1_n,					-- Raspberry Pi SPI CE 1
		spi_cs			=> spi_cs_n,					-- Saida Chip Select para a flash
		ft_cs				=> ft_cs_n,						-- Saida Chip Select para o FT813
		sd_cs0			=> sd_cs0_n,						-- Saida Chip Select para o cartao
		sd_cs1			=> sd_cs1_n,						-- Saida Chip Select para o cartao
		sd_sclk			=> spi_sclk,					-- Saida SCL
		sd_mosi			=> spi_mosi,					-- Master Out Slave In
		sd_miso			=> spi_miso,					-- Master In Slave Out

		nmi_button_n	=> s_button_divmmc,	  		-- Botao de NMI da DivMMC
		nmi_to_cpu_n	=> s_nmi,						-- Sinal de NMI para a CPU

		no_automap		=> divmmc_no_automap or (not s_divmmc_enabled),		-- Entrada para desabilitar o Auto Mapeamento
		ram_bank			=> divmmc_bank,				-- Sai?da informando qual banco de 8K da RAM deve mapear entre 2000 e 3FFF

		--sinais pra rom e ram
		ram_en_o			=> divmmc_ram_en,
		rom_en_o	 		=> divmmc_rom_en,
		dout				=> divmmc_dout,
		conmem_o			=> divmmc_conmem_s,
		regE3_o			=> divmmc_e3_s,
		
		-- Debug
		D_mapterm_o		=> oD_others(6),
		D_automap_o		=> oD_others(7)
	);

	--
	
	spi_miso		<= iSD_miso when sd_cs0_n = '0' or sd_cs1_n = '0'	else -- need to be first (ESXDOS is using some parcial byte to select the SD)
						iFlash_miso	when spi_cs_n = '0' else 
						iFT_miso when ft_cs_n = '0'	else 
						iRPi_miso when rpi_cs0_n = '0' or rpi_cs1_n = '0'	else
						'1';
							
							
	oSpi_sclk	<= spi_sclk;
	oSpi_mosi	<= spi_mosi;
	oSD_cs0_n	<= sd_cs0_n;
	oSD_cs1_n	<= sd_cs1_n;
	oFlash_cs_n	<= spi_cs_n;
	oFT_cs_n		<= ft_cs_n;
	oRPi_cs0_n	<= rpi_cs0_n;
	oRPi_cs1_n	<= rpi_cs1_n;
	
	--
	kmouse : entity work.kempston_mouse
	port map
	(
		clk					=> iClk_cpu,					-- Entrada do clock da CPU
		rst_n					=> reset_n,						-- Reset Power-on
		cpu_a					=> cpu_a,						-- Barramento de enderecos da CPU
		cpu_iorq_n			=> cpu_ioreq_n,				-- CPU acessando portas
		cpu_rd_n				=> cpu_rd_n,					-- Sinal de leitura da CPU

		enable				=> iMouse_en,					-- 1 habilita

		-- entrada
		mouse_x 				=> iMouse_x,
		mouse_y 				=> iMouse_y,
		mouse_bts 			=> iMouse_bts,
		mouse_wheel 		=> iMouse_wheel,

		--saida
		mouse_d				=> s_mouse_d,
		mouse_out			=> s_mouse_out

	);

	gl: if use_lightpen_g generate
	lp: entity work.light_pen
	port map
	(
		cpu_a						=> cpu_a,
		cpu_iorq_n				=> cpu_ioreq_n,
		cpu_rd_n					=> cpu_rd_n,

		-- pinos da Light pen
		lp_in						=> iLP_signal,				-- necessita hardware externo neste pino

		-- controle
		enable					=> s_lp_enabled,			-- '1' habilita o modulo

		-- Saida
		lp_do						=> s_lp_d0,
		lp_out					=> s_lp_out					-- "1" se a interface esta disponibilizando dados

	);
	end generate;

	kjoy: entity work.kempston_joystick
	port map
	(
		cpu_a					=> cpu_a(7 downto 0),
		cpu_iorq_n			=> cpu_ioreq_n,
		cpu_rd_n				=> cpu_rd_n,

		-- modos
		joy0_mode			=> joy0_mode,
		joy1_mode			=> joy1_mode,
		
		-- pinos do joystick
		joy0_pins				=> iJoy0,
		joy1_pins				=> iJoy1,

		-- controle
		enable				=> kempjoy_enable_s,
		
		-- Saida
		kj_do					=> kempston_dout,
		kj_out				=> s_kj_out 				-- "1" se a interface esta disponibilizando dados

	);

	-- Kempston joy
	kempjoy_enable_s <= '1' when usar_kempjoy = '1' and (joy0_mode = "001" or joy0_mode(2) = '1' or joy1_mode = "001" or joy1_mode(2) = '1' )	else '0';

	-- Keyboard joy have 2 buttons
	joys: entity work.joystick_keys
	port map
	(
		-- control
		cpu_a					=> cpu_a,
		enable				=> usar_keyjoy,			-- 1 habilita

		-- modes
		joy0_mode			=> joy0_mode,
		joy1_mode			=> joy1_mode,

		-- external pins
		joy0_pins				=> iJoy0,
		joy1_pins				=> iJoy1,

		-- outs
		joy_columns			=> joy_columns

	);
	
	gcopper: if use_copper_g generate
	copper : entity work.copper
		port map
		(
			clock_i					=> iClk_14,
			reset_n_i				=> reset_n,

			copper_en_i				=> copper_en_s,
			copper_contention_i	=> copper_contention_s,

			hcount_i					=> hcount_s,
			vcount_i					=> vcount_s,
			
			copper_list_addr_o	=> copper_addr_s,
			copper_list_data_i	=> copper_do_s,
		
			copper_dout_o			=> copper_dout_s,
			copper_data_o			=> copper_data_s


		);
	
	copper_list: entity work.dpram8_16
 	generic map 
 	(
 			addr_width_g  => 11
 	)
 	port map 
 	(
 		
 		clk_a_i  => iClk_28,
 		we_i     => copper_we_s,
 		addr_a_i => copper_we_addr_s,
 		data_a_i => copper_di_s,
 		data_a_o => open,
 		
 		clk_b_i  => iClk_28,
 		addr_b_i => copper_addr_s,
 		data_b_o => copper_do_s
 
    );
	end generate;
	
	

	multiface_inst : entity work.multiface
	port map (
		clock_i					=> iClk_cpu,
		reset_n_i				=> reset_n,
		cpu_addr_i				=> cpu_a,
		data_i					=> cpu_do,
		data_o					=> s_m1_do,
		has_data_o				=> s_m1_dout,
		cpu_mreq_n_i			=> cpu_mreq_n,
		cpu_iorq_n_i			=> cpu_ioreq_n,
		cpu_rd_n_i				=> cpu_rd_n,
		cpu_wr_n_i				=> cpu_wr_n,
		cpu_m1_n_i				=> cpu_m1_n,
		nmi_button_n_i			=> s_button_m1,
		nmi_to_cpu_n_o			=> s_nmi_m1,
		
		-- Multiface interface control
		enable_i					=> s_multiface_en and (not divmmc_conmem_s), --dont page the Multiface if divMMC is active
		mode_i					=> machine_type_t'pos(machine),
		zxromcs_o				=> open,
		
		-- RAM and ROM
		m1_rom_cs_n_o			=> s_m1_rom_cs,
		m1_ram_cs_n_o			=> s_m1_ram_cs,
		m1_ram_we_n_o			=> open,
		m1_7ffd_i				=> port7FFD_reg,
		m1_1ffd_i				=> port1FFD_reg
	);

	s_button_divmmc <= (not iKeyDivMMC) when s_divmmc_enabled = '1' else '1';
	s_button_m1 	 <= (not iKeyMF)     when s_m1_enabled = '1' 	 else '1';

	divmmc_no_automap <= '0' when reset_s = '1' or s_button_divmmc = '0' else
								'1' when falling_edge(s_button_m1);

	--so a multiface ou a divmmc pode estar habilitado
	-- na pratica podem ter o mesmo valor, ja que a m1 habilita em 1 e automap em 0
	s_multiface_en <= divmmc_no_automap;
	
	gLores: if use_loRes_g generate
	LoRes : entity work.lores
		port map
		(
			clock_i				=> iClk_7,
			reset_n_i			=> reset_n,

			LoRes_en_i			=> LoRes_en_s,

			LoRes_X_i			=> hcount_s,
			LoRes_Y_i			=> vcount_s,
			offset_x_i			=> LoRes_off_X_s,
			offset_y_i			=> LoRes_off_Y_s,
			
			transp_colour_i	=> transparency_q,

			LoRes_addr_o 	=> LoRes_addr_s,
			LoRes_data_i 	=> vram_dout,

			LoRes_R_o		=> palette_addr_lores_s(7 downto 5),
			LoRes_G_o		=> palette_addr_lores_s(4 downto 2),
			LoRes_B_o		=> palette_addr_lores_s(1 downto 0),

			LoRes_blank_o	=> LoRes_blank_s,
			
			-- clip window
			clip_x1_i => unsigned(clip_ULA_x1),
			clip_x2_i => unsigned(clip_ULA_x2),
			clip_y1_i => unsigned(clip_ULA_y1),
			clip_y2_i => unsigned(clip_ULA_y2)

		);
	end generate;

	uov: if use_layer2_g generate
		layer2 : entity work.layer2
		port map
		(
			clock_i			=> iClk_14,
			reset_n_i		=> reset_n,

			layer2_en_i	=> layer2_visible_en_s,

			layer2_X_i		=> hcount_s,
			layer2_Y_i		=> vcount_s,
			offset_x_i		=> layer2_off_X_s,
			offset_y_i		=> layer2_off_Y_s,

			transp_colour_i=>transparency_q,

			layer2_addr_o => layer2_addr_s,
			layer2_data_i => ilayer2_data,

			layer2_R_o		=> palette_addr_lay2_s(7 downto 5),
			layer2_G_o		=> palette_addr_lay2_s(4 downto 2),
			layer2_B_o		=> palette_addr_lay2_s(1 downto 0),

			layer2_blank_o=> layer2_blank_s,
			layer2_cs_o=> layer2_cs_s,
			
			-- clip window
			clip_x1_i => unsigned('0' & clip_layer2_x1),
			clip_x2_i => unsigned('0' & clip_layer2_x2),
			clip_y1_i => unsigned('0' & clip_layer2_y1),
			clip_y2_i => unsigned('0' & clip_layer2_y2)


		);
	end generate;
	
	--palette_addr_lay2_s <= ilayer2_data;

	uspt: if use_sprites_g generate
		-- Sprites
		sprite : entity work.sprites
		port map (
			clock_master_i	=> iClk_28,
			clock_master_180o_i => iClk_28_180o,
			clock_pixel_i	=> iClk_7,
			reset_i			=> reset_s,
			over_border_i	=> sprite_border_q,
			hcounter_i		=> sprite_hc_s,
			vcounter_i		=> sprite_vc_s,
			
			transp_colour_i=> transparency_index_s, --transparency_q, -- 

			-- CPU
			cpu_a_i			=> cpu_a,
			cpu_d_i			=> cpu_do,
			cpu_d_o			=> sprite_data_s,
			has_data_o		=> sprite_hd_s,
			cpu_iorq_n_i	=> cpu_ioreq_n,
			cpu_rd_n_i		=> cpu_rd_n,
			cpu_wr_n_i		=> cpu_wr_n,
			
			-- Video out
			rgb_o				=> sprite_colour_s,
			pixel_en_o		=> sprite_pixel_s,
			
			-- clip window
			clip_x1_i => unsigned(clip_sprites_x1),
			clip_x2_i => unsigned(clip_sprites_x2),
			clip_y1_i => unsigned(clip_sprites_y1),
			clip_y2_i => unsigned(clip_sprites_y2)

		);
	end generate;
	
	--- UART

	gUART : if use_UART_g generate
	uart : entity work.uart
   port map 
 	( 
			--ticks_per_bit_i 		=> uart_speed_s,
			uart_prescaler_i		=> uart_prescaler_s,
			clock_i       			=> iClk_28, --iClk_3M5,			-- 3.5 mhz no contention
			TX_start_i     		=> uart_tx_start_s,			-- '1' to start the transmission
			TX_byte_i  				=> uart_tx_byte_s,     		-- byte to transmit
			TX_active_o 			=> uart_tx_active_s,		  	-- '1' during transmission
			TX_out_o 				=> oRs232_tx, 					-- TX line
			TX_byte_finished_o   => open,  	  					-- When complete, '1' for one clock cycle
			RX_in_i 					=> iRs232_rx,					-- RX line
			RX_byte_finished_o	=> uart_rx_finished_s, 		-- When complete, '1' for one clock cycle
			RX_byte_o   			=> uart_rx_byte_s				-- The incoming byte
 	);
	
	fifo : entity work.FIFO
	port map
	( 
		clock_i		=> iClk_28,--iClk_3M5,	
		reset_i		=> reset_s,
		fifo_we_i	=> uart_rx_finished_s,
		fifo_data_i	=> uart_rx_byte_s,
		fifo_read_i	=> port143B_rd_s,
		fifo_data_o	=> fifo_next_byte_s,
		fifo_empty_o=> fifo_empty_s,
		fifo_full_o	=> fifo_full_s, --1 when the FIFO is full
		fifo_head_o	=> open
	);



	
	--uart_rx_ready_s <= not fifo_empty_s; --ready is the empty signal delayed one clock

	--ultima coisa adicionada, se nao funcionar deixar assincrono , linha acima
	--process(iClk_3M5)
	--begin
	--	if rising_edge(iClk_3M5) then
	--			uart_rx_ready_s <= not fifo_empty_s;
	--	end if;
	--end process;
	
	
	
	-- UART
	process(reset_s, iClk_cpu, uart_rx_finished_s)
	variable rx_finished_v : std_logic_vector(1 downto 0);
	variable port143B_edge_v : std_logic_vector(1 downto 0);
	begin
	
	-- 3500000 / 2400 	= 1458 	= 3.5mhz 2400 bps
	-- 3500000 / 4800 	= 729 	= 3.5mhz 4800 bps
	-- 3500000 / 9600 	= 364.58 = 3.5mhz 9600 bps
	-- 3500000 / 19200 	= 182.29 = 3.5mhz 19200 bps
	-- 3500000 / 31250 	= 112  	= 3.5mhz 31250 bps
	-- 3500000 / 38400 	= 91.14  = 3.5mhz 38400 bps
	-- 3500000 / 57600 	= 60.76	= 3.5mhz 57600 bps
	-- 3500000 / 115200 	= 30.38  = 3.5mhz 115200 bps
	
	-- 28000000 / 2400 	= 11666 	= 28mhz 2400 bps
	-- 28000000 / 4800 	= 5833 	= 28mhz 4800 bps
	-- 28000000 / 9600 	= 2916 	= 28mhz 9600 bps
	-- 28000000 / 19200 	= 1458 	= 28mhz 19200 bps
	-- 28000000 / 31250 	= 896  	= 28mhz 31250 bps
	-- 28000000 / 38400 	= 729  	= 28mhz 38400 bps
	-- 28000000 / 57600 	= 486		= 28mhz 57600 bps
	-- 28000000 / 115200 = 243  	= 28mhz 115200 bps
	
		
		
		
--		if reset_s = '1' then

--				uart_speed_s <= 30; -- uart default speed 115200 at 3.5Mhz
--				uart_speed_s <= 243; -- uart default speed 115200 at 28mhz
			
--		els
		if falling_edge(iClk_cpu) then
		
		
				if port143B_wr_s = '1' then -- A write on the RX port configures the UART pre-scaler (0x143b = 5179)
				
						if cpu_do(7) = '0' then
							uart_prescaler_s <= uart_prescaler_s(6 downto 0) & cpu_do(6 downto 0);
						else
							uart_prescaler_s <= cpu_do(6 downto 0) & uart_prescaler_s(6 downto 0);					
						end if;
						
						
					-- 3,5Mhz
					--		case cpu_do(2 downto 0) is
					--			when "001" => uart_speed_s <= 60; -- 57600
					--			when "010" => uart_speed_s <= 90; -- 38400
					--			when "011" => uart_speed_s <= 112; -- 31250	
					--			when "100" => uart_speed_s <= 182; -- 19200
					--			when "101" => uart_speed_s <= 364; -- 9600
					--			when "110" => uart_speed_s <= 729; -- 4800
					--			when "111" => uart_speed_s <= 1458; -- 2400
					--			when others => uart_speed_s <= 30; -- 115200 
					--		end case;
						
						-- 28Mhz
					--		case cpu_do(2 downto 0) is
					--			when "001" => uart_speed_s <= 486; -- 57600
					--			when "010" => uart_speed_s <= 729; -- 38400
					--			when "011" => uart_speed_s <= 896; -- 31250	
					--			when "100" => uart_speed_s <= 1458; -- 19200
					--			when "101" => uart_speed_s <= 2916; -- 9600
					--			when "110" => uart_speed_s <= 5833; -- 4800
					--			when "111" => uart_speed_s <= 11666; -- 2400
					--			when others => uart_speed_s <= 243; -- 115200 
					--		end case;
	
				elsif port133B_wr_s = '1' then -- TX port 0x133b = 4923
						uart_tx_byte_s <= cpu_do;
						uart_tx_start_s <= '1'; -- '1' to start the transmission
				else
						uart_tx_start_s <= '0'; 
				end if;
				

		end if;
	end process;
	end generate;

	
	

	-- Power-on
	process (iPowerOn, iClk_28)
	begin
		if iPowerOn = '1' then
			poweron_cnt <= (others => '1');
		elsif rising_edge(iClk_28) then
			if poweron_cnt /= "0000" then
				poweron_cnt <= poweron_cnt - 1;
			end if;
		end if;
	end process;

	-- Resets
	poweron_n	<= '0' when poweron_cnt /= 0 									else '1';	-- Negative logic
	hardreset_s	<= '1' when iHardReset = '1' or hardreset_q = '1'		else '0';
	oSoftReset	<= softreset_q;
	softreset_s	<= iSoftReset;
	reset_s		<= not poweron_n or hardreset_s or softreset_s;
	reset_n		<= not reset_s;																	-- Negative logic




--	oClk_vid	<= clk_vid;

	-- Register number
--	process (reset_s, iClk_cpu)
--	begin
--		if reset_s = '1' then
--			register_q <= (others => '0');
--		elsif falling_edge(iClk_cpu) then
--			if port243B_en_s = '1' and cpu_wr_n = '0' then
--				register_q <= cpu_do;
--			end if;
--		end if;
--	end process;

	-- Write Registers
	

	process ( iClk_28, iClk_cpu, port243B_wr_s , Z80N_dout_s , copper_dout_s , port253B_wr_s )
		variable clock_edge_v : std_logic_vector(1 downto 0);
		variable copper_edge_v : std_logic_vector(1 downto 0);
		variable out_edge_v : std_logic_vector(1 downto 0);
		
		variable keymap_we_a_v	: std_logic;
		variable data_v		: std_logic_vector(7 downto 0);
		variable register_v  : std_logic_vector(7 downto 0);
		variable palette_w_v  : std_logic_vector(1 downto 0);
		variable write_copper_v  : std_logic_vector(1 downto 0);
		variable clk35_v  : std_logic_vector(1 downto 0);
		variable reg44_v  : std_logic_vector(1 downto 0);
		variable copper_v	: std_logic;
		variable nextreg_w_v  : std_logic_vector(1 downto 0);
		
		variable nextreg_v	: std_logic;
		variable out_v	: std_logic;
		variable write_to_reg_v	: std_logic;
	begin



		if rising_edge(iClk_28) then
		

				
			keymap_we_s	<= '0';
			palette_we_s <= '0';
			palette_weLB_s		<= '0';
			palette_reg44_we_s <= '0';
			
			copper_we_s <= '0';
			
			clip_layer2_we_s <= '0';
			clip_sprites_we_s <= '0';
			clip_ULA_we_s <= '0';
			
			covox_we_s <= '0';
			
			--soft reset with drive button if the divmmc is disabled
			if iKeyDivMMC = '1' and s_divmmc_enabled = '0' then
				softreset_p_q 		<= '1';
			end if;
			
			
			if reset_s = '1' then
					
					turbo_v <= "00";
				
					MMU0_s <= X"FF"; -- page mapped from 0x0000 to 0x1fff
					MMU1_s <= X"FF"; -- page mapped from 0x2000 to 0x3fff
					MMU2_s <= X"0A"; -- page mapped from 0x4000 to 0x5fff
					MMU3_s <= X"0B"; -- page mapped from 0x6000 to 0x7fff
					MMU4_s <= X"04"; -- page mapped from 0x8000 to 0x9fff
					MMU5_s <= X"05"; -- page mapped from 0xa000 to 0xbfff
					MMU6_s <= X"00"; -- page mapped from 0xc000 to 0xdfff
					MMU7_s <= X"01"; -- page mapped from 0xe000 to 0xffff

					disable_contention_q <= '0';
										
					softreset_p_q 		<= '0';
					transparency_q		<= "11100011"; -- 0xE3
					transparency_index_s <= "11100011"; -- 0xE3
					transparency_fallback_s <= (others => '0');
					sprite_border_q	<= '0';
					sprite_visible_q	<= '0';
					lineint_ctrl_s		<= (others => '0');
					lineint_line_s		<= (others => '0');
					layer2_off_X_s	<= (others => '0');
					layer2_off_Y_s	<= (others => '0');
					layer2_page_s 	<= "001000"; -- speccy ram page 8 (first of extra RAM)
					layer2_page_shadow_s <= "001011"; -- speccy ram page 11 
					
					--default clipping window for Layer 2
					clip_layer2_x1 <= X"00";
					clip_layer2_x2 <= X"FF";
					clip_layer2_y1 <= X"00";
					clip_layer2_y2 <= X"BF";
					clip_layer2_idx_s <= "00";		
				
					--default clipping window for Sprites
					clip_sprites_x1 <= X"00";
					clip_sprites_x2 <= X"FF";
					clip_sprites_y1 <= X"00";
					clip_sprites_y2 <= X"BF";
					clip_sprites_idx_s <= "00";
					
					--default clipping window for LoRes
					clip_ULA_x1 <= X"00";
					clip_ULA_x2 <= X"FF";
					clip_ULA_y1 <= X"00";
					clip_ULA_y2 <= X"BF";
					clip_ULA_idx_s <= "00";
					
					layer_priorities_s <= "000";
					LoRes_en_s <= '0';
					
					--copper
					copper_en_s			<= "00";
					copper_we_s			<= '0';
					copper_we_addr_s	<= (others=>'0');	
					
					--palette
					ULAnext_value_s <= "00001111"; -- 16 ink colours and 16 paper colours (4/4)
					palette_we_sel_s <= (others=>'0');
					palette_sel_spr_s		<= '0';
					palette_sel_lay2_s	<= '0';
					palette_sel_ula_s		<= '0';
					disable_flash_s 		<= '0';
					palette_reg44_sel_s	<= '0';
					palette_no_auto_inc_s  <= '0';

					if machine = s_config then
						bootrom_s			<= '1';
					end if;

					if hardreset_s = '1' or poweron_n = '0' then
						--ula50_60hz			<= '0';
						flashboot_q			<= '0';
						hardreset_p_q		<= '0';
						machine				<= s_config;
						bootrom_s			<= '1';
						romram_page			<= (others => '0');
						s_divmmc_enabled	<= '0';
						s_m1_enabled		<= '0';
						psg_mode				<= "00";
						scandbl_en			<= '1';
						scanlines_s			<= "00";
						s_lp_enabled		<= '0';
						joy0_mode			<= "000";
						joy1_mode			<= "000";
						turbo_v				<= "00";
						s_ula_timex_en		<= '0';
						covox_en_q			<= '0';
						intsnd_en_q			<= '1';
						stereo_mode_q		<= '0';
					--	timing_idx_s 		<= (others =>'0');
					--	timing_hi_s			<= '0';
						
					end if;
					
					--s128_disable <= '0';
					port7FFD_reg <= (others=>'0');
					
				end if;
				
				if (port1FFD_wr_s = '1' or port7FFD_wr_s = '1' or portDFFD_wr_s = '1') and s128_disable = '0' then
			
							MMU0_s <=  X"FF";
							MMU1_s <=  X"FF";
					
				end if;
				
				if port7FFD_wr_s = '1' and s128_disable = '0' then
					
						--bit 5 = s128_disable
						--bit 4 = s128_rom_page	
						--bit 3 = s128_shadow		
						--bit2-0 = s128_ram_page
			
						--s128_disable			<= port7FFD_reg(5);		
		
						MMU6_s <= portDFFD_reg(3 downto 0) & cpu_do(2 downto 0) & '0';
						MMU7_s <= portDFFD_reg(3 downto 0) & cpu_do(2 downto 0) & '1';
						
						port7FFD_reg <= cpu_do;	
				
				end if;
				
				if portDFFD_wr_s = '1' and s128_disable = '0' then
				
						-- bit 3 = 2048k
						-- bit 2 = 1024k
						-- bit 1 = 512k
						-- bit 0 = 256k
						
						MMU6_s <= portDFFD_reg(3 downto 0) & port7FFD_reg(2 downto 0) & '0';
						MMU7_s <= portDFFD_reg(3 downto 0) & port7FFD_reg(2 downto 0) & '1';
				
				end if;
		
		
				-- OUT 243b to select the active Register
				if port243B_wr_s = '1' then
					register_q <= cpu_do;
					register_v := cpu_do;
				end if;
				
					--copper write
--					copper_v := '0';
--					if copper_dout_s = '1' or copper_delayed_s = '1' then
--							if (Z80N_dout_s = '1' or port253B_wr_s = '1') then
--								copper_d1_s <='1';
--								copper_delayed_data_s <= copper_data_s;
--							elsif copper_delayed_s = '1' then
--								copper_d1_s <='0';
--								register_v := '0' & copper_delayed_data_s(14 downto 8);
--								data_v := copper_delayed_data_s(7 downto 0);
--								copper_v := '1';
--							else
--								register_v := '0' & copper_data_s(14 downto 8);
--								data_v := copper_data_s(7 downto 0);
--								copper_v := '1';
--							end if;
--					end if;				
	--			copper_delayed_s <= copper_d1_s;
	
	
	
	
	
	
	
	
	
	
	
	
	
						
					-- REGNEXT write
					if Z80N_dout_s = '1' then
						if Z80N_command_s = NEXTREGW then
						--	register_v := Z80N_data_s(15 downto 8);
						--	data_v := Z80N_data_s(7 downto 0);
							nextreg_v := '1';
						else
							nextreg_v := '0';
						end if;	
					else
							nextreg_v := '0';
					end if;
					
					-- OUT write
					if port253B_wr_s = '1' then
					--	register_v := register_q;
					--	data_v := cpu_do;
						out_v := '1';
					else
						out_v := '0';
					end if;
					
					if copper_dout_s = '1' then
							--register_v := '0' & copper_data_s(14 downto 8);
							--data_v := copper_data_s(7 downto 0);
							copper_v := '1';
					else
							copper_v := '0';
					end if;			
					
					
			--clock_edge_v := clock_edge_v(0) & clock_register_s;
			--clock_edge_v := clock_edge_v(0) & (Z80N_dout_s or copper_v or port253B_wr_s); 
			
			
			
		--	if (slice_v = '0' and copper_dout_s = '1') or (slice_v = '1' and (Z80N_dout_s ='1' or port253B_wr_s = '1')) then
	--			clock_edge_v := clock_edge_v(0) & '1';
		----	else
		--		clock_edge_v := clock_edge_v(0) & '0'; 
	--		end if;
		
		--	if clock_edge_v = "01" then --rising edge
			
		
			write_to_reg_v := '0';
		
			copper_edge_v := copper_edge_v(0) & copper_v; 
			
			if copper_edge_v = "01" then
					register_v := '0' & copper_data_s(14 downto 8);
					data_v := copper_data_s(7 downto 0);
					write_to_reg_v := '1';
			else
				
					out_edge_v := out_edge_v(0) & out_v;
			
					if out_edge_v = "01" then
						register_v := register_q;
						data_v := cpu_do;
						write_to_reg_v := '1';
						
					else
					
						clock_edge_v := clock_edge_v(0) & nextreg_v;
						
						if clock_edge_v = "01" then
							register_v := Z80N_data_s(15 downto 8);
							data_v := Z80N_data_s(7 downto 0);
							write_to_reg_v := '1';
							
						end if;
					end if;
			end if;
	
					
			if write_to_reg_v = '1' then
				
					
			--		if port253B_wr_s = '1' or Z80N_dout_s = '1' or copper_dout_s = '1' then
			--			write_NextReg_en_s <= '1'; 
			--		else 
			--			write_NextReg_en_s <= '0';
			--		end if;
			--	
			--		nextreg_w_v 		:= nextreg_w_v(0) & write_NextReg_en_s;
			--	
			--		if nextreg_w_v = "01" then --rising edge
					
					
					
					oD_reg_o(15 downto 8) <= register_v;
					oD_reg_o(7 downto 0) <= data_v;
					
					
						case register_v is

							when X"02" =>
								hardreset_p_q <= data_v(1);
								softreset_p_q <= data_v(0);

							when X"03" =>
								if machine = s_config then
									bootrom_s <= '0';
									
									if data_v(7) = '1' then
										machine_timing <= data_v(6 downto 4);
									end if;
									
									machine_s <= data_v(2 downto 0);
		  
									case data_v(2 downto 0) is
										when "001"	=> machine <= s_speccy48;
										when "010"	=> machine <= s_speccy128;
										when "011"	=> machine <= s_speccy3e;
										when "100"	=> machine <= s_speccy128; 
										when others	=> machine <= s_config;
									end case;
								end if;

							when X"04" =>
								if machine = s_config and bootrom_s = '0' then
									romram_page <= data_v(4 downto 0);
								end if;

							when X"05" =>
								joy0_mode			<= data_v(3) & data_v(7 downto 6);
								joy1_mode			<= data_v(1) & data_v(5 downto 4);
								ula50_60hz			<= data_v(2);
								scandbl_en			<= data_v(0);

							when X"06" =>
								s_ena_turbo			<= data_v(7);
								s_dac					<= data_v(6);
								s_lp_enabled 		<= data_v(5);
								s_divmmc_enabled	<= data_v(4);
								s_m1_enabled		<= data_v(3);
								s_ps2_mode			<= data_v(2);
								psg_mode				<= data_v(1 downto 0);

							when X"07" =>
								if use_turbo then
									turbo_v			<= data_v(1 downto 0);
								end if;

							when X"08" =>
								port7FFD_reg(5)	<= not data_v(7);
								--s128_disable		<= not data_v(7);
								disable_contention_q			<= data_v(6);
								stereo_mode_q		<= data_v(5);
								intsnd_en_q			<= data_v(4);
								covox_en_q			<= data_v(3);
								s_ula_timex_en		<= data_v(2);
								s_turbosound		<= data_v(1);
								s_ntsc_pal			<= data_v(0);
								
							when X"09" =>
								scanlines_s 		<= data_v(1 downto 0);
								
				--		when X"0F" => -- 15
				--			if machine = s_config then
			--
				--				if timing_hi_s = '0' then
				--					timing_q(to_integer(unsigned(timing_idx_s)))(7 downto 0) <= data_v(7 downto 0);
				--				else
				--					timing_q(to_integer(unsigned(timing_idx_s)))(9 downto 8) <= data_v(1 downto 0);
				--					timing_idx_s <= timing_idx_s + 1; --advance to next register
				--				end if;
				--				
				--				timing_hi_s <= not timing_hi_s;
				--				
				--				
				--				end if;

							when X"10" =>	-- 16
								flashboot_q	<= data_v(7);
								
							when X"11" => -- 17
								if machine = s_config then
									machine_video_timing <= data_v(2 downto 0);
								end if;
								
							when X"12" => -- 18
								layer2_page_s	<= data_v(5 downto 0);

							when X"13" => -- 19
								layer2_page_shadow_s <= data_v(5 downto 0);

							when X"14" => -- 20
								transparency_q	<= data_v;

							when X"15" => -- 21
								LoRes_en_s <= data_v(7);
								layer_priorities_s <= data_v(4 downto 2);
								sprite_border_q	<= data_v(1);
								sprite_visible_q	<= data_v(0);

							when X"16" => -- 22
								layer2_off_X_s <= data_v;

							when X"17" => -- 23
								layer2_off_Y_s <= data_v;

							when X"18" => -- 24
								case clip_layer2_idx_s is 
									when "00" => clip_layer2_x1 <= data_v;
									when "01" => clip_layer2_x2 <= data_v;
									when "10" => clip_layer2_y1 <= data_v;
									when "11" => clip_layer2_y2 <= data_v;
									when others => null;
								end case;
								clip_layer2_idx_s <= clip_layer2_idx_s + 1; --advance to next register
			
							when X"19" => -- 25
								case clip_sprites_idx_s is 
									when "00" => clip_sprites_x1 <= data_v;
									when "01" => clip_sprites_x2 <= data_v;
									when "10" => clip_sprites_y1 <= data_v;
									when "11" => clip_sprites_y2 <= data_v;
									when others => null;
								end case;
								clip_sprites_idx_s <= clip_sprites_idx_s + 1; --advance to next register

							when X"1A" => -- 26
								case clip_ULA_idx_s is 
									when "00" => clip_ULA_x1 <= data_v;
									when "01" => clip_ULA_x2 <= data_v;
									when "10" => clip_ULA_y1 <= data_v;
									when "11" => clip_ULA_y2 <= data_v;
									when others => null;
								end case;
								clip_ULA_idx_s <= clip_ULA_idx_s + 1; --advance to next register
			
							when X"1C" => -- 28
									if data_v(0) = '1' then
										clip_layer2_idx_s <= "00";
									end if;
									
									if data_v(1) = '1' then
										clip_sprites_idx_s <= "00";
									end if;
									
									if data_v(2) = '1' then
										clip_ULA_idx_s <= "00";
									end if;
									
							when X"22" =>	-- 34
								lineint_ctrl_s		<= data_v(2 downto 1);
								lineint_line_s(8)	<= data_v(0);

							when X"23" =>	-- 35
								lineint_line_s(7 downto 0) <= data_v;

							when X"28" =>	-- 40
								keymap_addr_q(8) <= data_v(0);

							when X"29" =>	-- 41
								keymap_addr_q(7 downto 0)	<= unsigned(data_v);

							when X"2A" =>	-- 42
								keymap_data_q(8) <= data_v(0);

							when X"2B" =>	-- 43
								keymap_data_q(7 downto 0)	<= data_v;
								keymap_we_s						<= '1';
								
							when X"2D" =>	-- 45
								covox_we_s <= '1';
								covox_data_s <= data_v;
								
							when X"32" => -- 50
								LoRes_off_X_s <= data_v;

							when X"33" => -- 51
								LoRes_off_Y_s <= data_v;
								
							when X"40" => -- 64
								palette_index_s <= data_v;

							when X"41" => -- 65
								palette_we_value_s <= data_v;
								low_b_s <= data_v(1) or data_v(0);
								palette_we_s		<= '1';
					
								
							when X"42" => -- 66
								ULAnext_value_s <= data_v;

							when X"43" => -- 67
								palette_no_auto_inc_s  <= data_v(7);
								palette_we_sel_s		<= data_v(6 downto 4);
								palette_sel_spr_s		<= data_v(3);
								palette_sel_lay2_s	<= data_v(2);
								palette_sel_ula_s		<= data_v(1);
								disable_flash_s 		<= data_v(0);
								
							when X"44" => -- 68
								if palette_reg44_sel_s = '0' then
									palette_we_value_s <= data_v;
									palette_we_s		 <= '1';
								else
									low_b_s <= data_v(0);
									palette_weLB_s		 <= '1';
								end if;
								
								palette_reg44_sel_s <= not palette_reg44_sel_s;
								
							when X"4A" => -- 74
								transparency_fallback_s <= data_v;
								
							when X"4B" => -- 75
								transparency_index_s <= data_v;
								
							when X"50" => -- 80
								MMU0_s <= data_v;
								
							when X"51" => -- 81
								MMU1_s <= data_v;
								
							when X"52" => -- 82
								MMU2_s <= data_v;
								
							when X"53" => -- 83
								MMU3_s <= data_v;
								
							when X"54" => -- 84
								MMU4_s <= data_v;
								
							when X"55" => -- 85
								MMU5_s <= data_v;
								
							when X"56" => -- 86
								MMU6_s <= data_v;
								
							when X"57" => -- 87
								MMU7_s <= data_v;
								
							when X"60" => -- 96
								copper_di_s <= data_v;
								copper_we_s	<= '1';
								
								
					--			Bits 15-14 (2 bits)
					--			%00 = STOP
					--			%01 = START from the copper list addr 0000 and loop continuously
					--			%10 = Free running will run and loop continuously
					--			%11 = START and restart at next VBlank


					--			Bits 13-9 (5 bits)
					--			MUST be %00000

					--			Bits 8-0  (9 bits)
					--			Address Index (0 to 511)

								
							when X"61" => -- 97 Lo data copper
								copper_we_addr_s(7 downto 0) <= data_v;

							when X"62" => -- 98 Hi data copper
								copper_en_s <= data_v(7 downto 6);
								copper_we_addr_s(10 downto 8) <= data_v(2 downto 0);

							-- Debug
							when X"FF" =>
								oD_leds <= data_v;
							when others =>
								null;
						end case;

		--				reg44_v 		:= reg44_v(0) & palette_reg44_we_s;
		--				if reg44_v = "01" then 
		--						if palette_reg44_sel_s = '0' then
		--							palette_we_value_s <= data_v;
		--							palette_we_s		 <= '1';
		--						else
		--							low_b_s <= data_v(0);
		--							palette_weLB_s		 <= '1';
		--						end if;
		--						
		--					palette_reg44_sel_s <= not palette_reg44_sel_s;
		--				end if;
						

						
			--			write_layer2_win_v := write_layer2_win_v(0) & clip_layer2_we_s;
			--			if write_layer2_win_v = "01" then
			--					case clip_layer2_idx_s is 
			--						when "00" => clip_layer2_x1 <= data_v;
			--						when "01" => clip_layer2_x2 <= data_v;
			--						when "10" => clip_layer2_y1 <= data_v;
			--						when "11" => clip_layer2_y2 <= data_v;
			--						when others => null;
			--					end case;
			--					clip_layer2_idx_s <= clip_layer2_idx_s + 1; --advance to next register
			--			end if;
			--			
			--			write_sprites_win_v := write_sprites_win_v(0) & clip_sprites_we_s;
			--			if write_sprites_win_v = "01" then
			--					case clip_sprites_idx_s is 
			--						when "00" => clip_sprites_x1 <= data_v;
			--						when "01" => clip_sprites_x2 <= data_v;
			--						when "10" => clip_sprites_y1 <= data_v;
			--						when "11" => clip_sprites_y2 <= data_v;
			--						when others => null;
			--					end case;
			--					clip_sprites_idx_s <= clip_sprites_idx_s + 1; --advance to next register
			--			end if;
						
				--end if;
				
			end if;
			
			-- Keymap address auto-increment
			if keymap_we_a_v = '1' and keymap_we_s = '0' then
				keymap_addr_q <= keymap_addr_q + 1;
			end if;
			keymap_we_a_v	:= keymap_we_s;

			-- config change on-the-fly
			db1	<= db1(0) & iKeyScanDoubler;
			db2	<= db2(0) & iKey50_60hz;
			db3	<= db3(0) & iKeyScanlines;
			db4	<= db4(0) & iKeyTurbo;

			if db1 = "01" then
				scandbl_en <= not scandbl_en;
			end if;

			if db2 = "01" then
				ula50_60hz <= not ula50_60hz;
			end if;

			if db3 = "01" then
				scanlines_s <= scanlines_s + 1;
			end if;

			if db4 = "01" and s_ena_turbo = '1' and use_turbo then
				turbo_v <= turbo_v + 1;
			end if;
			
		--	clk35_v := clk35_v(0) & iClk_cpu;
		--	if clk35_v = "01" then
		--		turbo_s <= turbo_v;
		--	end if;
		
			-- limit the turbo speed inside the video area (up to 7mhz)
	--		if (hcount_s <= 255 + 12 and vcount_s <= 191) and turbo_v > "01" then
	--			turbo_s <= "01";
	--		else
	--			turbo_s <= turbo_v;
	--		end if;

		
			palette_w_v 		:= palette_w_v(0) & palette_we_s;
			if palette_w_v = "10" then --falling edge
					if palette_no_auto_inc_s = '0' then
						palette_index_s <= palette_index_s + 1; --increment palette index after the write
					end if;
			end if;
			
			write_copper_v := write_copper_v(0) & copper_we_s;
			if write_copper_v = "10" then
					copper_we_addr_s <= copper_we_addr_s + 1;
			end if;
			
		end if;
		
	end process;
	
	oTurbo_sel <= turbo_s;
	
		-- Reset register
	process ( iClk_28, hcount_s, vcount_s, turbo_v )
	begin
		if rising_edge( iClk_28 ) then		
			-- limit the turbo speed inside the video area (up to 7mhz)
			if ( hcount_s <= 255 + 12 and vcount_s <= 191 ) and turbo_v > "01" then
				turbo_s <= "01"; -- 00 = 3.5  01 = 7  10 = 14
			else
				turbo_s <= turbo_v;
			end if;
	end if;
	end process;		
			

	-- Reset register
	process (iClk_28)
		variable reset_v : std_logic_vector(2 downto 0);
	begin
		if falling_edge(iClk_28) then
			reset_v := reset_v(1 downto 0) & reset_s;
			if reset_v = "011" then
				reg02_data_s <= "00000" & not poweron_n & hardreset_q & softreset_q;
			end if;
		end if;
	end process;

	-- Read registers
	process (register_q, reg02_data_s, transparency_q,
				layer2_page_s, layer2_page_shadow_s, lineint_line_s,
				LoRes_off_X_s, LoRes_off_Y_s, MMU0_s, MMU1_s, MMU2_s, MMU3_s, MMU4_s, MMU5_s, MMU6_s, MMU7_s,
				palette_out_ula_s, palette_out_lay2_s, palette_out_spr_s,
				palette_we_sel_s, palette_sel_spr_s, palette_sel_lay2_s, palette_sel_ula_s, disable_flash_s, palette_index_s, ULAnext_value_s,
				joy0_mode, joy1_mode, ula50_60hz, scanlines_s, machine_video_timing, s_dac, s_ena_turbo, turbo_s, s_ntsc_pal, psg_mode,
				lineint_ctrl_s, vcount_s, layer2_off_X_s, layer2_off_Y_s, sprite_border_q, sprite_visible_q,
				s128_disable, intsnd_en_q, covox_en_q, s_ula_timex_en, s_turbosound, iKeysHard, ula_int_n, 
				s_lp_enabled, s_divmmc_enabled, s_m1_enabled, s_ps2_mode, scandbl_en, disable_contention_q, transparency_fallback_s,
				LoRes_en_s, layer_priorities_s, stereo_mode_q, transparency_index_s, palette_no_auto_inc_s,
				machine_timing, machine_s
				)
				
	begin
	
	
		case register_q is
			
				when X"00" =>
					register_data_s	<=  std_logic_vector(machine_id_g);
					
				when X"01" =>
					register_data_s	<= std_logic_vector(version_g);
					
				when X"02" =>
					register_data_s	<= reg02_data_s;		

				when X"03" =>				
					register_data_s	<= '0' & machine_timing & '0' & machine_s;
		
				when X"05" =>
					register_data_s	<= joy0_mode(1 downto 0) & joy1_mode(1 downto 0) & joy0_mode(2) & ula50_60hz & joy1_mode(2) & scandbl_en;
				
				when X"06" =>
					register_data_s	<= s_ena_turbo & s_dac & s_lp_enabled & s_divmmc_enabled & s_m1_enabled & s_ps2_mode & psg_mode;
					
				when X"07" =>
					register_data_s	<= "000000" & turbo_s;
					
				when X"08" =>
					register_data_s	<= (not s128_disable) & disable_contention_q & stereo_mode_q & intsnd_en_q & covox_en_q & s_ula_timex_en & s_turbosound & s_ntsc_pal;
					
				when X"09" =>
					register_data_s	<= "000000" & scanlines_s;		
					
				when X"0e" =>
					register_data_s	<= std_logic_vector(sub_version_g);
					
				when X"10" =>
					register_data_s	<= "000000" & iKeysHard;
					
				when X"11" => -- 17
					register_data_s 	<= "00000" & machine_video_timing;
		
				when X"12" => -- 18
					register_data_s	<= "00" & layer2_page_s;
					
				when X"13" => -- 19
					register_data_s	<= "00" & layer2_page_shadow_s;
					
				when X"14" => -- 20
					register_data_s	<= transparency_q;
					
				when X"15" => -- 21
					register_data_s	<= LoRes_en_s & "00" & layer_priorities_s & sprite_border_q & sprite_visible_q;
					
				when X"16" => -- 22
					register_data_s	<= layer2_off_X_s;
					
				when X"17" => -- 22
					register_data_s	<= layer2_off_Y_s;
					
				when X"1E" =>	-- 30
					register_data_s	<= "0000000" & vcount_s(8);
					
				when X"1F" =>	-- 31
					register_data_s	<= std_logic_vector(vcount_s(7 downto 0));
					
				when X"22" =>	-- 34
					register_data_s	<= not ula_int_n & "0000" & lineint_ctrl_s & lineint_line_s(8);
					
				when X"23" =>	-- 35
					register_data_s	<= lineint_line_s(7 downto 0);
					
				when X"32" => -- 50
					register_data_s	<= LoRes_off_X_s;
					
				when X"33" => -- 51
					register_data_s	<= LoRes_off_Y_s;

				when X"40" => -- 64
					register_data_s	<= palette_index_s;

				when X"41" => -- 65
					if palette_we_sel_s(1 downto 0) = "00" then
						register_data_s	<= palette_out_ula_s(8 downto 1);
					elsif palette_we_sel_s(1 downto 0) = "01" then
						register_data_s	<= palette_out_lay2_s(8 downto 1);
					else
						register_data_s	<= palette_out_spr_s(8 downto 1);
					end if;
					
				when X"42" => -- 66
					register_data_s	<=	ULAnext_value_s;

				when X"43" => -- 67
					register_data_s	<= palette_no_auto_inc_s & palette_we_sel_s & palette_sel_spr_s & palette_sel_lay2_s & palette_sel_ula_s & disable_flash_s;

				when X"44" => -- 68
					register_data_s(7 downto 1) <= "0000000"; 
				
					if palette_we_sel_s(1 downto 0) = "00" then
						register_data_s(0) <= palette_out_ula_s(0);
					elsif palette_we_sel_s(1 downto 0) = "01" then
						register_data_s(0) <= palette_out_lay2_s(0);
					else
						register_data_s(0 )<= palette_out_spr_s(0); 
					end if;
					
					
				when X"4A" => -- 74
					register_data_s	<= transparency_fallback_s;		

				when X"4B" => -- 75
					register_data_s	<= transparency_index_s;						

				when X"50" => -- 80
					register_data_s	<= MMU0_s;

				when X"51" => -- 81
					register_data_s	<= MMU1_s;

				when X"52" => -- 82
					register_data_s	<= MMU2_s;

				when X"53" => -- 83
					register_data_s	<= MMU3_s;

				when X"54" => -- 84
					register_data_s	<= MMU4_s;

				when X"55" => -- 85
					register_data_s	<= MMU5_s;

				when X"56" => -- 86
					register_data_s	<= MMU6_s;

				when X"57" => -- 87
					register_data_s	<= MMU7_s;				
					
					
					
				when others =>
					register_data_s	<= (others => '0');
		end case;
	end process;







	-- Reset counters
	process (softreset_p_q, iClk_28)
	begin
		if softreset_p_q = '1' then
			softreset_cnt <= (others => '1');
			softreset_q	<= '1';
		elsif rising_edge(iClk_28) then
			if softreset_cnt /= "0000" then
				softreset_cnt <= softreset_cnt - 1;
			else
				softreset_q	<= '0';
			end if;
		end if;
	end process;

	process (hardreset_p_q, iClk_28)
	begin
		if hardreset_p_q = '1' then
			hardreset_cnt <= (others => '1');
			hardreset_q	<= '1';
		elsif rising_edge(iClk_28) then
			if hardreset_cnt /= "0000" then
				hardreset_cnt <= hardreset_cnt - 1;
			else
				hardreset_q	<= '0';
			end if;
		end if;
	end process;


	-- Send config to TOP
	oPS2mode		<= s_ps2_mode;
	oLp_en		<= s_lp_enabled;
	oFlashboot	<= flashboot_q;

	-- 7FFD port = Speccy 128K/+3e
	-- bit5 (unlock) is set at Next regs
	s128_disable			<= port7FFD_reg(5);
	s128_rom_page			<= port7FFD_reg(4);
	s128_shadow				<= port7FFD_reg(3);
	s128_ram_page			<= port7FFD_reg(2 downto 0);

	-- 1FFD port = Speccy +3e
	plus3_page				<= port1FFD_reg(2 downto 1);
	plus3_special			<= port1FFD_reg(0);

	-- ULA
--	ram_contention_s <= 	'0' when disable_contention_q = '1' else 
--							'0' when portDFFD_reg(3 downto 0) /= "0000" else --disable the contention for pages > 7
--							s128_ram_page(0) 	when machine = s_speccy128	else		-- content odd pages
--							s128_ram_page(2)	when machine = s_speccy3e	else		-- content pages >= 4
--							'0';
							
	ram_contention_s <= 		'0' when disable_contention_q = '1' else -- no contention if it was disabled on REGNEXT
								'0' when portDFFD_reg(3 downto 0) /= "0000" else -- disable the contention for pages > 7

								'1' when machine = s_speccy3e and MMU0_s >= X"08" and  MMU0_s <= X"0F" and cpu_a(15 downto 13) = "000" else -- page mapped from 0x0000 to 1xdfff
								'1' when machine = s_speccy3e and MMU1_s >= X"08" and  MMU1_s <= X"0F" and cpu_a(15 downto 13) = "001" else -- page mapped from 0x2000 to 3xffff

								'1' when machine = s_speccy3e and MMU2_s >= X"08" and  MMU2_s <= X"0F" and cpu_a(15 downto 13) = "010" else -- page mapped from 0x4000 to 0x5fff
								'1' when machine = s_speccy3e and MMU3_s >= X"08" and  MMU3_s <= X"0F" and cpu_a(15 downto 13) = "011" else -- page mapped from 0x6000 to 0x7fff

								'1' when machine = s_speccy3e and MMU4_s >= X"08" and  MMU4_s <= X"0F" and cpu_a(15 downto 13) = "100" else -- page mapped from 0x8000 to 0x9fff
								'1' when machine = s_speccy3e and MMU5_s >= X"08" and  MMU5_s <= X"0F" and cpu_a(15 downto 13) = "101" else -- page mapped from 0xa000 to 0xbfff

								'1' when machine = s_speccy3e and MMU6_s >= X"08" and  MMU6_s <= X"0F" and cpu_a(15 downto 13) = "110" else -- page mapped from 0xc000 to 0xdfff
								'1' when machine = s_speccy3e and MMU7_s >= X"08" and  MMU7_s <= X"0F" and cpu_a(15 downto 13) = "111" else -- page mapped from 0xe000 to 0xffff
								
								'1' when machine = s_speccy128 and (cpu_a(15 downto 14) = "01" or (s128_ram_page(0) = '1' and cpu_a(15 downto 14) = "11"))	else		-- content odd pages
								'0';							
							

	vram_shadow  	<= s128_shadow			when machine = s_speccy128 or machine = s_speccy3e else '0';	-- bit indicando qual pagina mostrar se for speccy 128K

	bus_busreq_n_s	<= iCpu_busreq_n 		when machine /= s_config else '1';
	bus_romcs_s		<= iCpu_romcs			when machine /= s_config else '0';
	bus_ramcs_s		<= iCpu_ramcs			when machine /= s_config else '0';
	bus_iorqula_s	<= iCpu_iorqula		when machine /= s_config else '0';

	-- Memory enables
	
	-- Read 0000 to 3FFF (when running bootrom)
	romboot_en		<= '1' when layer2_access_en_s = '0' and cpu_mreq_n = '0' and cpu_rd_n = '0' and cpu_a(15 downto 14) = "00" and bootrom_s = '1' else '0';
	
	-- Read 0000 to 3FFF (when no bootrom)
	romram_en		<= '1' when cpu_mreq_n = '0' and cpu_rd_n = '0' and cpu_a(15 downto 14) = "00" and bootrom_s = '0' and plus3_special = '0'  and bus_romcs_s = '0' 	and MMU0_s = 255 and MMU1_s = 255
							else '0';
	
	-- Write 0000 to 3FFF (when at boot menu)
	romram_wr_en	<= '1' when layer2_access_en_s = '0' and cpu_mreq_n = '0' and cpu_wr_n = '0' and cpu_a(15 downto 14) = "00" and bootrom_s = '0' and machine = s_config else '0';

	-- Read or write 
	-- 8000 to FFFF (always
	-- anywhere (plus3 special mode is on)
	-- 0000 to 1FFF (MMU0 is paged)
	-- 2000 to 3FFF (MMU1 is paged)
	ram_en			<= '1' when cpu_mreq_n = '0' and bus_ramcs_s = '0' and 
															(
															cpu_a(15) = '1' or 
															plus3_special = '1' or
															(cpu_a(15 downto 13) = "000" and MMU0_s /= 255) or
															(cpu_a(15 downto 13) = "001" and MMU1_s /= 255)
															) 
								else '0';

	-- CPU Read or write from 4000 to 7FFF 
	vram_en			<= '1' when cpu_mreq_n = '0' and cpu_a(15 downto 14) = "01" else '0';		

	-- Write from 0000 to 3fff when Layer 2 is active
	layer2_en 	<= '1' when (( layer2_access_en_s = '1' and layer2_access_type_s = '0' and cpu_wr_n = '0' ) or ( layer2_access_en_s = '1' and layer2_access_type_s = '1' ))
											and cpu_mreq_n = '0' and cpu_a(15 downto 14) = "00" else '0';


	-- Address decoding.  Z80 has separate IO and memory address space
	-- IO ports (nominal addresses - incompletely decoded):
	-- 0xXXFE R/W = ULA  -- Feito pelo modulo da ULA
	-- 0x7FFD W   = 128K paging register
	-- 0xFFFD W   = 128K AY-3-8912 register select
	-- 0xFFFD R   = 128K AY-3-8912 register read
	-- 0xBFFD W   = 128K AY-3-8912 register write
	-- 0x1FFD W   = +3 paging and control register
	-- 0x2FFD R   = +3 FDC status register
	-- 0x3FFD R/W = +3 FDC data register

	-- I/O Port enables
	iocs_en			<= '1' when cpu_ioreq_n = '0' and cpu_m1_n = '1'							else '0';
	iord_en        <= '1' when cpu_ioreq_n = '0' and cpu_m1_n = '1' and cpu_rd_n = '0'  else '0';					-- Leitura em alguma porta
	iowr_en        <= '1' when cpu_ioreq_n = '0' and cpu_m1_n = '1' and cpu_wr_n = '0'  else '0';					-- Escrita em alguma porta

	ula_en         <= '1' when iocs_en = '1' and cpu_a(0) = '0' and bus_iorqula_s = '0'	else '0';

	port7FFD_wr_s    <= '1' when iowr_en = '1' and (machine = s_speccy128 or machine = s_speccy3e) and				-- Ativa somente se for escrita e speccy 128K ou +3e
										cpu_a(15 downto 14) = "01" and cpu_a(1 downto 0) = "01"		else '0';				-- Decodificacao parcial

	port1FFD_wr_s		<= '1' when iowr_en = '1' and machine = s_speccy3e and												-- Ativa somente se for escrita e speccy +3e
	                           cpu_a(15 downto 12) = "0001" and cpu_a(1 downto 0) = "01"	else '0';				-- Decodificacao parcial

	portDFFD_wr_s		<= '1' when iowr_en = '1' and (machine = s_speccy128 or machine = s_speccy3e) and			-- Ativa somente se for escrita e speccy 128K ou +3e
	                           cpu_a(15 downto 12) = "1101" and cpu_a(1 downto 0) = "01"	else '0';				-- Decodificacao parcial

	portE3_rd_s	<= '1' 	when iocs_en = '1' and cpu_a(7 downto 0) = X"E3" and cpu_rd_n = '0' else '0';
	
	port243B_rd_s	<= '1' when iocs_en = '1' and cpu_a = X"243B" and cpu_rd_n = '0' else '0';
	port243B_wr_s	<= '1' when iocs_en = '1' and cpu_a = X"243B" and cpu_wr_n = '0' else '0';

	port253B_rd_s	<= '1' when iocs_en = '1' and cpu_a = X"253B" and cpu_rd_n = '0' else '0';
	port253B_wr_s	<= '1' when iocs_en = '1' and cpu_a = X"253B" and cpu_wr_n = '0' else '0';

	port103B_en_s	<= '1' when iowr_en = '1' and cpu_a = X"103B" else '0';	-- RTC SCL (W)
	port113B_en_s	<= '1' when iocs_en = '1' and cpu_a = X"113B" else '0';	-- RTC SDA (R/W)

	port123B_rd_s	<= '1' when iord_en = '1' and cpu_a = X"123B" else '0';	-- Layer 2 port R
	port123B_wr_s	<= '1' when iowr_en = '1' and cpu_a = X"123B" else '0';	-- Layer 2 port W

	port143B_wr_s	<= '1' when iocs_en = '1' and cpu_a = X"143B" and cpu_wr_n = '0'			else '0';	-- UART RX (W)
	port143B_rd_s	<= '1' when iocs_en = '1' and cpu_a = X"143B" and cpu_rd_n = '0'			else '0';	-- UART RX (R)
	port133B_wr_s	<= '1' when iocs_en = '1' and cpu_a = X"133B" and cpu_wr_n = '0'			else '0';	-- UART TX (W)
	port133B_rd_s	<= '1' when iocs_en = '1' and cpu_a = X"133B" and cpu_rd_n = '0'			else '0';	-- UART TX (R)


	-- Memory control
	-- 128K has pageable RAM at 0xc000
	-- +3 has various additional modes in addition to "normal" mode, which is
	-- the same as the 128K
	-- Extra modes assign RAM banks as follows:
	-- plus3_page    0000    4000    8000    C000
	-- 00            0       1       2       3
	-- 01            4       5       6       7
	-- 10            4       5       6       3
	-- 11            4       7       6       3
	-- NORMAL        ROM     5       2       PAGED

--	ram_page <=
--		s128_ram_page																						when cpu_a(15 downto 14) = "11" and machine = s_speccy3e and plus3_special = '0'									else
--		"0" & cpu_a(15 downto 14)																		when 											machine = s_speccy3e and plus3_special = '1' and plus3_page = "00"	else
--		"1" & cpu_a(15 downto 14)																		when 											machine = s_speccy3e and plus3_special = '1' and plus3_page = "01"	else
--		(not(cpu_a(15) and cpu_a(14))) & cpu_a(15 downto 14)									when 											machine = s_speccy3e and plus3_special = '1' and plus3_page = "10" 	else
--		(not(cpu_a(15) and cpu_a(14))) & (cpu_a(15) or cpu_a(14)) & cpu_a(14)			when 											machine = s_speccy3e and plus3_special = '1' and plus3_page = "11"	else
--		s128_ram_page																						when cpu_a(15 downto 14) = "11" and machine = s_speccy128 																else		-- Selectable bank at 0xc000 only for speccy 128K
--		cpu_a(14) & cpu_a(15 downto 14); -- 00=ROM, 01=101 (page 5), 10=010 (page 2), 11=XXX		-- default, even for speccy 48k

	ram_page <=
		--"0000" & s128_ram_page																				when cpu_a(15 downto 14) = "11" and machine = s_speccy3e and plus3_special = '0'									else
		portDFFD_reg(3 downto 0) & s128_ram_page														when cpu_a(15 downto 14) = "11" and  machine = s_speccy3e and plus3_special = '0'									else
		"00000" & cpu_a(15 downto 14)																		when 											machine = s_speccy3e and plus3_special = '1' and plus3_page = "00"	else
		"00001" & cpu_a(15 downto 14)																		when 											machine = s_speccy3e and plus3_special = '1' and plus3_page = "01"	else
		"0000" & (not(cpu_a(15) and cpu_a(14))) & cpu_a(15 downto 14)							when 											machine = s_speccy3e and plus3_special = '1' and plus3_page = "10" 	else
		"0000" & (not(cpu_a(15) and cpu_a(14))) & (cpu_a(15) or cpu_a(14)) & cpu_a(14)	when 											machine = s_speccy3e and plus3_special = '1' and plus3_page = "11"	else
		portDFFD_reg(3 downto 0) & s128_ram_page														when cpu_a(15 downto 14) = "11" and machine = s_speccy128 																else		-- Selectable bank at 0xc000 only for speccy 128K
		"0000" & cpu_a(14) & cpu_a(15 downto 14); -- 00=ROM, 01=101 (page 5), 10=010 (page 2), 11=XXX		-- default, even for speccy 48k

		
	-- RAM Map 																IPL page
	-- 0x000000 to 0x00FFFF  (64K) Speccy ROM				A20..16 = 00000			0x00-0x03
	-- 0x010000 to 0x01FFFF  (64K) ESXDOS e M1			A20..16 = 00001			0x04-0x07
	--		0x010000 - ESXDOS rom (16K)	 					A20..14 = 0000100		0x04
	--		0x014000 - M1 rom (16K)		 						A20..14 = 0000101		0x05
	--		0x018000 - M1 extra rom (16K) 					A20..14 = 0000110		0x06
	--		0x01C000 - M1 ram (16K)		 						A20..14 = 0000111		0x07
	-- 0x020000 to 0x03FFFF (128K) DivMMC RAM  			A20..16 = 00010, 00011	0x08-0x0F
	-- 0x040000 to 0x05FFFF (128K) Speccy RAM				A20..16 = 00100, 00101	0x10-0x17
	-- 0x060000 to 0x07FFFF (128K) Extra RAM 				A20..16 = 00110, 00111	0x18-0x1F

	-- 0x080000 to 0x0FFFFF (512K) 1st Extra IC RAM		A20..16 = 01000, 01111	
	-- 0x100000 to 0x17FFFF (512K) 2nd Extra IC RAM		A20..16 = 10000, 10111	
	-- 0x180000 to 0xFFFFFF (512K) 3rd Extra IC RAM		A20..16 = 11000, 11111	

		process (machine, romram_en, cpu_a, s128_rom_page, plus3_page, divmmc_bank,
					divmmc_rom_en, vram_en, layer2_en, layer2_subpage_s,
					ram_page, romram_page, divmmc_ram_en, s_m1_rom_cs, s_m1_ram_cs,
					layer2_write_shadow_s, layer2_page_s, layer2_page_shadow_s,
					layer2_addr_s, portDFFD_reg, vram_a, MMU0_s, MMU1_s, MMU2_s, MMU3_s, MMU4_s, MMU5_s, MMU6_s, MMU7_s,
					LoRes_addr_s, LoRes_en_s, plus3_special
					)

					variable layer2_page_v	: unsigned(5 downto 0);
					variable layer2_subpage_v	: unsigned(5 downto 0);
					variable ram_addr3_v	: std_logic_vector(7 downto 0) := (others=>'0');
					
					function mmu_selector(msb_i: in std_logic_vector(2 downto 0))
						return std_logic_vector is
					begin
						case msb_i is
						  when "000" => return "001";
						  when "001" => return "010";
						  when "010" => return "011";
						  when "011" => return "100";
						  when "100" => return "101";
						  when "101" => return "110";
						  when "110" => return "111";
						  when others => return "111";
						end case;
					end mmu_selector;
		begin
		
					ram_addr3_v := (others=>'0');

					if layer2_write_shadow_s = '0' then
						layer2_subpage_v := unsigned(layer2_page_s) + unsigned(layer2_subpage_s);
					else
						layer2_subpage_v := unsigned(layer2_page_shadow_s) + unsigned(layer2_subpage_s);
					end if;
					
					-- Read or write from 4000 to 7FFF
					if vram_en = '1' then 
					
						if 	cpu_a(15 downto 13) = "010" then -- 4000 
							--ram_addr_v := "00100000" + unsigned(MMU2_s);
							ram_addr3_v := mmu_selector(MMU2_s(7 downto 5)) & MMU2_s(4 downto 0);
							
						elsif cpu_a(15 downto 13) = "011" then -- 6000
							--ram_addr_v := "00100000" + unsigned(MMU3_s);
							ram_addr3_v := mmu_selector(MMU3_s(7 downto 5)) & MMU3_s(4 downto 0);
							
						end if;
						ram_addr <= std_logic_vector(ram_addr3_v) & cpu_a(12 downto 0);
						
						--	ram_addr <= "0010101" & cpu_a(13 downto 0);		--10101					-- VRAM access by CPU
							
					-- Write from 0000 to 3fff when Layer 2 is active
					elsif layer2_en = '1' then 
							--ram_addr <= "0110" & layer2_subpage_s & cpu_a(13 downto 0); --010		-- layer2 access by CPU (write)
							ram_addr <= "001" & std_logic_vector(layer2_subpage_v(3 downto 0)) & cpu_a(13 downto 0); --010		-- layer2 access by CPU (write)
							
					-- boot menu only
					elsif machine = s_config then

							if cpu_a(15) = '1' then
								ram_addr <= "001" & ram_page(3 downto 0) & cpu_a(13 downto 0); --10
							else
								ram_addr <= "00" & romram_page & cpu_a(13 downto 0);
							end if;

					-- divMMC RAM
					elsif divmmc_ram_en = '1' then
							ram_addr <= "0001" & divmmc_bank(3 downto 0) & cpu_a(12 downto 0); -- 00
							
					-- Multiface ROM
					elsif s_m1_rom_cs = '0' then
							ram_addr <= "0000101" & cpu_a(13 downto 0); --11001
							
					-- Multiface RAM
					elsif s_m1_ram_cs = '0' then
							ram_addr <= "0000111" & cpu_a(13 downto 0); --11011
							
					-- Read 0000 to 3FFF 
					elsif romram_en = '1' then
							if divmmc_rom_en = '1' then
									ram_addr <= "0000100" & cpu_a(13 downto 0); --11000
									
							elsif machine = s_speccy48 then
									ram_addr <= "0000000" & cpu_a(13 downto 0); --11100
									
							elsif machine = s_speccy128 then
									ram_addr <= "000000" & s128_rom_page & cpu_a(13 downto 0); -- 1110
									
							else -- speccy3e
									ram_addr <= "00000" & plus3_page(1) & s128_rom_page & cpu_a(13 downto 0); --111
							end if;
							
					-- Read or write 
					-- 8000 to FFFF (always)
					-- anywhere (plus3 special mode on)
					-- 0000 to 1FFF (MMU0 is paged)
					-- 2000 to 3FFF (MMU1 is paged)
					else	
					
					-- +3 special mode overrides the MMU
						if plus3_special = '1' then
								
								ram_addr <= "001" &  ram_page(3 downto 0) & cpu_a(13 downto 0); 
											
						else
						
							if 	cpu_a(15 downto 13) = "000" and MMU0_s /= 255 then -- 0000 
									--ram_addr_v := "00100000" + unsigned(MMU0_s);
									ram_addr3_v := mmu_selector(MMU0_s(7 downto 5)) & MMU0_s(4 downto 0);
								
							elsif 	cpu_a(15 downto 13) = "001" and MMU1_s /= 255 then -- 2000 
									--ram_addr_v := "00100000" + unsigned(MMU1_s);
									ram_addr3_v := mmu_selector(MMU1_s(7 downto 5)) & MMU1_s(4 downto 0);

							elsif 	cpu_a(15 downto 13) = "100" then -- 8000 
									--ram_addr_v := "00100000" + unsigned(MMU4_s);
									ram_addr3_v := mmu_selector(MMU4_s(7 downto 5)) & MMU4_s(4 downto 0);
									
							elsif cpu_a(15 downto 13) = "101" then -- A000
									--ram_addr_v := "00100000" + unsigned(MMU5_s);
									ram_addr3_v := mmu_selector(MMU5_s(7 downto 5)) & MMU5_s(4 downto 0);
									
							elsif cpu_a(15 downto 13) = "110" then -- c000
									--ram_addr_v := "00100000" + unsigned(MMU6_s);
									ram_addr3_v := mmu_selector(MMU6_s(7 downto 5)) & MMU6_s(4 downto 0);
									
							elsif cpu_a(15 downto 13) = "111" then -- E000
									--ram_addr_v := "00100000" + unsigned(MMU7_s);
									ram_addr3_v := mmu_selector(MMU7_s(7 downto 5)) & MMU7_s(4 downto 0);
									
							end if;
							
							ram_addr <= std_logic_vector(ram_addr3_v) & cpu_a(12 downto 0);
						end if;
						
						--	ram_addr <= "01" &  ram_page(3 downto 0) & cpu_a(13 downto 0); --10
						
					end if;


					if  LoRes_en_s = '1' then
							oVram_a		<= "00101" & LoRes_addr_s; 
					else 
							oVram_a		<= "00101" & vram_a ; -- 101
					end if;
	
					layer2_page_v := unsigned(layer2_page_s) + unsigned(layer2_addr_s(15 downto 14));
					olayer2_addr	<= "001" & std_logic_vector(layer2_page_v(3 downto 0)) & layer2_addr_s(13 downto 0); --010 -- layer2 (read)
				
			end process;
	



	-- write to port 7FFD (config speccy 128/+3e)
--	process(reset_s, iClk_cpu)
--	begin
--		if reset_s = '1' then
--			port7FFD_reg <= (others => '0');
--		elsif falling_edge(iClk_cpu) then
--			if port7FFD_wr_s = '1' and s128_disable = '0' then						-- Se bit 5 da porta 7FFD for 1, desabilita escrita na porta
--				port7FFD_reg <= cpu_do;														-- Ler 8 bits
--			elsif port253B_wr_s = '1' and register_q = x"08" then
--				port7FFD_reg(5) <= not cpu_do(7);
--			end if;
			
--		end if;
--	end process;

	-- write to port 1FFD (config speccy +3e)
	process(reset_s, iClk_cpu)
	begin
		if reset_s = '1' then
			port1FFD_reg <= (others => '0');
		elsif falling_edge(iClk_cpu) then
			if port1FFD_wr_s = '1' and s128_disable = '0' then
				port1FFD_reg <= cpu_do;														-- Ler 8 bits
			end if;
		end if;
	end process;

	-- write to port DFFD (similar to ZX PROFI 1024k)
	process(reset_s, iClk_cpu)
	begin
		if reset_s = '1' then
			portDFFD_reg <= (others => '0');
		elsif falling_edge(iClk_cpu) then
			if portDFFD_wr_s = '1' and s128_disable = '0' then
				portDFFD_reg <= cpu_do;														-- Ler 8 bits
			end if;
		end if;
	end process;


	-- RTC SCL and SDA write
	process(reset_s, iClk_cpu)
	begin
		if reset_s = '1' then
			rtc_scl_s <= '1';
			rtc_sda_s <= '1';
		elsif falling_edge(iClk_cpu) then
			if port103B_en_s = '1' then
				rtc_scl_s <= cpu_do(0);
			elsif port113B_en_s = '1' and cpu_wr_n = '0' then
				rtc_sda_s <= cpu_do(0);
			end if;
		end if;
	end process;

	-- RTC
	ioRTC_scl	<= '0' when rtc_scl_s = '0' else 'Z';
	ioRTC_sda	<= '0' when rtc_sda_s = '0' else 'Z';
	
	ioPi_sda		<= '0' when rtc_scl_s = '0' else 'Z';
	ioPi_scl		<= '0' when rtc_sda_s = '0' else 'Z';


	-- Conexoes dos barramentos
	ula_din  <= cpu_do;
	ram_din  <= cpu_do;
	--psg_din  <= cpu_do;
	cpu_di <=
			-- Memory
			rom_dout						when romboot_en     = '1'        else		-- Leitura da bootrom interna
			ram_dout						when divmmc_ram_en  = '1'			else		-- Leitura e/ou escrita na RAM da DivMMC quando chaveada no lugar da ROM (0000 - 3FFF)
			ram_dout						when romram_en      = '1'			else		-- Leitura da ROM
			ram_dout						when vram_en        = '1'			else		-- Leitura da VRAM (pelo canal da CPU)
			ram_dout						when layer2_en  		= '1'			else		-- Leitura da new VRAM (pelo canal da CPU)
			ram_dout						when ram_en         = '1'			else		-- Leitura da RAM alta
			ram_dout						when s_m1_ram_cs    = '0'			else		-- Leitura da RAM m1
			ram_dout						when s_m1_rom_cs    = '0'			else		-- Leitura da ROM m1
			-- I/O
			s_mouse_d 					when s_mouse_out 	  = '1' 			else		-- Leitura portas do Mouse Kempston
			kempston_dout				when s_kj_out       = '1'			else		-- Leitura da porta Kempston (1F) (tem que ser antes de divmmc_en)
			ula_dout						when ula_hd_s       = '1'			else		-- Leitura de alguma porta da ULA
			register_data_s			when port253B_rd_s  = '1'			else		-- Register read
			dma_data_s					when dma_out_s		  = '1'        else     -- reading DMA status
			psg_do						when psg_dout       = '1'			else		-- Leitura da porta FFFD (AY)
			
			"0000000" & s_lp_d0 		when s_lp_out		  = '1'			else     -- Light Pen (3F)  (tem que ser antes de divmmc_en)
			s_m1_do				 		when s_m1_dout		  = '1'			else     -- Multiface 128
			divmmc_do					when divmmc_dout    = '1'			else		-- Leitura das portas da interface DivMMC
			divmmc_e3_s					when portE3_rd_s    = '1'			else		-- Leitura das portas E3 da DivMMC
			register_q 					when port243B_rd_s  = '1'        else 		-- Read current next IO port number
			port123B_reg_s				when port123B_rd_s  = '1'        else 
			"0000000" & ioRTC_sda	when port113B_en_s  = '1'			else		-- RTC SDA reading
			fifo_next_byte_s			when port143B_rd_s  = '1' 		   else		-- UART reading when character is present at FIFO
			"00000" & fifo_full_s &	 uart_tx_active_s & (not fifo_empty_s) when port133B_rd_s  = '1' 	else
			sprite_data_s				when sprite_hd_s    = '1'			else		-- Sprites status flag reading
			data_temp					when cpu_a= X"FF01" and iord_en    = '1'	else		-- just for testing
			iCpu_di;

	-- Ligacao dos sinais das memorias com o mundo externo

	vram_dout	<= iVram_dout;
	oVram_cs		<= vram_cs or LoRes_en_s;
	oVram_rd		<= vram_oe;

	oRam_a		<= ram_addr;
	oRam_din		<= ram_din;
	ram_dout		<= iRam_dout;
	oRam_cs		<= (romram_en or vram_en or layer2_en or ram_en or divmmc_ram_en or romram_wr_en or (not s_m1_ram_cs) or (not s_m1_rom_cs));
	oRam_rd		<= not cpu_rd_n;
	oRam_wr		<= not cpu_wr_n;

	-- bootrom interna
	oBootrom_en	<= bootrom_s;
	oRom_a		<= cpu_a(13 downto 0);
	rom_dout		<= iRom_dout;


	--olayer2_cs		<= layer2_visible_en_s and (not layer2_blank_s);

	

	-- port 0x123B = 4667
	-- bit 7 and 6 = Layer 2 page selection ("00", "01" or "10")
	-- bit 5 and 4 = not used
	-- bit 3 =  "0" page select for writing is selected by reg 18 (visible layer 2)
	--          "1" page select for writing is selected by reg 19 (shadow layer 2)
	-- bit 2 = 	"0" page selected is write only, ZX ROM visible at 0000-3FFF
	--				"1" page select visible
	-- bit 0 =  "0" Layer 2 read and write disabled

	process(reset_s, iClk_cpu)
		variable cpu_visible_v	: std_logic;
	begin
		if reset_s = '1' then
			layer2_subpage_s			<= "00";
			layer2_write_shadow_s	<= '0';
			layer2_access_type_s	<= '0';
			cpu_visible_v				:= '0';
			layer2_visible_en_s		<= '0';
			layer2_access_en_s 		<= '0';

		elsif falling_edge(iClk_cpu) then
			if port123B_wr_s = '1' and use_layer2_g then
				layer2_subpage_s			<= cpu_do(7 downto 6);
				layer2_write_shadow_s	<= cpu_do(3);
				layer2_access_type_s	<= cpu_do(2);
				cpu_visible_v				:= cpu_do(1);
				layer2_access_en_s 		<= cpu_do(0);
				port123B_reg_s <= cpu_do;
			end if;
			if (cpu_visible_v = '1' and vram_shadow = '0') then
				layer2_visible_en_s <= '1';
			else
				layer2_visible_en_s <= '0';	-- No Layer2 on the second video page
			end if;
		end if;
	end process;

	-- RGB
	oRGB_hs_n	<= ula_hsync_n;
	oRGB_vs_n	<= ula_vsync_n;
	oRGB_cs_n	<= ula_hsync_n and ula_vsync_n;
	oScanlines	<= scanlines_s;
	oScandbl_en	<= scandbl_en;
	oULA50_60	<= ula50_60hz;
	oMachTiming	<= machine_timing;
	oMachVideoTiming <= machine_video_timing;
	oNTSC_PAL	<= s_ntsc_pal;

	-- RGB Mixer
--	rgb_r_s <= LoRes_R_s when LoRes_en_s = '1' and LoRes_blank_s = '0' else ula_r;
--	rgb_g_s <= LoRes_G_s when LoRes_en_s = '1' and LoRes_blank_s = '0' else ula_g;
--	rgb_b_s <= LoRes_B_s & LoRes_B_s(0) when LoRes_en_s = '1' and LoRes_blank_s = '0' else ula_b;

	

	-- mesmo desabilitando os pixels do lores, ainda temos a borda
	process (LoRes_en_s, LoRes_blank_s, LoRes_R_s, LoRes_G_s, LoRes_B_s, ula_r, ula_g, ula_b, transparency_q, cliped_s)
	begin
		if cliped_s = '1' then	
			rgb_r_s <= transparency_q(7 downto 5); 
			rgb_g_s <= transparency_q(4 downto 2); 
			rgb_b_s <= transparency_q(1 downto 0) & '0'; 	
		elsif LoRes_en_s = '1' and LoRes_blank_s = '0' then
			rgb_r_s <= LoRes_R_s;
			rgb_g_s <= LoRes_G_s;
			rgb_b_s <= LoRes_B_s;
		else
			rgb_r_s <= ula_r; --vem daqui
			rgb_g_s <= ula_g;
			rgb_b_s <= ula_b;
		end if;
		
	end process;
	
	-- rgb_r_s <= transparency_q 
	
	
	
	oRGB_hb_n	<= hblank_n_s;
	oRGB_vb_n	<= vblank_n_s;
	oRGB_hcnt	<= hcount_s;
	oRGB_vcnt	<=	vcount_s;


	--ULA_rgb_transp_s <= '1' when rgb_r_s & rgb_g_s & rgb_b_s (2 downto 1) = transparency_q and LoRes_blank_s = '1' and layer2_blank_s = '1' else '0';
	
	ULA_rgb_transp_s <= '1' when rgb_r_s & rgb_g_s & rgb_b_s (2 downto 1) = transparency_q else '0';
	

	--process (iClk_14)
	--begin
	--	if rising_edge(iClk_14) then -- must be rising edge
	--		ov_clk_cs_s <= not ov_clk_cs_s;
	--	end if;
	--end process;
	
	--olayer2_cs		<= layer2_visible_en_s and (not layer2_blank_s) and layer2_cs_s and ( ov_clk_cs_s);
	--olayer2_cs		<= layer2_visible_en_s and layer2_cs_s and ( ov_clk_cs_s);

	--process (ov_clk_cs_s, layer2_visible_en_s, layer2_blank_s, layer2_cs_s)
	--begin
	--	if ov_clk_cs_s = '0' then
	--		olayer2_cs    <= layer2_visible_en_s and layer2_cs_s; --(not layer2_blank_s) and layer2_cs_s;
	--	else
	--		olayer2_cs    <= '0';
	--	end if;
   --end process;
   
   olayer2_cs <= layer2_cs_s and layer2_visible_en_s;
	

	process (iClk_28, iClk_14)
		variable layer2_pixel_t : std_logic;
	begin
		if rising_edge(iClk_14) then
			
			oRGB_r <= transparency_fallback_s(7 downto 5);
			oRGB_g <= transparency_fallback_s(4 downto 2);
			oRGB_b <= transparency_fallback_s(1 downto 0) & '0';

			if (layer2_R_s & layer2_G_s & layer2_B_s(2 downto 1) = transparency_q) or layer2_blank_s = '1' then-- or layer2_cs_s = '0' or layer2_visible_en_s = '0') then 
					layer2_pixel_t := '0';
			else
					layer2_pixel_t := '1';
			end if;
			
			
			-- Layer priorities - reg 21 (0x15) - bits 4-2
			-- the default is 000,  sprites over the layer 2 over the ULA
			-- 000 S L U
			-- 001 L S U
			-- 010 S U L
			-- 011 L U S
			-- 100 U S L
			-- 101 U L S

			if layer_priorities_s = "000" then -- SLU
			
					if sprite_pixel_s = '1' and sprite_visible_q = '1'	then
						oRGB_r <= sprite_RGB_s(8 downto 6);
						oRGB_g <= sprite_RGB_s(5 downto 3);
						oRGB_b <= sprite_RGB_s(2 downto 0);
						
					elsif layer2_pixel_t = '1' and layer2_visible_en_s = '1' then 
						 oRGB_r <= layer2_R_s;
						 oRGB_g <= layer2_G_s;
						 oRGB_b <= layer2_B_s;
						 
					elsif ULA_rgb_transp_s = '0' then
						 oRGB_r <= rgb_r_s;
						 oRGB_g <= rgb_g_s;
						 oRGB_b <= rgb_b_s;
						 
					end if;
					
			elsif layer_priorities_s = "001" then -- LSU
			
					if layer2_pixel_t = '1' and layer2_visible_en_s = '1' then 
						 oRGB_r <= layer2_R_s;
						 oRGB_g <= layer2_G_s;
						 oRGB_b <= layer2_B_s;
						 
					elsif sprite_pixel_s = '1' and sprite_visible_q = '1'	then
						oRGB_r <= sprite_RGB_s(8 downto 6);
						oRGB_g <= sprite_RGB_s(5 downto 3);
						oRGB_b <= sprite_RGB_s(2 downto 0);
						
					elsif ULA_rgb_transp_s = '0' then
						 oRGB_r <= rgb_r_s;
						 oRGB_g <= rgb_g_s;
						 oRGB_b <= rgb_b_s;
						 
					end if;
					
			elsif layer_priorities_s = "010" then -- SUL
			
					if sprite_pixel_s = '1' and sprite_visible_q = '1'	then
						oRGB_r <= sprite_RGB_s(8 downto 6);
						oRGB_g <= sprite_RGB_s(5 downto 3);
						oRGB_b <= sprite_RGB_s(2 downto 0);
						
					elsif ULA_rgb_transp_s = '0' then
						 oRGB_r <= rgb_r_s;
						 oRGB_g <= rgb_g_s;
						 oRGB_b <= rgb_b_s;
						 
					elsif layer2_pixel_t = '1' and layer2_visible_en_s = '1' then 
						 oRGB_r <= layer2_R_s;
						 oRGB_g <= layer2_G_s;
						 oRGB_b <= layer2_B_s;
						 
					end if;
					
			elsif layer_priorities_s = "011" then -- LUS
			
					if layer2_pixel_t = '1' and layer2_visible_en_s = '1' then 
						 oRGB_r <= layer2_R_s;
						 oRGB_g <= layer2_G_s;
						 oRGB_b <= layer2_B_s;
						 
					elsif ULA_rgb_transp_s = '0' then
						 oRGB_r <= rgb_r_s;
						 oRGB_g <= rgb_g_s;
						 oRGB_b <= rgb_b_s;
						 
					elsif sprite_pixel_s = '1' and sprite_visible_q = '1'	then
						oRGB_r <= sprite_RGB_s(8 downto 6);
						oRGB_g <= sprite_RGB_s(5 downto 3);
						oRGB_b <= sprite_RGB_s(2 downto 0);
												 
					end if;
						
			elsif layer_priorities_s = "100" then -- USL
			
					if ULA_rgb_transp_s = '0' then
						 oRGB_r <= rgb_r_s;
						 oRGB_g <= rgb_g_s;
						 oRGB_b <= rgb_b_s;
					
					elsif sprite_pixel_s = '1' and sprite_visible_q = '1'	then
						oRGB_r <= sprite_RGB_s(8 downto 6);
						oRGB_g <= sprite_RGB_s(5 downto 3);
						oRGB_b <= sprite_RGB_s(2 downto 0);
					
					elsif layer2_pixel_t = '1' and layer2_visible_en_s = '1' then 
						 oRGB_r <= layer2_R_s;
						 oRGB_g <= layer2_G_s;
						 oRGB_b <= layer2_B_s;
						 		 
					end if;
					
			elsif layer_priorities_s = "101" then -- ULS
			
					if ULA_rgb_transp_s = '0' then
						 oRGB_r <= rgb_r_s;
						 oRGB_g <= rgb_g_s;
						 oRGB_b <= rgb_b_s;
					
					elsif layer2_pixel_t = '1' and layer2_visible_en_s = '1' then 
						 oRGB_r <= layer2_R_s;
						 oRGB_g <= layer2_G_s;
						 oRGB_b <= layer2_B_s;
						 		 
					elsif sprite_pixel_s = '1' and sprite_visible_q = '1'	then
						oRGB_r <= sprite_RGB_s(8 downto 6);
						oRGB_g <= sprite_RGB_s(5 downto 3);
						oRGB_b <= sprite_RGB_s(2 downto 0);
					end if;
			end if;
		end if;
	end process;

	-- Audio
	ula_ear	<= iEAR;
	oSPK		<= ula_spk;
	oMIC		<=	ula_mic;
	oDAC		<= s_dac;
	oIntSnd	<= intsnd_en_q;

	-- Keyboard
	oPort254_cs		<= '1'	when cpu_ioreq_n = '0' and cpu_m1_n = '1' and cpu_rd_n = '0' and cpu_a(0) = '0'	else '0';
	oKeymap_addr	<= std_logic_vector(keymap_addr_q);
	oKeymap_data	<= keymap_data_q;
	oKeymap_we		<= keymap_we_s;

	-- Bus
	oCpu_a 			<= cpu_a;		--when machine /= s_config else (others => '0');
	oCpu_do 			<= cpu_do		when machine /= s_config else (others => '0');
	oCpu_ioreq 		<= cpu_ioreq_n when machine /= s_config else '1';
	oCpu_wr			<= cpu_wr_n    when machine /= s_config else '1';
	oCpu_rd			<= cpu_rd_n    when machine /= s_config else '1';
	oCpu_mreq		<= cpu_mreq_n  when machine /= s_config else '1';
	oCpu_m1			<= cpu_m1_n    when machine /= s_config else '1';
	oCpu_clock		<= iClk_cpu;
	
	
	--for external timing.ini reading
	-- Timing TV
--	h_visible_o		<= timing_q(0) when ula50_60hz = '0' else timing_q(8);	
--	hsync_start_o	<= timing_q(1) when ula50_60hz = '0' else timing_q(9);	
--	hsync_end_o		<= timing_q(2) when ula50_60hz = '0' else timing_q(10);	
--	hcnt_end_o		<= timing_q(3) when ula50_60hz = '0' else timing_q(11);	
--	v_visible_o		<= timing_q(4) when ula50_60hz = '0' else timing_q(12);
--	vsync_start_o	<= timing_q(5) when ula50_60hz = '0' else timing_q(13);
--	vsync_end_o		<= timing_q(6) when ula50_60hz = '0' else timing_q(14);
--	vcnt_end_o		<= timing_q(7) when ula50_60hz = '0' else timing_q(15);
--	
--	-- Timings ULA
--	hblank_min_s	<= timing_q(16)(8 downto 0) when ula50_60hz = '0' else timing_q(26)(8 downto 0);
--	hsync_min_s		<= timing_q(17)(8 downto 0) when ula50_60hz = '0' else timing_q(27)(8 downto 0);
--	hsync_max_s		<= timing_q(18)(8 downto 0) when ula50_60hz = '0' else timing_q(28)(8 downto 0);
--	hblank_max_s	<= timing_q(19)(8 downto 0) when ula50_60hz = '0' else timing_q(29)(8 downto 0);
--	max_hc_s			<= timing_q(20)(8 downto 0) when ula50_60hz = '0' else timing_q(30)(8 downto 0);
--	vblank_min_s	<= timing_q(21)(8 downto 0) when ula50_60hz = '0' else timing_q(31)(8 downto 0);
--	vsync_min_s		<= timing_q(22)(8 downto 0) when ula50_60hz = '0' else timing_q(32)(8 downto 0);
--	vsync_max_s		<= timing_q(23)(8 downto 0) when ula50_60hz = '0' else timing_q(33)(8 downto 0);
--	vblank_max_s	<= timing_q(24)(8 downto 0) when ula50_60hz = '0' else timing_q(34)(8 downto 0);
--	max_vc_s			<= timing_q(25)(8 downto 0) when ula50_60hz = '0' else timing_q(35)(8 downto 0);
	

	-- Defaults
	-- Modeline "720x576x50hz" 	27 	720  	732  	796  	864   	576  	581  	586  	625 
	-- ModeLine "720x480@60hz" 	27 	720 	736 	798 	858 		480 	489 	495 	525 
--	process (ula50_60_s)
--	begin
--		if ula50_60_s = '0' then --50hz
--
--			h_visible_s		<= tv50_visible_h; --720 - 1;
--			hsync_start_s	<= tv50_hsync_sta; --732 - 1;
--			hsync_end_s		<= tv50_hsync_end; --796 - 1;
--			hcnt_end_s		<= tv50_cnt_h_end; --864 - 1;
--			--
--			v_visible_s		<= 576 - 1;
--			vsync_start_s	<= 581 - 1;
--			vsync_end_s		<= 586 - 1;
--			vcnt_end_s		<= 625 - 2;
--			
--		else -- 60hz
--		
--			h_visible_s		<= 720 - 1;
--			hsync_start_s	<= 736 - 1;
--			hsync_end_s		<= 798 - 1;
--			hcnt_end_s		<= 858 - 1;
--			--
--			v_visible_s		<= 480 - 1;
--			vsync_start_s	<= 489 - 1;
--			vsync_end_s		<= 495 - 1;
--			vcnt_end_s		<= 525 - 2;
--			
--		end if;
--	end process;

	
	----------------------------
	-- debugs
	----------------------------

--	oD_others(0) <= dma_busreq_s;
--	oD_others(1) <= softreset_q;
--	oD_others(2) <= bootrom_s;
--	oD_others(3) <= turbo_s(0);
--	oD_others(4) <= turbo_s(1);
--	oD_others(5) <= divmmc_rom_en;
	--oD_others(6) <= '0';
	--oD_others(7) <= dma_busreq_s;
--	oD_reg_o	<= reg02_data_s;

	--oD_others(0) <= dma_a_s(10) when dma_mreq_n_s = '0' and dma_wr_n_s = '0';

--	oD_reg_o <= dma_data_s					when dma_out_s		  = '1'  ;

--	oD_reg_o <= register_temp & data_temp;

	--oD_others(1 downto 0) <= clip_layer2_idx_s;
	oD_others(3) <= copper_we_s;
	oD_others(2 downto 1) <= copper_en_s;
	oD_others(0) <= copper_dout_s;
	
end architecture;
