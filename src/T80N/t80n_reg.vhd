--
-- T80 Registers, technology independent
--
-- Version : 0244
--
-- Copyright (c) 2002 Daniel Wallner (jesus@opencores.org)
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- Please report bugs to the author, but before you do so, please
-- make sure that this is not a derivative work and that
-- you have the latest version of this file.
--
-- The latest version of this file can be found at:
--	http://www.opencores.org/cvsweb.shtml/t51/
--
-- Limitations :
--
-- File history :
--
--	0242 : Initial release
--
--	0244 : Changed to single register file
--

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity T80N_Reg is
	port(
		Clk		: in std_logic;
		CEN		: in std_logic;
		WEH		: in std_logic;
		WEL		: in std_logic;
		
		AddrA		: in std_logic_vector(2 downto 0);
		AddrB		: in std_logic_vector(2 downto 0);
		AddrC		: in std_logic_vector(2 downto 0);
		
		DIH		: in std_logic_vector(7 downto 0);
		DIL		: in std_logic_vector(7 downto 0);
		
		WEH_B		: in std_logic;
		WEL_B		: in std_logic;		
		DIH_B		: in std_logic_vector(7 downto 0);
		DIL_B		: in std_logic_vector(7 downto 0);
		
		DOAH		: out std_logic_vector(7 downto 0);
		DOAL		: out std_logic_vector(7 downto 0);
		DOBH		: out std_logic_vector(7 downto 0);
		DOBL		: out std_logic_vector(7 downto 0);
		DOCH		: out std_logic_vector(7 downto 0);
		DOCL		: out std_logic_vector(7 downto 0);
		
		--extended 
		reg_wr_i : in std_logic_vector(5 downto 0) := (others=>'0'); --BCHLDE
		reg_HL_i : in std_logic_vector(15 downto 0);
		reg_DE_i : in std_logic_vector(15 downto 0);
		reg_BC_i : in std_logic_vector(15 downto 0);
		reg_BC_o : out std_logic_vector(15 downto 0);
		reg_HL_o : out std_logic_vector(15 downto 0);
		reg_DE_o : out std_logic_vector(15 downto 0)
		
	);
end T80N_Reg;

architecture rtl of T80N_Reg is

	type Register_Image is array (natural range <>) of std_logic_vector(7 downto 0);
	signal	RegsH	: Register_Image(0 to 7);
	signal	RegsL	: Register_Image(0 to 7);

	signal	RegHL_s	: std_logic_vector(15 downto 0);
	signal	RegDE_s	: std_logic_vector(15 downto 0);
	signal	RegBC_s	: std_logic_vector(15 downto 0);

	
	-- 00 = BC
	-- 01 - DE
	-- 10 - HL
	
	
	--regsH(0) - B	
	--regsH(1) - D
	--regsH(2) - H
	--regsH(4) - B'
	--regsH(5) - D'
	--regsH(6) - H'

	
	--regsL(0) - C
	--regsL(1) - E
	--regsL(2) - L
	--regsL(4) - C'
	--regsL(5) - E'
	--regsL(6) - L'
	
	
begin

	process (Clk)
	begin
		if Clk'event and Clk = '1' then
			if CEN = '1' then
			
					if WEH = '1' then
						RegsH(to_integer(unsigned(AddrA))) <= DIH;
						
						case  to_integer(unsigned(AddrA)) is
							when 0 => RegBC_s(15 downto 8) <= DIH;
							when 1 => RegDE_s(15 downto 8) <= DIH;
							when 2 => RegHL_s(15 downto 8) <= DIH;
							when others => null;
						end case;
						
					end if;
					
					if WEL = '1' then
						RegsL(to_integer(unsigned(AddrA))) <= DIL;
						
						case  to_integer(unsigned(AddrA)) is
							when 0 => RegBC_s(7 downto 0) <= DIL;
							when 1 => RegDE_s(7 downto 0) <= DIL;
							when 2 => RegHL_s(7 downto 0) <= DIL;
							when others => null;
						end case;
						
					end if;
		
					if WEH_B = '1' then
						RegsH(to_integer(unsigned(AddrB))) <= DIH_B;
					end if;
					
					if WEL_B = '1' then
						RegsL(to_integer(unsigned(AddrB))) <= DIL_B;	
					end if;
					
			end if;
			
			if reg_wr_i(5) = '1' then --B
				RegBC_s(15 downto 8) <= reg_BC_i(15 downto 8);
				RegsH(0) <= reg_BC_i(15 downto 8);
			end if;
			if reg_wr_i(4) = '1' then --C
				RegBC_s(7 downto 0) <= reg_BC_i(7 downto 0);
				RegsL(0) <= reg_BC_i(7 downto 0);
			end if;
			if reg_wr_i(3) = '1' then --H
				RegHL_s(15 downto 8) <= reg_HL_i(15 downto 8);
				RegsH(2) <= reg_HL_i(15 downto 8);
			end if;
			if reg_wr_i(2) = '1' then --L
				RegHL_s(7 downto 0) <= reg_HL_i(7 downto 0);
				RegsL(2) <= reg_HL_i(7 downto 0);
			end if;
			if reg_wr_i(1) = '1' then --D
				RegDE_s(15 downto 8) <= reg_DE_i(15 downto 8);
				RegsH(1) <= reg_DE_i(15 downto 8);
			end if;
			if reg_wr_i(0) = '1' then --E
				RegDE_s(7 downto 0) <= reg_DE_i(7 downto 0);
				RegsL(1) <= reg_DE_i(7 downto 0);
			end if;
			
		end if;
	end process;

	DOAH <= RegsH(to_integer(unsigned(AddrA)));
	DOAL <= RegsL(to_integer(unsigned(AddrA)));
	DOBH <= RegsH(to_integer(unsigned(AddrB)));
	DOBL <= RegsL(to_integer(unsigned(AddrB)));
	DOCH <= RegsH(to_integer(unsigned(AddrC)));
	DOCL <= RegsL(to_integer(unsigned(AddrC)));

	reg_HL_o <= RegHL_s;
	reg_DE_o <= RegDE_s;
	reg_BC_o <= RegBC_s;
end;
