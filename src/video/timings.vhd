--
-- TBBlue / ZX Spectrum Next project
-- Copyright (c) 2015 - Fabio Belavenuto & Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity timings is
	port (
		clk_a_i  : in  std_logic;
		we_i     : in  std_logic;
		addr_a_i : in  std_logic_vector(5 downto 0);
		data_a_i : in  std_logic_vector(9 downto 0);
		data_a_o : out std_logic_vector(9 downto 0);
		
		sel : in std_logic;
		out0: out std_logic_vector(9 downto 0);
		out1: out std_logic_vector(9 downto 0);
		out2: out std_logic_vector(9 downto 0);
		out3: out std_logic_vector(9 downto 0);
		out4: out std_logic_vector(9 downto 0);
		out5: out std_logic_vector(9 downto 0);
		out6: out std_logic_vector(9 downto 0);
		out7: out std_logic_vector(9 downto 0);
		out8: out std_logic_vector(9 downto 0);
		out9: out std_logic_vector(9 downto 0);
		out10: out std_logic_vector(9 downto 0);
  );

end dpram2;

architecture rtl of timings is

	type ram_t is array (0 to 35) of std_logic_vector(9 downto 0);
	signal ram_q : ram_t := (
	
	
		-- TV 50hz	
		"1011001111", -- 720 - 1;
		"1011011011", -- 732 - 1;
		"1100011011", -- 796 - 1;
		"1101011111", -- 864 - 1;
		--			
		"1000111111", -- 576 - 1;
		"1001000100", -- 581 - 1;
		"1001001001", -- 586 - 1;
		"1001101111", -- 625 - 2;
				
		-- TV 60hz
		"1011001111", -- 720 - 1;
		"1011011111", -- 736 - 1;
		"1100011101", -- 798 - 1;
		"1101011010", -- 858 - 1;
		--
		"0111011111", -- 480 - 1;
		"0111101000", -- 489 - 1;
		"0111101110", -- 495 - 1;
		"1000001011", -- 525 - 2;
		
		-- ULA 50Hz
		"0101000011", --323
		"0101001001", --329
		"0101101001", --361
		"0110001010", --394
		"0110101111", --431
		--
		"0100000000", --256
		"0100000000", --256
		"0100000011", --259
		"0100000111", --263
		"0100110111", --311

		-- ULA 60Hz
		"0101000011", --323
		"0101001011", --331
		"0101101010", --362
		"0110000111", --391
		"0110101100", --428
		--
		"0011100111", --231
		"0011101011", --235
		"0011101110", --238
		"0011101110", --238
		"0100000101"  --261
	);
	
	signal 	read_addr_a_q,
	read_addr_b_q  : unsigned(addr_width_g-1 downto 0);

begin

	mem_a: process (clk_a_i)
		variable read_addr_v		: unsigned(addr_width_g-1 downto 0);
	begin
		if rising_edge(clk_a_i) then
			read_addr_v := unsigned(addr_a_i);
			if we_i = '1' then
				ram_q(to_integer(read_addr_v)) <= data_a_i;
			end if;
			data_a_o <= ram_q(to_integer(read_addr_v));
		end if;
	end process mem_a;

	mem_b: process (clk_b_i)
		variable read_addr_v		: unsigned(addr_width_g-1 downto 0);
	begin
		if rising_edge(clk_b_i) then
			read_addr_v := unsigned(addr_b_i);
			data_b_o		<= ram_q(to_integer(read_addr_v));
		end if;
	end process mem_b;

end rtl;




