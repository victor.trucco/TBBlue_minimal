--
-- TBBlue / ZX Spectrum Next project
--
-- HDMI frame - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.ALL;

entity hdmi_frame is
	port
	(
		clock_i		: in std_logic;
		clock2X_i	: in std_logic;
		reset_i		: in std_logic;
		scanlines_i	: in std_logic_vector(1 downto 0);  
		
		rgb_i			: in std_logic_vector(8 downto 0);
		hsync_i		: in std_logic;
		vsync_i		: in std_logic;
		hblank_n_i	: in std_logic;
		vblank_n_i	: in std_logic;
		timing_i		: in std_logic_vector(2 downto 0);
		
		rgb_o			: out std_logic_vector(8 downto 0);
		hsync_o 		: out std_logic;
		vsync_o 		: out std_logic;
		
		blank_o 	: out std_logic;

		-- config values
		h_visible			: in integer := 720 - 1;
		hsync_start			: in integer := 732 - 1;
		hsync_end			: in integer := 796 - 1;
		hcnt_end				: in integer := 864 - 1;
		--
		v_visible			: in integer := 576 - 1;
		vsync_start			: in integer := 581 - 1;
		vsync_end			: in integer := 586 - 1;
		vcnt_end				: in integer := 625 - 2
	);
end entity;

architecture rtl of hdmi_frame is	
	
	signal input_addr_s	: std_logic_vector(10 downto 0) := (others=>'0');
	signal output_addr_s: std_logic_vector(10 downto 0) := (others=>'0');
	
	signal rgb_s		: std_logic_vector(8 downto 0);
	signal pixel_out		: std_logic_vector(8 downto 0);
	signal max_scanline		: std_logic_vector(9 downto 0):= (others=>'0');
	
	signal hsync_s		: std_logic := '1';
	signal vsync_s		: std_logic := '1';
	signal odd_line_s	: std_logic := '0';

	signal locked_s	: std_logic := '0';
	
	signal vs_counter_s	: std_logic_vector(15 downto 0):= (others=>'0');
	
	

	--ModeLine "720x480@60" 		27.00 	720 	736 	798 	858 		480 	489 	495 	525 
	-- Horizontal Timing constants  
--	constant h_visible	: integer := 720 - 1;
--	constant hsync_start			: integer := 736 - 1;
--	constant hsync_end			: integer := 798 - 1;
--	constant hcnt_end			: integer := 858 - 1;
--	-- Vertical Timing constants
--	constant v_visible		: integer := 480 - 1;
--	constant vsync_start			: integer := 489 - 1;
--	constant vsync_end			: integer := 495 - 1;
--	constant vcnt_end			: integer := 525 - 2;

	
	
--	----Modeline "720x576x50hz" 	27 	720  	732  	796  	864   	576  	581  	586  	625 
--	-- Horizontal Timing constants  
--	constant h_visible	: integer := 720 - 1;
--	constant hsync_start			: integer := 732 - 1;
--	constant hsync_end			: integer := 796 - 1;
--	constant hcnt_end			: integer := 864 - 1;
--	-- Vertical Timing constants
--	constant v_visible		: integer := 576 - 1;
--	constant vsync_start			: integer := 581 - 1;
--	constant vsync_end			: integer := 586 - 1;
--	constant vcnt_end			: integer := 625 - 2;

	
	--
	signal hcnt				: std_logic_vector(9 downto 0) := (others => '0');
	signal h					: std_logic_vector(9 downto 0) := (others => '0');
--	signal vcnt				: std_logic_vector(9 downto 0) := (others => '0');
--	signal vcnt				: std_logic_vector(9 downto 0) := "0000110010"; --50 (initial vertical adjust) 60hz
--	signal vcnt				: std_logic_vector(9 downto 0) := "0001011010"; --90 (initial vertical adjust)
--	signal vcnt				: std_logic_vector(9 downto 0) := "0001100100"; --100 (initial vertical adjust)
--	signal vcnt				: std_logic_vector(9 downto 0) := "0010010110"; --150 (initial vertical adjust)

	signal vcnt				: std_logic_vector(9 downto 0) := (others => '0');--std_logic_vector(to_unsigned(ver_adj,10));


	signal blank			: std_logic;
	signal picture			: std_logic;
	
	signal line_bank 		: std_logic := '0';
	


	signal timing_selected_s : std_logic_vector (2 downto 0) := "000";
	




begin


	
	




	
	--ModeLine "720x480@60" 		27.00 	720 	736 	798 	858 		480 	489 	495 	525 
	-- Horizontal Timing constants  
--	constant h_visible	: integer := 720 - 1;
--	constant hsync_start			: integer := 736 - 1;
--	constant hsync_end			: integer := 798 - 1;
--	constant hcnt_end			: integer := 858 - 1;
--	-- Vertical Timing constants
--	constant v_visible		: integer := 480 - 1;
--	constant vsync_start			: integer := 489 - 1;
--	constant vsync_end			: integer := 495 - 1;
--	constant vcnt_end			: integer := 525 - 2;

	
	
	----Modeline "720x576x50hz" 	27 	720  	732  	796  	864   	576  	581  	586  	625 
	-- Horizontal Timing constants  
--	constant h_visible	: integer := 720 - 1;
--	constant hsync_start			: integer := 732 - 1;
--	constant hsync_end			: integer := 796 - 1;
--	constant hcnt_end			: integer := 864 - 1;
--	-- Vertical Timing constants
--	constant v_visible		: integer := 576 - 1;
--	constant vsync_start			: integer := 581 - 1;
--	constant vsync_end			: integer := 586 - 1;
--	constant vcnt_end			: integer := 625 - 2;

		
	
	
	process (clock2X_i)
	variable egde_vb : std_logic_vector(1 downto 0);
	variable egde_hb : std_logic_vector(1 downto 0);
	variable egde_he : std_logic_vector(1 downto 0);
	begin
	
		if rising_edge(clock2X_i) then
		
			if reset_i = '1' then
				locked_s <= '0';
			end if;
			
			egde_hb := egde_hb(0) & hblank_n_i;		
			
			if hcnt = hcnt_end then
				hcnt <= (others => '0');
			else
				hcnt <= hcnt + 1;
			end if;
			
			if egde_hb = "01" then
					if (timing_i /= timing_selected_s) then
						timing_selected_s <= timing_i;
						--if hdmi_i = '1' and locked_s = '0' then
							hcnt <= (others => '0');
							locked_s <= '1';
						end if;
			end if;
			
			if hcnt = hsync_start then
				egde_vb := egde_vb(0) & vblank_n_i;	
			
				if egde_vb = "01"  then -- rising edge of vblank
				

				--if vcnt = vcnt_end then 
					vcnt <= (others => '0');
					odd_line_s <= '0';
				else
					vcnt <= vcnt + 1;
					odd_line_s <= not odd_line_s;
				end if;
			end if;
			
		end if;
		
	end process;

	scandoubler_ram : entity work.dpram2
	generic map (
		addr_width_g	=> 11,
		data_width_g	=> 9
	)
	port map (
		clk_a_i  	=> clock_i,
		we_i     	=> '1',
		addr_a_i 	=> input_addr_s,
		data_a_i 	=> rgb_i,
		data_a_o 	=> open,
		--
		clk_b_i  	=> clock2X_i,
		addr_b_i		=> output_addr_s,
		data_b_o 	=> rgb_s
	);
		
	process (clock_i, input_addr_s)
	variable egde_hb : std_logic_vector(1 downto 0);
	begin
	
		if rising_edge(clock_i) then
			
			egde_hb := egde_hb(0) & hblank_n_i;	
			
			if egde_hb = "01" then -- rising edge of hblank
				input_addr_s <= (not line_bank) & "0000000000";
				line_bank <= not line_bank;
			else
				input_addr_s <= input_addr_s + 1;
			end if;
				
		end if;
		
	end process;
	
	process (clock2X_i, output_addr_s)
	begin
	
		if rising_edge(clock2X_i) then
			output_addr_s <= ((not line_bank) & hcnt);-- + hcnt_adj;
		end if;
		
	end process;
	

   pixel_out <= 	"00" & rgb_s(8) & "00" & rgb_s(5) & "00" & rgb_s(2) when odd_line_s = '1' and scanlines_i = "01" else -- 75% 
						'0' & rgb_s(8 downto 7) & '0' & rgb_s(5 downto 4) & '0' & rgb_s(2 downto 1) when odd_line_s = '1' and scanlines_i = "10" else -- 50%
						
						std_logic_vector(unsigned('0' & rgb_s(8 downto 7)) + unsigned("00" & rgb_s(8 downto 8))) &
						std_logic_vector(unsigned('0' & rgb_s(5 downto 4)) + unsigned("00" & rgb_s(5 downto 5))) &
						std_logic_vector(unsigned('0' & rgb_s(2 downto 1)) + unsigned("00" & rgb_s(2 downto 2))) when odd_line_s = '1' and scanlines_i = "11"  -- 25%
						
						else rgb_s;
	
    
	blank		<= '1' when (hcnt > h_visible) or (vcnt > v_visible) else '0';
	--picture	<= '1' when (blank = '0') and (hcnt > h_start and hcnt < h_end) and (vcnt > v_start and vcnt < v_end) else '0';

	hsync_s	<= '1' when (hcnt <= hsync_start) or (hcnt > hsync_end) else '0';
	vsync_s	<= '1' when (vcnt <= vsync_start) or (vcnt > vsync_end) else '0';
	
	blank_o	<= blank;
	
	hsync_o <= hsync_s;
	vsync_o <= vsync_s;
	rgb_o	<= pixel_out when blank = '0' else "000000000";

end architecture;
     