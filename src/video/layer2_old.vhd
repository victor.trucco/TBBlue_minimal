--
-- TBBlue / ZX Spectrum Next project
--
-- Layer 2 - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity layer2_old is
	generic (
		DEBUG		: boolean := false
	);
	port (
		clock_i			: in  std_logic;
		reset_n_i		: in  std_logic;
			
		layer2_en_i	: in  std_logic := '1';

		layer2_X_i		: in unsigned(8 downto 0);
		layer2_Y_i		: in unsigned(8 downto 0);
		offset_x_i		: in std_logic_vector(7 downto 0);
		offset_y_i		: in std_logic_vector(7 downto 0);
		
		transp_colour_i: in std_logic_vector(7 downto 0);
		
		layer2_addr_o : out std_logic_vector(15 downto 0) := X"0000";
		layer2_data_i : in std_logic_vector(7 downto 0);
		
		layer2_R_o		: out std_logic_vector(2 downto 0);
		layer2_G_o		: out std_logic_vector(2 downto 0);
		layer2_B_o		: out std_logic_vector(1 downto 0);
		
		layer2_blank_o: out std_logic;
		layer2_cs_o	: out std_logic;
		
		-- clip window
		clip_x1_i: in unsigned(8 downto 0);
		clip_x2_i: in unsigned(8 downto 0);
		clip_y1_i: in unsigned(8 downto 0);
		clip_y2_i: in unsigned(8 downto 0)
		

	);
end entity;

architecture rtl of layer2_old is

	signal layer2_addr_s			: std_logic_vector(15 downto 0);
	signal layer2_R_s				: std_logic_vector(2 downto 0);
	signal layer2_G_s				: std_logic_vector(2 downto 0);
	signal layer2_B_s				: std_logic_vector(1 downto 0);
	signal layer2_blank_s			: std_logic;
	signal layer2_cs_s				: std_logic;
	signal layer2_delay_cs_s				: std_logic;
	signal layer2_X_s				: unsigned(8 downto 0);

begin

	layer2_X_s <= layer2_X_i - 12; --layer 2 position

	process (clock_i,reset_n_i, layer2_X_s, layer2_Y_i, offset_x_i, offset_y_i, layer2_cs_s)
		variable sum_x_v	: unsigned(9 downto 0);
		variable sum_y_v	: unsigned(9 downto 0);		

	begin
	
		--if reset_n_i = '0' or layer2_cs_s = '0' then
		
		--		layer2_addr_o <= X"0000";
				
	--	els
		--if falling_edge(clock_i) then --rising_edge wraps 1 px on left side
				
			sum_x_v := "0000000000" + layer2_X_s + unsigned(offset_x_i);
			sum_y_v := "0000000000" + layer2_Y_i + unsigned(offset_y_i);
			
			if sum_y_v > 191 then
				sum_y_v := sum_y_v - 192;
			end if;
			
			
			
				layer2_addr_o <= std_logic_vector(sum_y_v(7 downto 0)) & std_logic_vector(sum_x_v(7 downto 0));
				--layer2_addr_s <= "00000000" & std_logic_vector(sum_x_v(7 downto 0));
				--layer2_addr_s <= std_logic_vector(layer2_Y_i(7 downto 0)) & "00000000";


		--end if;
	end process;
	
	process (clock_i, layer2_X_s, layer2_Y_i, offset_x_i, offset_y_i, clip_x1_i, clip_x2_i, clip_y1_i, clip_y2_i)
	variable cs_v	: std_logic;
	begin
	
	--	if rising_edge(clock_i) then 
		
			cs_v := '0';	
				
			if (layer2_X_s >= clip_x1_i and layer2_X_s <= clip_x2_i and layer2_Y_i >= clip_y1_i and layer2_Y_i <= clip_y2_i) then								
					cs_v := '1';
			end if;
			
			layer2_cs_s <= cs_v;
			layer2_cs_o <= cs_v;
			
--			layer2_cs_o <= layer2_cs_s;
				
	--	end if;
	end process;

--layer2_cs_o <= layer2_cs_s and not clock_i;

	process (clock_i,layer2_X_s, layer2_Y_i, layer2_en_i, layer2_data_i, layer2_R_s, layer2_G_s, layer2_B_s)
		variable max_x	: unsigned(8 downto 0) := to_unsigned(511, 9);

		
	begin
	
		if falling_edge(clock_i) then --falling_edge moves layer 2 1px to the left
		

	
			if max_x < layer2_X_s then
				max_x := layer2_X_s;
			end if;

			layer2_R_s <= transp_colour_i(7 downto 5);
			layer2_G_s <= transp_colour_i(4 downto 2);
			layer2_B_s <= transp_colour_i(1 downto 0);
			
			if layer2_en_i = '1' then
		
					--to adjust the visible part
					if (layer2_X_s >= clip_x1_i+1 and layer2_X_s <= clip_x2_i+1 and layer2_Y_i >= clip_y1_i and layer2_Y_i <= clip_y2_i) then
						
							layer2_R_s <= layer2_data_i(7 downto 5);
							layer2_G_s <= layer2_data_i(4 downto 2);
							layer2_B_s <= layer2_data_i(1 downto 0);	
		
					end if;

					if ((layer2_X_s = max_x or layer2_X_s <= 256) and layer2_Y_i < 192) then
							layer2_blank_o <= '0';
					else
						--it's outside the screen
						layer2_blank_o <= '1';
						
					end if;

				
			end if;	

			-- external connections

			--layer2_addr_o <= layer2_addr_s;
			layer2_R_o <= layer2_R_s;
			layer2_G_o <= layer2_G_s;
			layer2_B_o <= layer2_B_s;
			
		end if;
	end process;

end architecture;