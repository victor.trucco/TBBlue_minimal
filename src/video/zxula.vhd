--
-- TBBlue / ZX Spectrum Next project
--
-- ULA - Fabio Belavenuto
--
-- Based on ula.v - Copyright (c) 2012 - Miguel Angel Rodriguez Jodar. rodriguj@atc.us.es
-- https://opencores.org/project,zx_ula
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--
	

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity zxula is
	port (
		--clock_28_i		: in  std_logic;
		clock_14_i			: in  std_logic;
		clock_7_i			: in  std_logic;
		reset_i			: in  std_logic;
		mode_i			: in  std_logic_vector(2 downto 0);
		video_timing_i			: in  std_logic_vector(2 downto 0);
		timex_en_i		: in  std_logic;
		vf50_60_i		: in  std_logic;
		iocs_i			: in  std_logic;
		cpu_addr_i		: in  std_logic_vector(15 downto 0);
		cpu_data_i		: in  std_logic_vector( 7 downto 0);
		cpu_data_o		: out std_logic_vector( 7 downto 0);
		has_data_o		: out std_logic;
		cpu_mreq_n_i	: in  std_logic;
		cpu_iorq_n_i	: in  std_logic;
		cpu_rd_n_i		: in  std_logic;
		cpu_wr_n_i		: in  std_logic;
		cpu_clock_cont_o		: out std_logic;
		cpu_int_n_o		: out std_logic;
		cpu_clock_i		: in std_logic;
		
		-- vram
		vram_shadow_i	: in  std_logic;
		ram_contention_i: in  std_logic;
		mem_addr_o		: out std_logic_vector(15 downto 0);
		mem_data_i		: in  std_logic_vector( 7 downto 0);
		mem_cs_o			: out std_logic;
		mem_oe_o			: out std_logic;
		s128_disable_i	: in  std_logic; -- 1 when paging is disabled
		
		-- I/O
		ear_i				: in  std_logic;
		speaker_o		: out std_logic;
		mic_o				: out std_logic;
		kb_columns_i	: in  std_logic_vector(4 downto 0);
		
		-- Line Interrupt
		lint_ctrl_i		: in  std_logic_vector(1 downto 0);
		lint_line_i		: in  std_logic_vector(8 downto 0);

		-- RGB
--		rgb_r_o			: out std_logic_vector(2 downto 0);
--		rgb_g_o			: out std_logic_vector(2 downto 0);
--		rgb_b_o			: out std_logic_vector(1 downto 0);
		rgb_hsync_o		: out std_logic;
		rgb_vsync_o		: out std_logic;
		rgb_hblank_o	: out std_logic;
		rgb_vblank_o	: out std_logic;
		
		-- Horizontal and vertical counters
		hcount_o			: out unsigned(8 downto 0);
		vcount_o			: out unsigned(8 downto 0);
		timex_hires_o 	: out std_logic;
	--	pixel_clock_o 	: out std_logic;
		
		-- Sprites counters
		spt_hcount_o	: out unsigned(8 downto 0);
		spt_vcount_o	: out unsigned(8 downto 0);
		
		-- Palette control
		disable_flash_i: in  std_logic := '0';
		ULAnext_value_i: in  std_logic_vector(7 downto 0);
		addr_ulanext_o	: out std_logic_vector(7 downto 0);
		
		-- clip window	
		clip_x1_i		: in unsigned(7 downto 0);
		clip_x2_i		: in unsigned(7 downto 0);
		clip_y1_i		: in unsigned(7 downto 0);
		clip_y2_i		: in unsigned(7 downto 0);
		cliped_o 		: out std_logic;
		--
		reset_conter_i : in std_logic
		
	);
end entity;


architecture behave of zxula is

	signal clock_turbo_s				: std_logic;
	signal clock_counter_s			: unsigned(1 downto 0)				:= (others => '0');

	signal border_color_s			: std_logic_vector(2 downto 0)	:= "100";
	signal clock_7_s					: std_logic := '0';
	signal hc_s 						: unsigned(8 downto 0);
	signal vc_s 						: unsigned(8 downto 0);
	signal hc_h_s 						: std_logic_vector(8 downto 0);
	signal vc_h_s 						: std_logic_vector(8 downto 0);
	signal flash_cnt_s				: unsigned(4 downto 0);
	signal hblank_n_s 				: std_logic;
	signal hsync_n_s 					: std_logic;
	signal vblank_n_s 				: std_logic;
	signal vsync_n_s 					: std_logic;
	signal vint_n_s					: std_logic;
	signal border_n_s					: std_logic;
	signal viden_n_s					: std_logic;
	signal datalatch_n_s				: std_logic;
	signal attrlatch_n_s				: std_logic;
	signal sload_s						: std_logic;
	signal aolatch_n_s				: std_logic;
	signal bitmap_r					: std_logic_vector(7 downto 0);
	signal shift_r						: std_logic_vector(7 downto 0);
	signal attr_r						: std_logic_vector(7 downto 0);
	signal attr_o_r					: std_logic_vector(7 downto 0);
	signal pixel_s						: std_logic;
	signal ula_cs_n_s					: std_logic;
	signal cpu_clk_s					: std_logic;
	signal ioreqtw3_s					: std_logic;
	signal mreqt23_s					: std_logic;
	signal wait_s						: std_logic;
	signal cdet1_s						: std_logic;
	signal cdet2_s						: std_logic;
	signal clk_cont_s					: std_logic;
	signal port255_en_s				: std_logic;
	signal port255_dis_en_s			: std_logic;
	signal ear_s						: std_logic;
	signal speaker_s					: std_logic;
	signal floatingbus_s				: std_logic_vector( 7 downto 0);
	signal floatingbus_3A_s			: std_logic_vector( 7 downto 0);
	
	-- palette control
	signal rgb_r_s						: std_logic;
	signal rgb_g_s						: std_logic;
	signal rgb_b_s						: std_logic;
	signal rgb_i_s						: std_logic;
	signal rgb_f_s						: std_logic;

	signal palette_rgb_s				: std_logic_vector(7 downto 0);
	
	-- Timex
	signal timex_reg_r				: std_logic_vector(7 downto 0);
	alias timex_hrink_a				: std_logic_vector(2 downto 0) is timex_reg_r(5 downto 3);
	alias timex_hires_a				: std_logic is timex_reg_r(2);
	alias timex_hicolor_mode_s		: std_logic is timex_reg_r(1);
	alias timex_page_a				: std_logic is timex_reg_r(0);
		
	signal shift_hr_r					: std_logic_vector(15 downto 0);
	
	signal pentagon_mode				: std_logic		:= '0';
	signal border_pentagond1_s		: std_logic_vector(2 downto 0)	:= "100";
	signal border_pentagond2_s		: std_logic_vector(2 downto 0)	:= "100";
	signal border_pentagond3_s		: std_logic_vector(2 downto 0)	:= "100";

begin


  pentagon_mode <= '1' when mode_i = "100" else '0';
  
  -- 0 to 127 are inks
  -- 128 to 255 are papers
  -- except when in full ink colour mode (0-255 are inks)

	process (clock_14_i)
	variable paper_initial_index_s  : unsigned (7 downto 0); 
	begin
		if rising_edge(clock_14_i) then
			if disable_flash_i = '0' then -- standard ULA: 0-15 ink 16-31 paper
				addr_ulanext_o <= "000" & not(pixel_s) & rgb_i_s & rgb_g_s & rgb_r_s & rgb_b_s;
			else
				
				paper_initial_index_s := "10000000"; --128
				
				if pixel_s = '1' then --ink
					addr_ulanext_o <= (attr_o_r and ULAnext_value_i); --get only the ink part
					
				else -- paper

					case ULAnext_value_i is

						when "00000001" =>
							addr_ulanext_o <= std_logic_vector(paper_initial_index_s + unsigned(attr_o_r(7 downto 1))); 

						when "00000011" =>
							addr_ulanext_o <= std_logic_vector(paper_initial_index_s + unsigned(attr_o_r(7 downto 2))); 

						when "00000111" =>
							addr_ulanext_o <= std_logic_vector(paper_initial_index_s + unsigned(attr_o_r(7 downto 3))); 

						when "00001111" =>
							addr_ulanext_o <= std_logic_vector(paper_initial_index_s + unsigned(attr_o_r(7 downto 4))); 
					
						when "00011111" =>
							addr_ulanext_o <= std_logic_vector(paper_initial_index_s + unsigned(attr_o_r(7 downto 5))); 
					
						when "00111111" =>
							addr_ulanext_o <= std_logic_vector(paper_initial_index_s + unsigned(attr_o_r(7 downto 6))); 

						when "01111111" =>
							if attr_o_r(7) = '1' then
								addr_ulanext_o <= std_logic_vector(paper_initial_index_s + 1); 
							else
								addr_ulanext_o <= std_logic_vector(paper_initial_index_s); 
							end if;

						when "11111111" =>
							addr_ulanext_o <= std_logic_vector(paper_initial_index_s); 

						when others =>  
							addr_ulanext_o <= std_logic_vector(paper_initial_index_s + unsigned(attr_o_r(7 downto 4)));  -- default (4/4)
					end case;
				
					
				end if;
			end if;
		end if;
	end process;

	--rgb_r_o <= palette_rgb_s(7 downto 5);
	--rgb_g_o <= palette_rgb_s(4 downto 2);
	--rgb_b_o <= palette_rgb_s(1 downto 0);

	-- Timing generator
	timing: entity work.zxula_timing
	port map (
		clock_i			=> clock_7_i,
		mode_i			=> mode_i,
		video_timing_i	=> video_timing_i,
		vf50_60_i		=> vf50_60_i,
		hcount_o			=> hc_s,
		vcount_o			=> vc_s,
		hsync_n_o		=> hsync_n_s,
		vsync_n_o		=> vsync_n_s,
		hblank_n_o		=> hblank_n_s,
		vblank_n_o		=> vblank_n_s,
		int_n_o			=> vint_n_s,
		lint_ctrl_i		=> lint_ctrl_i,
		lint_line_i		=> lint_line_i,
		-- Sprites
		spt_hcount_o	=> spt_hcount_o,
		spt_vcount_o	=> spt_vcount_o,
		reset_conter_i => reset_conter_i
	);

	--external counters to overlay
	hcount_o <= hc_s;
	vcount_o <= vc_s;

	-- Pixel clock
	process (clock_14_i)
	begin
		if rising_edge(clock_14_i) then
			clock_7_s <= not clock_7_s;
		end if;
	end process;

--	pixel_clock_o <= clock_7_i;

	--pixel_clock_o <= clock_14_i when timex_hires_a = '1' else clock_7_i;

	timex_hires_o <=  timex_hires_a; 

	-- Border control signal (=0 when we're not displaying paper/ink pixels)
	process (clock_7_i)
	begin
		if falling_edge(clock_7_i) then
			if ((vc_s(7) and vc_s(6)) or vc_s(8) or hc_s(8)) = '1' then
				border_n_s <= '0';
			else
				border_n_s <= '1';
			end if;
		end if;
	end process;

	-- VidEN generation (delaying Border 8 clocks)
	process (clock_7_i)
	begin
		if falling_edge(clock_7_i) then
			if hc_s(3) = '1' then
				viden_n_s <= not border_n_s;
			end if;
		end if;
	end process;
	
	process (clock_7_i)
		variable x1_v	: unsigned(8 downto 0);
		variable x2_v	: unsigned(8 downto 0);	
	begin
		if rising_edge(clock_7_i) then
			
			x1_v := ('0' & clip_x1_i) + 12;
			x2_v := ('0' & clip_x2_i) + 13; --14
						
			if (hc_s > x1_v and hc_s <= x2_v and vc_s >= clip_y1_i and vc_s <= clip_y2_i) then	
				cliped_o <= '0';
			elsif hc_s <= 12 or hc_s >= 256 + 13 or vc_s > 191 then --never clip the border
				cliped_o <= '0';
			else
				cliped_o <= '1';
			end if;
			
		end if;
	end process;

	-- DataLatch generation (negedge to capture data from memory)
	process (clock_7_i)
	begin
		if falling_edge(clock_7_i) then
			if border_n_s = '1' and hc_s(3) = '1' and hc_s(1 downto 0) = "11" then
				datalatch_n_s <= '0';
			else
				datalatch_n_s <= '1';
			end if;
		end if;
	end process;

	-- AttrLatch generation (negedge to capture data from memory)
	process (clock_7_i)
	begin
		if falling_edge(clock_7_i) then
			if border_n_s = '1' and hc_s(3) = '1' and hc_s(1 downto 0) = "01" then
				attrlatch_n_s <= '0';
			else
				attrlatch_n_s <= '1';
			end if;
		end if;
	end process;

	-- SLoad generation (negedge to load shift register)
	process (clock_7_i)
	begin
		if falling_edge(clock_7_i) then
			if viden_n_s = '0' and hc_s(2 downto 0) = "100" then
				sload_s <= '1';
			else
				sload_s <= '0';
			end if;
		end if;
	end process;

	-- AOLatch generation (negedge to update attr output latch)
	process (clock_7_i)
	begin
		if falling_edge(clock_7_i) then
			if hc_s(2 downto 0) = "101" then
				aolatch_n_s <= '0';
			else
				aolatch_n_s <= '1';
			end if;
		end if;
	end process;

	-- First buffer for bitmap
	process (datalatch_n_s)
	begin
		if falling_edge(datalatch_n_s) then
			bitmap_r <= mem_data_i;
		end if;
	end process;

	-- Shift register (second bitmap register)
	process (clock_7_i)
	begin
		if falling_edge(clock_7_i) then
			if sload_s = '1' then
				shift_r	<= bitmap_r;
			else
				shift_r	<= shift_r(6 downto 0) & '0';
			end if;
		end if;
	end process;
	
	-- shift_hr_r register
	process (clock_14_i)
	begin
		if falling_edge(clock_14_i) then
			if sload_s = '1' and clock_7_s = '1' then 
				shift_hr_r <= bitmap_r & attr_r; 
			else
				shift_hr_r <= shift_hr_r(14 downto 0) & '0';
			end if;
		end if;
	end process;


	-- First buffer for attribute
	process (attrlatch_n_s)
	begin
		if falling_edge(attrlatch_n_s) then
			attr_r <= mem_data_i;
		end if;
	end process;
	
	-- Border for Pentagon mode
	process (clock_7_i)
	begin
		if falling_edge(clock_7_i) then
			border_pentagond3_s <= border_pentagond2_s;
			border_pentagond2_s <= border_pentagond1_s;
			border_pentagond1_s <= border_color_s;
		end if;
	end process;

	
	-- Second buffer for attribute
	process (aolatch_n_s)
	begin
		if falling_edge(aolatch_n_s) then
		
			if timex_hires_a = '1' then
				attr_o_r	<="01" & (not timex_hrink_a) & timex_hrink_a;		
				
			elsif viden_n_s = '0' then
				attr_o_r	<= attr_r;
				
			else
				if disable_flash_i = '0' then -- standard ULA
				
				--   if pentagon_mode = '1' then
					--	attr_o_r	<= "00" & border_pentagond3_s & border_pentagond_s;
					--else
						attr_o_r	<= "00" & border_color_s & border_color_s;
				--	end if;
					
					
				else -- flash is disabled, the border colour are papers 0 to 7
				
				
						attr_o_r	<= "00000" & border_color_s;
				
				
				--	case ULAnext_value_i is

				--			when "00000001" =>
				--				attr_o_r	<= "0000" & border_color_s & '0'; 

				--			when "00000011" =>
				--				attr_o_r	<= "000" & border_color_s & "00"; 

				--			when "00000111" =>
				--				attr_o_r	<= "00" & border_color_s & "000"; 

				--			when "00001111" =>
				--				attr_o_r	<= "0" & border_color_s & "0000"; 
						
				--			when "00011111" =>
				--				attr_o_r	<= border_color_s & "00000"; 
						
				--			when "00111111" =>
				--				attr_o_r	<= border_color_s(1 downto 0) & "000000"; 

				--			when "01111111" | "11111111" =>
				--				attr_o_r	<= border_color_s(0) & "0000000"; 

				--			when others =>  
				--				attr_o_r	<= "0" & border_color_s & "0000";   -- default (4/4)
				--	end case;
					end if;
			
			end if;
		end if;
	end process;
	
   --Flash counter
 	process (clock_7_i)
 	begin
 		if rising_edge(clock_7_i) then
 			if hc_s = 0 and vc_s = 0 then
 				flash_cnt_s	<= flash_cnt_s + 1;
 			end if;
 		end if;
 	end process;

	-- Flash counter
--	process (vsync_n_s)
--	begin
--		if falling_edge(vsync_n_s) then
--			flash_cnt_s	<= flash_cnt_s + 1;
--		end if;
--	end process;

	-- Pixel generation
	pixel_s	<= shift_hr_r(15) when timex_hires_a = '1' else
					shift_r(7) xor (attr_o_r(7) and flash_cnt_s(4)) when disable_flash_i = '0' else 
					shift_r(7);

	-- RGB generation
	process (hblank_n_s, vblank_n_s, pixel_s, attr_o_r)
	begin
		--if hblank_n_s = '1' and vblank_n_s = '1' then
			rgb_i_s	<= attr_o_r(6);
			rgb_f_s	<= attr_o_r(7);
			if pixel_s = '1' then --ink
				rgb_g_s	<= attr_o_r(2);
				rgb_r_s	<= attr_o_r(1);
				rgb_b_s	<= attr_o_r(0);
			else -- paper
				rgb_g_s	<= attr_o_r(5);
				rgb_r_s	<= attr_o_r(4);
				rgb_b_s	<= attr_o_r(3);
			end if;
		--else
		--	rgb_f_s	<= '0';
		--	rgb_i_s	<= '0';
		--	rgb_r_s	<= '0';
		--	rgb_g_s	<= '0';
		--	rgb_b_s	<= '0';
		--end if;
	end process;

	-- VRAM address and control line generation

	-- Latches to hold delayed versions of V and H counters
	process (clock_7_i)
	begin
		if falling_edge(clock_7_i) then
			if border_n_s = '1' and (hc_s(3 downto 0) = X"7" or hc_s(3 downto 0) = X"B") then	-- cycles 7 and 11: load V and C from VC and HC
				hc_h_s <= std_logic_vector(hc_s);
				vc_h_s <= std_logic_vector(vc_s);
			end if;
		end if;
	end process;

	-- Address and control line multiplexor ULA/CPU
	process (cpu_mreq_n_i, cpu_addr_i, cpu_rd_n_i, cpu_wr_n_i, border_n_s, hc_s, vc_s, vc_h_s, hc_h_s, vram_shadow_i, timex_hicolor_mode_s)
	begin
		if border_n_s = '1' and (hc_s(3 downto 0) = X"8" or hc_s(3 downto 0) = X"9" or hc_s(3 downto 0) = X"C" or hc_s(3 downto 0) = X"D") then	-- cycles 8 and 12: present attribute address to VRAM
			--if timex_hicolor_mode_s = '1' then																																		-- (cycles 9 and 13 load attr byte)
			--	mem_addr_o	<= vram_shadow_i & "11" & vc_h_s(7 downto 6) & vc_h_s(2 downto 0) & vc_h_s(5 downto 3) & hc_h_s(7 downto 3);
			--else
			--	mem_addr_o	<= vram_shadow_i & "10110" & vc_h_s(7 downto 3) & hc_h_s(7 downto 3);
			--end if;
			
			if timex_hicolor_mode_s = '1' then																																		-- (cycles 9 and 13 load attr byte)
					mem_addr_o	<= vram_shadow_i & "11" & vc_h_s(7 downto 6) & vc_h_s(2 downto 0) & vc_h_s(5 downto 3) & hc_h_s(7 downto 3); -- 0x6000 (shadow 0xe000)
			else
					mem_addr_o	<= vram_shadow_i & '1' & timex_page_a & "110" & vc_h_s(7 downto 3) & hc_h_s(7 downto 3); --0x5800 / 0x7800 (shadow D800 / F800)
			end if;	
			
			mem_cs_o		<= '1';
			mem_oe_o		<= not hc_s(0);
			
		elsif border_n_s = '1' and (hc_s(3 downto 0) = X"A" or hc_s(3 downto 0) = X"B" or hc_s(3 downto 0) = X"E" or hc_s(3 downto 0) = X"F") then	-- cycles 10 and 14: present display address to VRAM 
			--mem_addr_o	<= vram_shadow_i & "10" & vc_h_s(7 downto 6) & vc_h_s(2 downto 0) & vc_h_s(5 downto 3) & hc_h_s(7 downto 3);						-- (cycles 11 and 15 load display byte)
			
	
			mem_addr_o <= vram_shadow_i & '1' & timex_page_a & vc_h_s(7 downto 6) & vc_h_s(2 downto 0) & vc_h_s(5 downto 3) & hc_h_s(7 downto 3); -- 0x4000 / -x6000
			
			mem_cs_o		<= '1';
			mem_oe_o		<= not hc_s(0);
			
		else
			mem_addr_o	<= (others => '0');
			mem_cs_o		<= '0';
			mem_oe_o		<= '0';
			
		end if;
	end process;

	rgb_hsync_o		<= hsync_n_s;
	rgb_vsync_o		<= vsync_n_s;
	rgb_hblank_o	<= hblank_n_s;
	rgb_vblank_o	<= vblank_n_s;
	cpu_int_n_o		<= vint_n_s;

	port255_en_s		<= '1' when cpu_iorq_n_i = '0' and cpu_addr_i(7 downto 0) = X"FF" and timex_en_i = '1'	else '0';		
	port255_dis_en_s	<= '1' when cpu_iorq_n_i = '0' and cpu_addr_i(7 downto 0) = X"FF" and timex_en_i = '0'	else '0';		

	-- 
	process (reset_i, cpu_clock_i)
	begin
		if reset_i = '1' then
			timex_reg_r				<= (others=>'0');
			speaker_s				<= '0';
			mic_o						<= '0';
			
		elsif rising_edge(cpu_clock_i) then
			if    port255_en_s = '1' and cpu_wr_n_i = '0' then			-- port 255
				timex_reg_r <= cpu_data_i;
				
			elsif iocs_i = '1' and cpu_wr_n_i = '0' then					-- port 254
				speaker_s		<= cpu_data_i(4);
				mic_o				<= cpu_data_i(3);
				border_color_s	<= cpu_data_i(2 downto 0);
				
			end if;
		end if;
	end process;

	ear_s <= ear_i xor speaker_s;	-- Issue 3
	speaker_o <= speaker_s;

	-- Simulates floating-bus on I/O port 0xFF
	floatingbus_s <= mem_data_i when datalatch_n_s = '0' or attrlatch_n_s = '0'	else (others => '1');
	
	--process (reset_i, clock_7_i)
	--begin
		--if rising_edge(clock_7_i) then
		
		--	if datalatch_n_s = '0' or attrlatch_n_s = '0' then
		--		floatingbus_s <= mem_data_i;
		--	else
		--		floatingbus_s <= (others => '1');
		--	end if;
	
		--end if;
	--end process;
	
	-- for the +3, During idling intervals (i.e. when the ULA is drawing the border or in between fetching the bitmap/attribute byte), 
	-- the bus latches onto the last value that was written to, or read from, contended memory, and not strictly 0xFF.
	process (reset_i, clock_7_i)
	begin
		
			if datalatch_n_s = '0' or attrlatch_n_s = '0' then
				floatingbus_3A_s <= mem_data_i;
			end if;
	
	end process;
	
	
	
	

	-- ULA-CPU interface
--	cpu_data_o <=
--					'1' & ear_s & '1' & kb_columns_i			when iocs_i = '1' and cpu_rd_n_i = '0'									else	-- keyboard and EAR state
--					timex_reg_r										when port255_en_s = '1' and cpu_rd_n_i = '0'							else	-- Timex hicolor config port. 
--					floatingbus_s									when port255_dis_en_s = '1' and cpu_rd_n_i = '0'					else	-- Floating Bus in FF port
--					(others => '1');

--	has_data_o	<= '1' when iocs_i = '1'         		  and cpu_rd_n_i = '0'	else
--						'1' when port255_en_s = '1'           and cpu_rd_n_i = '0'	else
--						'1' when port255_dis_en_s = '1'       and cpu_rd_n_i = '0'	else
--						'0';

	-- ULA-CPU interface			
	process (reset_i, cpu_clock_i)
	begin
	-- doesnt work without an egde or falling_edge
	-- at moment works for FF on  rising_edge(cpu_clock_i)
	
		if rising_edge(cpu_clock_i) then
			
			if iocs_i = '1' and cpu_rd_n_i = '0' then		-- keyboard and EAR state	
					cpu_data_o <= '1' & ear_s & '1' & kb_columns_i;	
					has_data_o <= '1';		

					
			elsif port255_en_s = '1' and cpu_rd_n_i = '0'	then -- Timex hicolor config port. 
					cpu_data_o <= timex_reg_r;				
					has_data_o <= '1';	

					
			elsif	port255_dis_en_s = '1' and cpu_rd_n_i = '0' then -- Floating Bus in FF port
					cpu_data_o <= floatingbus_s;
					has_data_o <= '1';	


			elsif	cpu_iorq_n_i = '0' and cpu_addr_i(15 downto 12) = "0000"  and cpu_addr_i(1 downto 0) = "01" and cpu_rd_n_i = '0' and mode_i = "011" and s128_disable_i = '0' then -- Floating Bus on +3 port 0000XXXXXXXXXX01
					cpu_data_o <= floatingbus_3A_s(7 downto 1) & '1';
					has_data_o <= '1';	

					
			else	
					cpu_data_o <= (others => '1');		
					has_data_o <= '0';

			end if;
			
		end if;
	end process;
	
						
	-- CPU contention
	ula_cs_n_s	<= not iocs_i;

	-- =0 quando estivermos exibindo tela e HC[3:2] /= 00
	wait_s	<= (not ((hc_s(3) or hc_s(2)))) or (not border_n_s);

	-- =0 quando a[15:14] == 01 ou (a[15:14] == 11 e ram_page == 1) ou ioreq_n == 0
	--cdet1_s	<= (cpu_addr_i(15) or not cpu_addr_i(14)) and (not cpu_addr_i(15) or not cpu_addr_i(14) or not ram_contention_i) and ula_cs_n_s;
	cdet1_s	<= (not ram_contention_i) and ula_cs_n_s;
	
	-- =0 quando ioreqtw3(ioreq atrasado) e mreqt23(mreq atrasado) forem 1
	cdet2_s	<= not (ioreqtw3_s and mreqt23_s);

	-- =1 (contencao) quando todos os sinais forem 0
	--clk_cont_s <= not (wait_s or cdet1_s or cdet2_s);
	clk_cont_s <= '0' when pentagon_mode = '1' else
					  '1' when (wait_s = '0' and cdet1_s = '0' and cdet2_s = '0') else 
					  '0';

	-- Delay IOREQ and MREQ
	process (cpu_clock_i)
	begin
		if rising_edge(cpu_clock_i) then
			ioreqtw3_s	<= ula_cs_n_s;
			mreqt23_s	<= cpu_mreq_n_i;
		end if;
	end process;

	cpu_clock_cont_o 	<= clk_cont_s;
	
--	clock_turbo_s	<= clock_7_i	when turbo_i = "00"		else --for 3.5
--							clock_14_i	when turbo_i = "01"		else --for 7
--							clock_28_i; -- for 14
--
--	process (clock_turbo_s)
--	begin
--		if rising_edge(clock_turbo_s) then
--			if cpu_clk_s = '1' and clk_cont_s = '0' then		-- if there's no contention, the clock can go low
--				cpu_clk_s <= '0';
--			else
--				cpu_clk_s <= '1';
--			end if;
--		end if;
--	end process;
--
--	cpu_clock_o 	<= cpu_clk_s;



end architecture;