--
-- TBBlue / ZX Spectrum Next project
--
-- LoRes - Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--

-- LoRes video mode
-- 128 x 96
-- $4000-$57FF for top half
-- $6000-$77FF for bottom half


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity lores is
	generic (
		DEBUG		: boolean := false
	);
	port (
		clock_i				: in  std_logic;
		reset_n_i			: in  std_logic;
			
		LoRes_en_i		: in  std_logic := '1';

		LoRes_X_i		: in unsigned(8 downto 0);
		LoRes_Y_i		: in unsigned(8 downto 0);
		offset_x_i			: in std_logic_vector(7 downto 0);
		offset_y_i			: in std_logic_vector(7 downto 0);

		transp_colour_i	: in std_logic_vector(7 downto 0);
		
		LoRes_addr_o 	: out std_logic_vector(15 downto 0) := X"0000";
		LoRes_data_i 	: in std_logic_vector(7 downto 0);
		
		LoRes_R_o		: out std_logic_vector(2 downto 0);
		LoRes_G_o		: out std_logic_vector(2 downto 0);
		LoRes_B_o		: out std_logic_vector(1 downto 0);
		
		LoRes_blank_o	: out std_logic;
		
		-- clip window
		clip_x1_i: in unsigned(7 downto 0);
		clip_x2_i: in unsigned(7 downto 0);
		clip_y1_i: in unsigned(7 downto 0);
		clip_y2_i: in unsigned(7 downto 0)

	);
end entity;

architecture rtl of lores is

	signal LoRes_addr_s			: std_logic_vector(15 downto 0);
	signal LoRes_X_s				: unsigned(8 downto 0); 
	signal LoRes_Y_s				: unsigned(8 downto 0); 
	signal LoRes_R_s				: std_logic_vector(2 downto 0);
	signal LoRes_G_s				: std_logic_vector(2 downto 0);
	signal LoRes_B_s				: std_logic_vector(1 downto 0);
	signal LoRes_blank_s			: std_logic;

begin

	LoRes_X_s <= LoRes_X_i - 12; 

	process (clock_i,reset_n_i, LoRes_X_s, LoRes_Y_i, offset_x_i, offset_y_i)
		variable sum_x_v	: unsigned(8 downto 0);
		variable sum_y_v	: unsigned(8 downto 0);		
	begin
	
		if rising_edge(clock_i) then
		
			sum_x_v := LoRes_X_s + unsigned(offset_x_i)+2;
			sum_y_v := LoRes_Y_i + unsigned(offset_y_i);
			
			if sum_y_v > 191 then
				sum_y_v := sum_y_v - 192;
			end if;
			
			if reset_n_i = '0' then
				LoRes_addr_s <= X"4000";
			else
				
				if sum_y_v < 96 then
					LoRes_addr_s <= "01" & std_logic_vector(sum_y_v(7 downto 1)) & std_logic_vector(sum_x_v(7 downto 1));
				else
					LoRes_addr_s <= std_logic_vector( unsigned("01" & std_logic_vector(sum_y_v(7 downto 1)) & std_logic_vector(sum_x_v(7 downto 1)) ) + "0000100000000000");
				end if;
				
			end if;

		end if;
	end process;


	process (clock_i,LoRes_X_s, LoRes_Y_i, LoRes_en_i, LoRes_data_i, LoRes_R_s, LoRes_G_s, LoRes_B_s)
		variable x1_v	: unsigned(8 downto 0);
		variable x2_v	: unsigned(8 downto 0);		
	begin
	
		if rising_edge(clock_i) then
	
			
			LoRes_R_s <= transp_colour_i(7 downto 5);
			LoRes_G_s <= transp_colour_i(4 downto 2);
			LoRes_B_s <= transp_colour_i(1 downto 0);
			
			if LoRes_en_i = '1' then
		
					if (LoRes_X_s >= 0 and LoRes_X_s <= 256 and LoRes_Y_i < 192) then
						
							LoRes_R_s <= LoRes_data_i(7 downto 5);
							LoRes_G_s <= LoRes_data_i(4 downto 2);
							LoRes_B_s <= LoRes_data_i(1 downto 0);	
		
					end if;
					
					x1_v := ('0' & clip_x1_i) + 1;
					x2_v := ('0' & clip_x2_i) + 1;
						
					
					--if ((LoRes_X_s >=1 and LoRes_X_s <= 256) and LoRes_Y_i < 192) then
					if (LoRes_X_s >= x1_v and LoRes_X_s <= x2_v and LoRes_Y_i >= clip_y1_i and LoRes_Y_i <= clip_y2_i) then								
						LoRes_blank_o <= '0';
						
					else
						--it's outside the video area
						LoRes_blank_o <= '1';
						
					end if;

				
			end if;	

			-- external connections
			LoRes_addr_o <= LoRes_addr_s;
			LoRes_R_o <= LoRes_R_s;
			LoRes_G_o <= LoRes_G_s;
			LoRes_B_o <= LoRes_B_s;
			
		end if;
	end process;

end architecture;