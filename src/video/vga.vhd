--
-- TBBlue / ZX Spectrum Next project
--
-- Copyright (c) 2015 - Fabio Belavenuto & Victor Trucco
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.std_logic_unsigned.ALL;

entity vga is
	port
	(
		clock_i		: in std_logic;
		clock2X_i	: in std_logic;
		scanlines_i	: in std_logic;  
		
		rgb_i			: in std_logic_vector(8 downto 0);
		hsync_i		: in std_logic;
		vsync_i		: in std_logic;
		
		rgb_o			: out std_logic_vector(8 downto 0);
		hsync_o 		: out std_logic;
		vsync_o 		: out std_logic
	);
end entity;

architecture rtl of vga is	

	constant HSYNC_END : integer := 85; --90; --94;
	constant VSYNC_END : integer := 2800; --3086; --3200;
	
	signal input_addr_s	: std_logic_vector(10 downto 0) := (others=>'0');
	signal output_addr_s: std_logic_vector(10 downto 0) := (others=>'0');
	
	signal rgb_s		: std_logic_vector(8 downto 0);
	signal pixel_s		: std_logic_vector(8 downto 0);
	signal max_scanline		: std_logic_vector(9 downto 0):= (others=>'0');

	signal hsync_s		: std_logic := '1';
	signal vsync_s		: std_logic := '1';
	signal odd_line_s	: std_logic := '0';
	
	signal vs_counter_s	: std_logic_vector(15 downto 0):= (others=>'0');
	
begin	

	scandoubler_ram : entity work.dpram2
	generic map (
		addr_width_g	=> 11,
		data_width_g	=> 9
	)
	port map (
		clk_a_i  	=> clock_i,
		we_i     	=> '1',
		addr_a_i 	=> input_addr_s,
		data_a_i 	=> rgb_i,
		data_a_o 	=> open,
		--
		clk_b_i  	=> clock2X_i,
		addr_b_i	=> output_addr_s,
		data_b_o 	=> rgb_s
	);
		
	process (clock_i, input_addr_s, max_scanline)
	variable egde_hs : std_logic_vector(1 downto 0);
	begin
	
		if rising_edge(clock_i) then
		
			egde_hs := egde_hs(0) & hsync_i;

		--	if (hsync_i = '0' and input_addr_s(9 downto 7) /= "000") then
			if egde_hs = "10" then -- falling edge of HS
				max_scanline <= input_addr_s (9 downto 0);
				input_addr_s <= (not input_addr_s(10)) & "0000000000";
			else
				input_addr_s <= input_addr_s + 1;
			end if;
			
		end if;
	end process;
	
	process (clock2X_i, output_addr_s, max_scanline)
	begin
	
		if rising_edge(clock2X_i) then
  
			if (output_addr_s(9 downto 0) = max_scanline and hsync_i = '1') then
				output_addr_s(9 downto 0) <= (others=>'0');

			elsif (hsync_i = '0' and output_addr_s(9 downto 7) /= "000") then
				output_addr_s <= (not output_addr_s(10)) & "0000000000";

			else
				output_addr_s <= output_addr_s + 1;
			end if;
			
		end if;
		
	end process;
	
	
	process (clock2X_i, vsync_i)
	begin
	
		if rising_edge(clock2X_i) then
		
			if (vsync_i = '0') then
				if (vs_counter_s > VSYNC_END) then
					vs_counter_s <= (others=>'0');
					vsync_s <= '0';
					
				elsif (vs_counter_s < VSYNC_END) then
					vs_counter_s <= vs_counter_s + 1;
				else
					vsync_s <= '1';

				end if;
			elsif (vsync_i = '1') then
				vs_counter_s <= vs_counter_s + 1;
			end if;
		end if;
	end process;
	
	hsync_s <= '0' when output_addr_s(9 downto 0) < HSYNC_END else '1';

	process(hsync_s,vsync_s)
	begin
		if falling_edge(hsync_s) then
			odd_line_s <= not odd_line_s and vsync_s;
		end if;
	end process;

   pixel_s <= '0' & rgb_s(7 downto 6) & '0' & rgb_s(4 downto 3) & '0' & rgb_s(1 downto 0) when odd_line_s = '1' and scanlines_i = '1' else rgb_s;
	hsync_o <= hsync_s;
	vsync_o <= vsync_s;
	
	rgb_o <= pixel_s when hsync_s = '1' and vsync_s = '1' else (others=>'0');
    

end architecture;
     