--
-- TBBlue / ZX Spectrum Next project
--
-- DMA - Victor Trucco 
--
-- Special thanks to Allen Albright for the bug hunting
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--

-- ATTENTION: Loosely based on Zilog Z80C10. There are differences!

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.all;
 
entity z80dma is
	port 
	( 
		reset_i			: in std_logic;
		mode_i			: in std_logic := '0'; -- 0 = port 11(#0B) MB02 compatible. 1 = port 107(#6B) 
		clk_i				: in std_logic;
		cpu_a_i			: in std_logic_vector(7 downto 0);
		cpu_d_i			: in std_logic_vector(7 downto 0);
		cpu_rd_n_i		: in std_logic;
		cpu_wr_n_i		: in std_logic;
		cpu_mreq_n_i	: in std_logic;
		cpu_ioreq_n_i	: in std_logic;
		cpu_m1_i			: in std_logic;
		wait_n_i			: in std_logic	:= '1';
		
		cpu_busack_n_i	: in std_logic;
		cpu_busreq_n_o	: out std_logic;

		
		dma_a_o			: out std_logic_vector(15 downto 0);
		dma_d_o			: out std_logic_vector(7 downto 0);
		dma_d_i			: in std_logic_vector(7 downto 0);
		dma_rd_n_o		: out std_logic;
		dma_wr_n_o		: out std_logic;
		dma_mreq_n_o	: out std_logic;
		dma_ioreq_n_o	: out std_logic;
		
		cpu_d_o			: out std_logic_vector(7 downto 0); 
		dma_out_o		: out   std_logic := '0' 	-- "1" when the interface have data to report
		
		
	);
		
end z80dma;
 
architecture z80dma_unit of z80dma is

	signal dma_cs_n_s					: std_logic;
	
	type  array_7x8 is array (0 to 6) of std_logic_vector(7 downto 0);
	signal reg							: array_7x8;



	signal reg_temp					: std_logic_vector(7 downto 0);
		
	signal R0_dir_AtoB_s				: std_logic;  -- direction - 0 = B -> A   1 = A -> B
	--signal R0_tranfer_s				: std_logic; 
	--signal R0_search_s				: std_logic;  
	signal R0_start_addr_port_A_s 	: std_logic_vector (15 downto 0);
	signal R0_block_len_s 			: std_logic_vector (15 downto 0);
	
	signal R1_portAisIO_s			: std_logic;  -- 0 = Port A is Memory   1 = Port A is IO
	signal R1_portA_addrMode_s		: std_logic_vector (1 downto 0); -- address mode - 00 = decrements, 01 = increments, 10 or 11 = fixed
	signal R1_portA_timming_byte_s : std_logic_vector (7 downto 0) := x"02";
	--signal R1_portA_preescaler_s 	: std_logic_vector (7 downto 0);
	
	signal R2_portBisIO_s			: std_logic;  -- 0 = Port B is Memory   1 = Port B is IO
	signal R2_portB_addrMode_s		: std_logic_vector (1 downto 0); -- address mode - 00 = decrements, 01 = increments, 10 or 11 = fixed
	signal R2_portB_timming_byte_s : std_logic_vector (7 downto 0) := x"02";
	signal R2_portB_preescaler_s 	: std_logic_vector (7 downto 0);

	signal R3_dma_en_s				: std_logic; 
	--signal R3_int_en_s				: std_logic;
	--signal R3_stop_match_s			: std_logic;
	--signal R3_mask_s 					: std_logic_vector (7 downto 0);
	--signal R3_match_s 				: std_logic_vector (7 downto 0);
	
	signal R4_mode_s				: std_logic_vector (1 downto 0); -- 00 byte, 01 continuos, 10 burst
	signal R4_start_addr_port_B_s : std_logic_vector (15 downto 0);
	--signal R4_interrupt_control_s : std_logic_vector (7 downto 0);
	--signal R4_pulse_control_s 		: std_logic_vector (7 downto 0);
	--signal R4_interrupt_vector_s 	: std_logic_vector (7 downto 0);
	
	--signal R5_ready_act_hi_s		: std_logic; -- 0 = READY activel low, 1 = READY active high
	signal R5_ce_wait_s				: std_logic;-- 0 = CE only, 1 = CE / WAIT multiplexed
	signal R5_auto_restart_s		: std_logic;-- 0 = stop on end, 1 = auto restart
	
	signal R6_read_mask_s 			: std_logic_vector (7 downto 0);
	
	signal temp_cs: std_logic_vector(1 downto 0);
	
	type dma_seq_t is ( IDLE, START_DMA, WAITING_ACK, TRANSFERING_READ_1, TRANSFERING_READ_2, TRANSFERING_READ_3, TRANSFERING_READ_4, TRANSFERING_WRITE_1, TRANSFERING_WRITE_2, TRANSFERING_WRITE_3, TRANSFERING_WRITE_4, WAITING_CYCLES, FINISH_DMA );
	signal dma_seq_s		: dma_seq_t;
	
	signal dma_a_s				: std_logic_vector(15 downto 0);
	signal dma_d_s				: std_logic_vector(7 downto 0);
	signal dma_rd_n_s			: std_logic;
	signal dma_wr_n_s			: std_logic;
	signal dma_mreq_n_s		: std_logic;
	signal dma_ioreq_n_s		: std_logic;
	
	signal dma_src_s			: std_logic_vector(15 downto 0);
	signal dma_dest_s			: std_logic_vector(15 downto 0);
	signal dma_counter_s		: std_logic_vector(15 downto 0);
	
	signal DMA_timer_s : std_logic_vector(7 downto 0);
	
	signal read_count_s : std_logic_vector(2 downto 0);
	
begin


	dma_cs_n_s <=	'0' when mode_i = '0' and cpu_a_i(7 downto 0) = X"0b" and cpu_ioreq_n_i = '0'  else
						'0' when mode_i = '1' and cpu_a_i(7 downto 0) = X"6b" and cpu_ioreq_n_i = '0'  else
						'1'; 
						
	dma_out_o <= '1' when dma_cs_n_s = '0' and cpu_rd_n_i = '0' else '0';

	dma_a_o			<= dma_a_s;	
	dma_d_o			<= dma_d_s;	
	dma_rd_n_o		<= dma_rd_n_s;
	dma_wr_n_o		<= dma_wr_n_s;
	dma_mreq_n_o	<= dma_mreq_n_s;
	dma_ioreq_n_o	<= dma_ioreq_n_s;		
						
	process (clk_i, reset_i)
	
		type reg_wr_seq_t is ( IDLE, R0_BYTE_0, R0_BYTE_1, R0_BYTE_2, R0_BYTE_3, R1_BYTE_0,  R1_BYTE_1, R2_BYTE_0, R2_BYTE_1, R3_BYTE_0, R3_BYTE_1, R4_BYTE_0, R4_BYTE_1, R4_BYTE_2, R4_BYTE_3, R4_BYTE_4, R6_BYTE_0 );
		variable reg_wr_seq_s		: reg_wr_seq_t;
		
		type reg_rd_seq_t is ( RD_STATUS, RD_COUNTER_HI, RD_COUNTER_LO, RD_PORT_A_LO, RD_PORT_A_HI, RD_PORT_B_LO, RD_PORT_B_HI, RD_IDLE );
		variable reg_rd_seq_s		: reg_rd_seq_t;

		variable cs_wr_v : std_logic_vector(1 downto 0);
		variable cs_rd_v : std_logic_vector(1 downto 0);

		
	begin
		if reset_i = '1' then
		
				dma_seq_s <= IDLE;
				
				dma_a_s			<= X"ffff"; --ffff
				dma_d_s			<= X"ff"; --00
				dma_rd_n_s		<= '1';
				dma_wr_n_s		<= '1';
				dma_mreq_n_s	<= '1';
				dma_ioreq_n_s	<= '1';
				dma_counter_s 	<= (others=>'0');
				
				cpu_d_o			<= (others=>'0');

				cpu_busreq_n_o <= '1';
				
				reg_wr_seq_s := IDLE;
				
				DMA_timer_s <= X"00";
				
				R1_portA_timming_byte_s <= x"02";
				R2_portB_timming_byte_s <= x"02";
				R2_portB_preescaler_s <= X"00";
				R5_auto_restart_s <= '0';
				R6_read_mask_s <= "01111111";
				
				read_count_s <= "000";
				
				reg_rd_seq_s := RD_STATUS;
				
		elsif rising_edge(clk_i) then
		
			DMA_timer_s <= DMA_timer_s + 1;
		
			--DMA transfer process
			case dma_seq_s is
						
					when IDLE => 
					
						cpu_busreq_n_o <= '1';
						
					when START_DMA => 
					
						cpu_busreq_n_o <= '0';

						dma_seq_s 		<= WAITING_ACK;
					
					when WAITING_ACK => 
					
						dma_a_s	 		<= dma_src_s;	
						dma_rd_n_s		<= '1';
						dma_mreq_n_s	<= '1';
						dma_ioreq_n_s	<= '1';	
					
						if cpu_busack_n_i = '0' then
							dma_seq_s <= TRANSFERING_READ_1;
						end if;
						
					when TRANSFERING_READ_1 => 
					
						DMA_timer_s <= x"00";
						
						--dma_a_s 			<= dma_src_s;
					
						if R5_ce_wait_s = '1' and wait_n_i = '0' then
						
								dma_seq_s 		<= TRANSFERING_READ_1;
								
						else
						
								dma_d_s 			<= dma_d_i;	
						
								dma_rd_n_s		<= '0';
								
								if (R0_dir_AtoB_s = '1' and R1_portAisIO_s = '0') or
									(R0_dir_AtoB_s = '0' and R2_portBisIO_s = '0') then
									dma_mreq_n_s	<= '0';		
								end if;
								
								if (R0_dir_AtoB_s = '1' and R1_portAisIO_s = '1') or
									(R0_dir_AtoB_s = '0' and R2_portBisIO_s = '1') then
									dma_ioreq_n_s	<= '0';	
								end if;
								
								--dma_seq_s 		<= TRANSFERING_READ_2;
								
								if R0_dir_AtoB_s = '1' then -- A -> B
									case R1_portA_timming_byte_s(1 downto 0) is
										when "00" =>   dma_seq_s 	<= TRANSFERING_READ_2; -- 4 cycles
										when "01" =>   dma_seq_s 	<= TRANSFERING_READ_3; -- 3 cycles
										when "10" =>   dma_seq_s 	<= TRANSFERING_READ_4; -- 2 cycles									
										when others => dma_seq_s 	<= TRANSFERING_READ_2; -- 4 cycles		
									end case;
								else -- B -> A
									case R2_portB_timming_byte_s(1 downto 0) is
										when "00" =>   dma_seq_s 	<= TRANSFERING_READ_2; -- 4 cycles
										when "01" =>   dma_seq_s 	<= TRANSFERING_READ_3; -- 3 cycles
										when "10" =>   dma_seq_s 	<= TRANSFERING_READ_4; -- 2 cycles									
										when others => dma_seq_s 	<= TRANSFERING_READ_2; -- 4 cycles		
									end case;
								end if;
						
						end if;
						
					when TRANSFERING_READ_2 => 
						dma_d_s 			<= dma_d_i;	
						dma_seq_s 	<= TRANSFERING_READ_3;
						
					when TRANSFERING_READ_3 => 
						dma_d_s 			<= dma_d_i;	
						dma_seq_s 	<= TRANSFERING_READ_4;
								
					when TRANSFERING_READ_4 => 
						dma_d_s 			<= dma_d_i;	
						
						dma_rd_n_s		<= '1';
						dma_mreq_n_s	<= '1';	
						dma_ioreq_n_s	<= '1';			
						
						dma_seq_s 	<= TRANSFERING_WRITE_1;
						dma_a_s 			<= dma_dest_s; --set the addr for writing at the next cycle

					when TRANSFERING_WRITE_1 => 
					
						--dma_a_s 			<= dma_dest_s;
						
						if R5_ce_wait_s = '1' and wait_n_i = '0' then
						
								dma_seq_s		<= TRANSFERING_WRITE_1;
							
						else

								
								
								if (R0_dir_AtoB_s = '1' and R2_portBisIO_s = '0') or
									(R0_dir_AtoB_s = '0' and R1_portAisIO_s = '0') then
									dma_mreq_n_s	<= '0';		
								end if;
								
								if (R0_dir_AtoB_s = '1' and R2_portBisIO_s = '1') or
									(R0_dir_AtoB_s = '0' and R1_portAisIO_s = '1') then
									dma_ioreq_n_s	<= '0';	
								end if;
								
								dma_counter_s 	<= dma_counter_s + 1;
								dma_wr_n_s		<= '0';
								
								--dma_seq_s		<= TRANSFERING_WRITE_2;
								
								if R0_dir_AtoB_s = '0' then -- B -> A
									case R1_portA_timming_byte_s(1 downto 0) is
										when "00" =>   dma_seq_s 	<= TRANSFERING_WRITE_2; -- 4 cycles
										when "01" =>   dma_seq_s 	<= TRANSFERING_WRITE_3; -- 3 cycles
										when "10" =>   dma_seq_s 	<= TRANSFERING_WRITE_4; -- 2 cycles									
										when others => dma_seq_s 	<= TRANSFERING_WRITE_2; -- 4 cycles		
									end case;
								else -- A -> B
									case R2_portB_timming_byte_s(1 downto 0) is
										when "00" =>   dma_seq_s 	<= TRANSFERING_WRITE_2; -- 4 cycles
										when "01" =>   dma_seq_s 	<= TRANSFERING_WRITE_3; -- 3 cycles
										when "10" =>   dma_seq_s 	<= TRANSFERING_WRITE_4; -- 2 cycles									
										when others => dma_seq_s 	<= TRANSFERING_WRITE_2; -- 4 cycles		
									end case;
								end if;
								
								
								
								if (R0_dir_AtoB_s = '1' and R1_portA_addrMode_s	= "01") or
									(R0_dir_AtoB_s = '0' and R2_portB_addrMode_s	= "01") then
									dma_src_s		<= dma_src_s + 1;
								end if;
								
								if (R0_dir_AtoB_s = '1' and R1_portA_addrMode_s	= "00") or
									(R0_dir_AtoB_s = '0' and R2_portB_addrMode_s	= "00") then
									dma_src_s		<= dma_src_s - 1;
								end if;
									
								if (R0_dir_AtoB_s = '1' and R2_portB_addrMode_s	= "01") or
									(R0_dir_AtoB_s = '0' and R1_portA_addrMode_s	= "01") then
									dma_dest_s		<= dma_dest_s + 1;
								end if;
								
								if (R0_dir_AtoB_s = '1' and R2_portB_addrMode_s	= "00") or
									(R0_dir_AtoB_s = '0' and R1_portA_addrMode_s	= "00") then
									dma_dest_s		<= dma_dest_s - 1;
								end if;
								
								
						
						end if;
						
					
					when TRANSFERING_WRITE_2 => 
						dma_wr_n_s		<= '0';
						dma_seq_s		<= TRANSFERING_WRITE_3;

					when TRANSFERING_WRITE_3 => 
						dma_wr_n_s		<= '0';
						dma_seq_s		<= TRANSFERING_WRITE_4;
						
					when TRANSFERING_WRITE_4 => 
					
						dma_wr_n_s		<= '1';
						dma_mreq_n_s	<= '1';
						dma_ioreq_n_s	<= '1';	
				

					 -- movi o bloco de atualizat o endere�o destino para o come�o do write
						
						
						--prepare the next read
						dma_a_s 			<= dma_src_s;
						
						--check if we need to wait cycles before the next cycle
						if R2_portB_preescaler_s > 0 then
						
								if R2_portB_preescaler_s > DMA_timer_s then
										dma_seq_s 	<= WAITING_CYCLES;
								end if;
								
						elsif dma_counter_s < R0_block_len_s then --TO DO: test for block len = 0 - check the datasheet!
						
								--	if R4_mode_s = "10" then -- burst mode
								--		dma_seq_s <= WAITING_ACK;
								--	else
										dma_seq_s <= TRANSFERING_READ_1;
								--	end if;
								
						else  
									dma_seq_s <= FINISH_DMA;
						end if;
					
						
					when WAITING_CYCLES => 
					
						if R4_mode_s = "10" then -- burst mode

								-- TO DO: give more time to CPU in burst mode
								if R2_portB_preescaler_s(7 downto 1) <  DMA_timer_s then -- ok, bu it's half of the cpu cycles
								--if DMA_timer_s >= std_logic_vector(unsigned(R2_portB_preescaler_s - 100)) then --   -100 is ok. -75 start to works
								--if R2_portB_preescaler_s > DMA_timer_s then
									cpu_busreq_n_o <= '0'; --request the cpu and wait for the next ack
								else
									cpu_busreq_n_o <= '1'; --release the cpu
								end if;
								
						end if;
									
						if R2_portB_preescaler_s > 0 and R2_portB_preescaler_s > DMA_timer_s then
									dma_seq_s 	<= WAITING_CYCLES;
									
						elsif dma_counter_s < R0_block_len_s then --TO DO: test for block len = 0 - check the datasheet!

									if R4_mode_s = "10" then -- burst mode
										dma_seq_s <= WAITING_ACK;
									else
										dma_seq_s <= TRANSFERING_READ_1;
									end if;

						else  
									dma_seq_s 	<= FINISH_DMA;
						end if;
						
						
						
					when FINISH_DMA =>
						
						if R5_auto_restart_s = '1' then --reload the values
								if R0_dir_AtoB_s = '1' then  -- direction - 0 = B -> A   1 = A -> B
									dma_src_s		<= R0_start_addr_port_A_s;
									dma_dest_s		<= R4_start_addr_port_B_s;
								else
									dma_src_s		<= R4_start_addr_port_B_s;
									dma_dest_s		<= R0_start_addr_port_A_s;
								end if;
								
								dma_counter_s 		<= (others=>'0');
								
								dma_seq_s 	<= WAITING_ACK;
						else
								dma_seq_s 	<= IDLE;
						end if;
						

		
			end case;	
		
				
				--detect WR falling edge 
				cs_wr_v := cs_wr_v(0) & (dma_cs_n_s or cpu_wr_n_i);
					 
				--detect RD falling edge 
				cs_rd_v := cs_rd_v(0) & (dma_cs_n_s or cpu_rd_n_i);
				
				temp_cs <= cs_wr_v;
				
				if cs_wr_v = "10" then
				--write in registers (only on falling edge of DMA CS and write)
				
				
						case reg_wr_seq_s is
						
						when IDLE =>
									
									-- Register 0
									if cpu_d_i(7) = '0' and (cpu_d_i(1) = '1' or cpu_d_i(0) = '1') then 
											
											reg_temp <= cpu_d_i;

											R0_dir_AtoB_s 	<= cpu_d_i(2);
											--R0_search_s 	<= cpu_d_i(1);
											--R0_tranfer_s 	<= cpu_d_i(0);
											
											if cpu_d_i(3) = '1' then
												reg_wr_seq_s := R0_BYTE_0;
											elsif cpu_d_i(4) = '1' then
												reg_wr_seq_s := R0_BYTE_1;
											elsif cpu_d_i(5) = '1' then
												reg_wr_seq_s := R0_BYTE_2;
											elsif cpu_d_i(6) = '1' then
												reg_wr_seq_s := R0_BYTE_3;
											else
												reg_wr_seq_s := IDLE;
											end if;

									end if;
									-- end of Register 0
									
									-- Register 1
									if cpu_d_i(7) = '0' and cpu_d_i(2 downto 0) = "100" then 
											
											reg_temp <= cpu_d_i;
											
											R1_portAisIO_s <= cpu_d_i(3);
											R1_portA_addrMode_s <= cpu_d_i(5 downto 4);
											
											if cpu_d_i(6) = '0' then 
												reg_wr_seq_s := IDLE;
											else
												reg_wr_seq_s := R1_BYTE_0;
											end if;

									end if;
									-- end of Register 1
									
									-- Register 2
									if cpu_d_i(7) = '0' and cpu_d_i(2 downto 0) = "000" then 
											
											reg_temp <= cpu_d_i;
											
											R2_portBisIO_s <= cpu_d_i(3);
											R2_portB_addrMode_s <= cpu_d_i(5 downto 4);
											
											if cpu_d_i(6) = '0' then 
												reg_wr_seq_s := IDLE;
											else
												reg_wr_seq_s := R2_BYTE_0;
											end if;

									end if;
									-- end of Register 2
									
									-- Register 3
									if cpu_d_i(7) = '1' and cpu_d_i(1 downto 0) = "00" then 
											
											reg_temp <= cpu_d_i;
											
											R3_dma_en_s	<= cpu_d_i(6);
											
											if cpu_d_i(6) = '1' then
												dma_seq_s <= START_DMA;
											end if;
											
											--R3_int_en_s	<= cpu_d_i(5);
											--R3_stop_match_s <= cpu_d_i(2);
											
											if cpu_d_i(3) = '1' then
												reg_wr_seq_s := R3_BYTE_0;
											elsif cpu_d_i(4) = '1' then
												reg_wr_seq_s := R3_BYTE_1;
											else
												reg_wr_seq_s := IDLE;
											end if;

									end if;
									-- end of Register 3
									
									-- Register 4
									if cpu_d_i(7) = '1' and cpu_d_i(1 downto 0) = "01" then 
									
											reg_temp <= cpu_d_i;
											
											R4_mode_s <= cpu_d_i(6 downto 5);
											
											if cpu_d_i(2) = '1' then
												reg_wr_seq_s := R4_BYTE_0;
											elsif cpu_d_i(3) = '1' then
												reg_wr_seq_s := R4_BYTE_1;
											elsif cpu_d_i(4) = '1' then
												reg_wr_seq_s := R4_BYTE_2;
											else
												reg_wr_seq_s := IDLE;
											end if;
										
									end if;
									-- end of Register 4
							
									-- Register 5
									if cpu_d_i(7 downto 6) = "10" and cpu_d_i(2 downto 0) = "010" then 
											
											reg_temp <= cpu_d_i;

										--	R5_ready_act_hi_s <= cpu_d_i(3);
											R5_ce_wait_s <= cpu_d_i(4);
											R5_auto_restart_s <= cpu_d_i(5);

											reg_wr_seq_s := IDLE;

									end if;
									-- end of Register 5
							
							
									-- Register 6
									if cpu_d_i(7) = '1' and cpu_d_i(1 downto 0) = "11" then 
										
										reg_temp <= cpu_d_i;
										
										reg_wr_seq_s := IDLE;
										
										if cpu_d_i(7 downto 0) = X"C3" then -- reset
										
										elsif cpu_d_i(7 downto 0) = X"C7" then -- reset port A timming
										
										elsif cpu_d_i(7 downto 0) = X"CB" then -- reset port B timming
										
										
										
										elsif cpu_d_i(7 downto 0) = X"CF" then -- Load
											if R0_dir_AtoB_s = '1' then  -- direction - 0 = B -> A   1 = A -> B
												dma_src_s		<= R0_start_addr_port_A_s;
												dma_dest_s		<= R4_start_addr_port_B_s;
											else
												dma_src_s		<= R4_start_addr_port_B_s;
												dma_dest_s		<= R0_start_addr_port_A_s;
											end if;
											
											dma_counter_s 		<= (others=>'0');
						
										
										elsif cpu_d_i(7 downto 0) = X"D3" then -- Continue
											dma_counter_s 		<= (others=>'0');
										
										elsif cpu_d_i(7 downto 0) = X"AF" then -- Disable Interrupts
										elsif cpu_d_i(7 downto 0) = X"AB" then -- Enable Interrupts
										elsif cpu_d_i(7 downto 0) = X"A3" then -- Reset and Disable Interrupts
										elsif cpu_d_i(7 downto 0) = X"B7" then -- Enable after RETI
								
										elsif cpu_d_i(7 downto 0) = X"BF" then -- Read Status byte
										elsif cpu_d_i(7 downto 0) = X"8B" then -- Reinitialize Status Byte
								
						
										elsif cpu_d_i(7 downto 0) = X"A7" then -- Initialize Read Sequence
								
										elsif cpu_d_i(7 downto 0) = X"B3" then -- Force Ready
										
										elsif cpu_d_i(7 downto 0) = X"87" then -- Enable DMA
											dma_seq_s <= START_DMA;
											
										elsif cpu_d_i(7 downto 0) = X"83" then -- Disable DMA
											cpu_busreq_n_o <= '1'; --release the bus
											dma_seq_s 	<=  IDLE;

										elsif cpu_d_i(7 downto 0) = X"BB" then -- Read mask follows
											reg_wr_seq_s := R6_BYTE_0;
										end if;
										
									end if;
									-- end of Register 6
	
						
						when R0_BYTE_0 =>
								R0_start_addr_port_A_s(7 downto 0) <= cpu_d_i;
								
								if reg_temp(4) = '1' then
									reg_wr_seq_s := R0_BYTE_1;
								elsif reg_temp(5) = '1' then
									reg_wr_seq_s := R0_BYTE_2;
								elsif reg_temp(6) = '1' then
									reg_wr_seq_s := R0_BYTE_3;
								else
									reg_wr_seq_s := IDLE;
								end if;

						when R0_BYTE_1 =>
								R0_start_addr_port_A_s(15 downto 8) <= cpu_d_i;
								
								if reg_temp(5) = '1' then
									reg_wr_seq_s := R0_BYTE_2;
								elsif reg_temp(6) = '1' then
									reg_wr_seq_s := R0_BYTE_3;
								else
									reg_wr_seq_s := IDLE;
								end if;
											
						when R0_BYTE_2 =>
								R0_block_len_s(7 downto 0) <= cpu_d_i;
								
								if reg_temp(6) = '1' then
									reg_wr_seq_s := R0_BYTE_3;
								else
									reg_wr_seq_s := IDLE;
								end if;

						when R0_BYTE_3 =>
								R0_block_len_s(15 downto 8) <= cpu_d_i;
								reg_wr_seq_s := IDLE;
									
						when R1_BYTE_0 =>
								R1_portA_timming_byte_s <= cpu_d_i;
								
								if cpu_d_i(5) = '1' then
										reg_wr_seq_s := R1_BYTE_1;
								else
										reg_wr_seq_s := IDLE;
								end if;
											
								
						when R1_BYTE_1 =>
								--R1_portA_preescaler_s <= cpu_d_i;
								reg_wr_seq_s := IDLE;
						
						
								
						when R2_BYTE_0 =>
								R2_portB_timming_byte_s <= cpu_d_i;
								
								if cpu_d_i(5) = '1' then
										reg_wr_seq_s := R2_BYTE_1;
								else
										reg_wr_seq_s := IDLE;
								end if;
								
						when R2_BYTE_1 =>
								R2_portB_preescaler_s <= cpu_d_i;
								reg_wr_seq_s := IDLE;
								
						when R3_BYTE_0 =>
								--R3_mask_s <= cpu_d_i;
								
								if reg_temp(4) = '1' then
									reg_wr_seq_s := R3_BYTE_1;
								else
									reg_wr_seq_s := IDLE;
								end if;

						when R3_BYTE_1 =>
								--R3_match_s <= cpu_d_i;
								reg_wr_seq_s := IDLE;

						when R4_BYTE_0 =>
								R4_start_addr_port_B_s(7 downto 0) <= cpu_d_i;
								
								if reg_temp(3) = '1' then
									reg_wr_seq_s := R4_BYTE_1;
								elsif reg_temp(4) = '1' then
									reg_wr_seq_s := IDLE; --R4_BYTE_2;
								else
									reg_wr_seq_s := IDLE;
								end if;

						when R4_BYTE_1 =>
								R4_start_addr_port_B_s(15 downto 8) <= cpu_d_i;
								
						--		if reg_temp(4) = '1' then
						--			reg_wr_seq_s := R4_BYTE_2;
						--		else
									reg_wr_seq_s := IDLE;
						--		end if;
									
					--	when R4_BYTE_2 =>
					--			R4_interrupt_control_s <= cpu_d_i;
					--			
					--			if cpu_d_i(3) = '1' then
					--				reg_wr_seq_s := R4_BYTE_3;
					--			elsif cpu_d_i(4) = '1' then
					--				reg_wr_seq_s := R4_BYTE_4;
					--			else
					--				reg_wr_seq_s := IDLE;
					--			end if;
					--				
					--	when R4_BYTE_3 =>
					--			R4_pulse_control_s <= cpu_d_i;
					--			
					--			if R4_interrupt_control_s(4) = '1' then
					--				reg_wr_seq_s := R4_BYTE_4;
					--			else
					--				reg_wr_seq_s := IDLE;
					--			end if;
	--
					--	when R4_BYTE_4 =>
					--			R4_interrupt_vector_s <= cpu_d_i;
					--			reg_wr_seq_s := IDLE;
							
						when R6_BYTE_0 =>
								R6_read_mask_s <= cpu_d_i;
								
								if cpu_d_i(0) = '1' then -- status byte
									reg_rd_seq_s := RD_STATUS;
									
								elsif cpu_d_i(1) = '1' then -- byte counter LO
									reg_rd_seq_s := RD_COUNTER_LO;
								
								elsif cpu_d_i(2) = '1' then -- byte counter HI
									reg_rd_seq_s := RD_COUNTER_HI;	
									
								elsif cpu_d_i(3) = '1' then -- port A address counter LO
									reg_rd_seq_s := RD_PORT_A_LO;
									
								elsif cpu_d_i(4) = '1' then -- port A address counter HI
									reg_rd_seq_s := RD_PORT_A_HI;

								elsif cpu_d_i(5) = '1' then -- port B address counter LO
									reg_rd_seq_s := RD_PORT_B_LO;
									
								elsif cpu_d_i(6) = '1' then -- port B address counter HI
									reg_rd_seq_s := RD_PORT_B_HI;
									
								end if;
								
								reg_wr_seq_s := IDLE;	
								
								
						when others => null;
						end case;	

	
				elsif cs_rd_v = "10" then
				--read from registers (only on falling edge of DMA CS and RD)
									
							
							
							case reg_rd_seq_s is
								
							when RD_STATUS => 
							
								cpu_d_o <= x"00"; -- TO DO
							
								if R6_read_mask_s(1) = '1' then -- byte counter LO
									reg_rd_seq_s := RD_COUNTER_LO;
									
								elsif R6_read_mask_s(2) = '1' then -- byte counter HI
									reg_rd_seq_s := RD_COUNTER_HI;
									
								elsif R6_read_mask_s(3) = '1' then -- port A address counter LO
									reg_rd_seq_s := RD_PORT_A_LO;
									
								elsif R6_read_mask_s(4) = '1' then -- port A address counter HI
									reg_rd_seq_s := RD_PORT_A_HI;
									
								elsif R6_read_mask_s(5) = '1' then -- port B address counter LO
									reg_rd_seq_s := RD_PORT_B_LO;
									
								elsif R6_read_mask_s(6) = '1' then -- port B address counter HI
									reg_rd_seq_s := RD_PORT_B_HI;
									
								elsif R6_read_mask_s(0) = '1' then -- status byte
									reg_rd_seq_s := RD_STATUS;
									
								else
									reg_rd_seq_s := RD_IDLE;
									
								end if;
							
							
						when RD_COUNTER_LO => 
							
								cpu_d_o <= dma_counter_s(15 downto 8);
							
								if R6_read_mask_s(2) = '1' then -- byte counter HI
									reg_rd_seq_s := RD_COUNTER_HI;
									
								elsif R6_read_mask_s(3) = '1' then -- port A address counter LO
									reg_rd_seq_s := RD_PORT_A_LO;
									
								elsif R6_read_mask_s(4) = '1' then -- port A address counter HI
									reg_rd_seq_s := RD_PORT_A_HI;
									
								elsif R6_read_mask_s(5) = '1' then -- port B address counter LO
									reg_rd_seq_s := RD_PORT_B_LO;
									
								elsif R6_read_mask_s(6) = '1' then -- port B address counter HI
									reg_rd_seq_s := RD_PORT_B_HI;
									
								elsif R6_read_mask_s(0) = '1' then -- status byte
									reg_rd_seq_s := RD_STATUS;
									
								elsif R6_read_mask_s(1) = '1' then -- byte counter LO
									reg_rd_seq_s := RD_COUNTER_LO;	
									
								else
									reg_rd_seq_s := RD_IDLE;
									
								end if;
							
							
							when RD_COUNTER_HI => 
							
								cpu_d_o <= dma_counter_s(7 downto 0);
								
								if R6_read_mask_s(3) = '1' then -- port A address counter LO
									reg_rd_seq_s := RD_PORT_A_LO;
									
								elsif R6_read_mask_s(4) = '1' then -- port A address counter HI
									reg_rd_seq_s := RD_PORT_A_HI;
									
								elsif R6_read_mask_s(5) = '1' then -- port B address counter LO
									reg_rd_seq_s := RD_PORT_B_LO;
									
								elsif R6_read_mask_s(6) = '1' then -- port B address counter HI
									reg_rd_seq_s := RD_PORT_B_HI;
									
								elsif R6_read_mask_s(0) = '1' then -- status byte
									reg_rd_seq_s := RD_STATUS;
									
								elsif R6_read_mask_s(1) = '1' then -- byte counter LO
									reg_rd_seq_s := RD_COUNTER_LO;
								
								elsif R6_read_mask_s(2) = '1' then -- byte counter HI
									reg_rd_seq_s := RD_COUNTER_HI;
								
								else
									reg_rd_seq_s := RD_IDLE;
									
								end if;
						
							
							when RD_PORT_A_LO => 
							
							
								if R0_dir_AtoB_s = '1' then  -- direction - 0 = B -> A   1 = A -> B
										cpu_d_o <=	dma_src_s(7 downto 0);
								else
										cpu_d_o <=	dma_dest_s(7 downto 0);
								end if;
									
								if R6_read_mask_s(4) = '1' then -- port A address counter HI
									reg_rd_seq_s := RD_PORT_A_HI;
									
								elsif R6_read_mask_s(5) = '1' then -- port B address counter LO
									reg_rd_seq_s := RD_PORT_B_LO;
									
								elsif R6_read_mask_s(6) = '1' then -- port B address counter HI
									reg_rd_seq_s := RD_PORT_B_HI;
									
								elsif R6_read_mask_s(0) = '1' then -- status byte
									reg_rd_seq_s := RD_STATUS;
									
								elsif R6_read_mask_s(1) = '1' then -- byte counter LO
									reg_rd_seq_s := RD_COUNTER_LO;
								
								elsif R6_read_mask_s(2) = '1' then -- byte counter HI
									reg_rd_seq_s := RD_COUNTER_HI;	
									
								elsif R6_read_mask_s(3) = '1' then -- port A address counter LO
									reg_rd_seq_s := RD_PORT_A_LO;
									
								else
									reg_rd_seq_s := RD_IDLE;
									
								end if;
								
								
						
							when RD_PORT_A_HI => 
							
								if R0_dir_AtoB_s = '1' then  -- direction - 0 = B -> A   1 = A -> B
										cpu_d_o <=	dma_src_s(15 downto 8);
								else
										cpu_d_o <=	dma_dest_s(15 downto 8);
								end if;
									
								if R6_read_mask_s(5) = '1' then -- port B address counter LO
									reg_rd_seq_s := RD_PORT_B_LO;
									
								elsif R6_read_mask_s(6) = '1' then -- port B address counter HI
									reg_rd_seq_s := RD_PORT_B_HI;
									
								elsif R6_read_mask_s(0) = '1' then -- status byte
									reg_rd_seq_s := RD_STATUS;
									
								elsif R6_read_mask_s(1) = '1' then -- byte counter LO
									reg_rd_seq_s := RD_COUNTER_LO;
								
								elsif R6_read_mask_s(2) = '1' then -- byte counter HI
									reg_rd_seq_s := RD_COUNTER_HI;	
									
								elsif R6_read_mask_s(3) = '1' then -- port A address counter LO
									reg_rd_seq_s := RD_PORT_A_LO;
									
								elsif R6_read_mask_s(4) = '1' then -- port A address counter HI
									reg_rd_seq_s := RD_PORT_A_HI;
									
								else
									reg_rd_seq_s := RD_IDLE;

								end if;
								
								
							when RD_PORT_B_LO => 
							
								if R0_dir_AtoB_s = '1' then  -- direction - 0 = B -> A   1 = A -> B
										cpu_d_o <=	dma_dest_s(7 downto 0);
								else
										cpu_d_o <=	dma_src_s(7 downto 0);
								end if;
									
								if R6_read_mask_s(6) = '1' then -- port B address counter HI
									reg_rd_seq_s := RD_PORT_B_HI;
									
								elsif R6_read_mask_s(0) = '1' then -- status byte
									reg_rd_seq_s := RD_STATUS;
									
								elsif R6_read_mask_s(1) = '1' then -- byte counter LO
									reg_rd_seq_s := RD_COUNTER_LO;
								
								elsif R6_read_mask_s(2) = '1' then -- byte counter HI
									reg_rd_seq_s := RD_COUNTER_HI;	
									
								elsif R6_read_mask_s(3) = '1' then -- port A address counter LO
									reg_rd_seq_s := RD_PORT_A_LO;
									
								elsif R6_read_mask_s(4) = '1' then -- port A address counter HI
									reg_rd_seq_s := RD_PORT_A_HI;

								elsif R6_read_mask_s(5) = '1' then -- port B address counter LO
									reg_rd_seq_s := RD_PORT_B_LO;
									
								else
									reg_rd_seq_s := RD_IDLE;
									
								end if;
								
								
							when RD_PORT_B_HI => 
							
								if R0_dir_AtoB_s = '1' then  -- direction - 0 = B -> A   1 = A -> B
										cpu_d_o <=	dma_dest_s(15 downto 8);
								else
										cpu_d_o <=	dma_src_s(15 downto 8);
								end if;
									
								if R6_read_mask_s(0) = '1' then -- status byte
									reg_rd_seq_s := RD_STATUS;
									
								elsif R6_read_mask_s(1) = '1' then -- byte counter LO
									reg_rd_seq_s := RD_COUNTER_LO;
								
								elsif R6_read_mask_s(2) = '1' then -- byte counter HI
									reg_rd_seq_s := RD_COUNTER_HI;	
									
								elsif R6_read_mask_s(3) = '1' then -- port A address counter LO
									reg_rd_seq_s := RD_PORT_A_LO;
									
								elsif R6_read_mask_s(4) = '1' then -- port A address counter HI
									reg_rd_seq_s := RD_PORT_A_HI;

								elsif R6_read_mask_s(5) = '1' then -- port B address counter LO
									reg_rd_seq_s := RD_PORT_B_LO;
									
								elsif R6_read_mask_s(6) = '1' then -- port B address counter HI
									reg_rd_seq_s := RD_PORT_B_HI;
									
								else
									reg_rd_seq_s := RD_IDLE;
									
								end if;
							
		
							when RD_IDLE => null;
							
							end case;
				
				end if;
			
		end if;
	end process;
	

	
end z80dma_unit;