--
-- TBBlue / ZX Spectrum Next project
--
-- Function Keys emulator - Fabio Belavenuto
--
-- All rights reserved
--
-- Redistribution and use in source and synthezised forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
-- Redistributions of source code must retain the above copyright notice,
-- this list of conditions and the following disclaimer.
--
-- Redistributions in synthesized form must reproduce the above copyright
-- notice, this list of conditions and the following disclaimer in the
-- documentation and/or other materials provided with the distribution.
--
-- Neither the name of the author nor the names of other contributors may
-- be used to endorse or promote products derived from this software without
-- specific prior written permission.
--
-- THIS CODE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
-- THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
-- PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.
--
-- You are responsible for any legal issues arising from your use of this code.
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity emu_fnkeys is
	generic (
		clkfreq_g			: integer										-- This is the system clock value in KHz
	);
	port (
		clock_i				: in  std_logic;
		rows_i				: in  std_logic_vector( 7 downto 0);
		rows_filter_o		: out std_logic_vector( 7 downto 0);
		cols_i				: in  std_logic_vector( 4 downto 0);
		cols_filter_o		: out std_logic_vector( 4 downto 0);
		btn_mf_n_i			: in  std_logic;
		btn_mf_filter_n_o	: out std_logic;
		btn_reset_n_i		: in  std_logic;
		fnkeys_o				: out std_logic_vector(12 downto 1)
	);
end entity;

architecture rtl of emu_fnkeys is

	-- Timer
	constant divider_time_c	: integer 									:= clkfreq_g*100;		-- 200 ms
	signal divider_cnt_q		: integer range 0 to divider_time_c	:= 0;
	signal timer_value_s		: unsigned(3 downto 0)					:= (others => '0');
	signal timer_set_s		: std_logic;
	signal timer_cnt_q		: unsigned(3 downto 0)					:= (others => '0');

	-- MF & RST keys
	signal mf_pressed_s		: std_logic;
	signal rst_holded_s		: std_logic;

	-- Matrix
	signal my_rows_s			: std_logic_vector( 7 downto 0);

begin

	rows_filter_o		<= rows_i	when mf_pressed_s = '0'	else my_rows_s;
	cols_filter_o		<= cols_i	when mf_pressed_s = '0'	else (others => '1');

	-- Timer
	process (clock_i)
	begin
		if rising_edge(clock_i) then
			if timer_set_s = '1' then
				timer_cnt_q <= timer_value_s;
			end if;
			if divider_cnt_q = divider_time_c then
				divider_cnt_q <= 0;
				if timer_cnt_q /= 0 then
					timer_cnt_q <= timer_cnt_q - 1;
				end if;
			else
				divider_cnt_q <= divider_cnt_q + 1;
			end if;
		end if;
	end process;

	-- Multiface button
	process (clock_i)
		variable keymf_edge_v		: std_logic_vector(1 downto 0)	:= "00";
		variable keyrst_edge_v		: std_logic_vector(1 downto 0)	:= "00";
		type states_t is (IDLE, MFPRESSING, MFRELEASE, MULTIFACE, RSTPRESSING, RSTRELEASE);
		variable state_v : states_t;
		variable cnt_v		: unsigned(7 downto 0) := (others => '0');
	begin
		if rising_edge(clock_i) then
			timer_set_s 		<= '0';
			btn_mf_filter_n_o	<= '1';
			mf_pressed_s		<= '0';
			rst_holded_s		<= '0';

			keymf_edge_v		:= keymf_edge_v(0)  & btn_mf_n_i;
			keyrst_edge_v		:= keyrst_edge_v(0) & btn_reset_n_i;

			case state_v is
				when IDLE =>
					btn_mf_filter_n_o	<= '1';
					if keymf_edge_v = "10" then
						state_v 			:= MFPRESSING;
						timer_set_s		<= '1';
						timer_value_s	<= to_unsigned(4, 4);		-- 4*200ms = 800ms
					end if;
					--
					if keyrst_edge_v = "10" then
						state_v 			:= RSTPRESSING;
						timer_set_s		<= '1';
						timer_value_s	<= to_unsigned(4, 4);		-- 4*200ms = 800ms
					end if;

				when MFPRESSING =>
					mf_pressed_s		<= '1';
					if keymf_edge_v = "01" then
						state_v 		:= MFRELEASE;
					end if;

				when MFRELEASE =>
					timer_set_s		<= '1';
					timer_value_s	<= (others => '0');
					if timer_cnt_q /= 0 then							-- Released before time
						cnt_v			:= (others => '1');
						state_v 		:= MULTIFACE;						-- Simulate the Multiface button
					else
						state_v 		:= IDLE;
					end if;

				when MULTIFACE =>
					btn_mf_filter_n_o	<= '0';
					if cnt_v /= 0 then
						cnt_v := cnt_v - 1;
					else
						state_v 		:= IDLE;
					end if;

				when RSTPRESSING =>
					if keyrst_edge_v = "01" then
						state_v 		:= RSTRELEASE;
					end if;

				when RSTRELEASE =>
					timer_set_s		<= '1';
					timer_value_s	<= (others => '0');
					if timer_cnt_q = 0 then								-- Released after time
						rst_holded_s	<= '1';
					end if;
					state_v 		:= IDLE;

			end case;
		end if;
	end process;

	-- Matrix controller
	process (clock_i)
		type states_t is (IDLE, SETTING, WAITING, READING, HARDRESET);
		variable state_v	: states_t;
		variable row_v		: std_logic;
		variable cnt_v		: unsigned(15 downto 0) := (others => '0');
	begin
		if rising_edge(clock_i) then
			case state_v is
				when IDLE =>
					fnkeys_o <= (others => '0');
					if mf_pressed_s = '1' then
						state_v 	:= SETTING;
						row_v		:= '0';
					end if;
					if rst_holded_s = '1' then
						state_v	:= HARDRESET;
					end if;

				when SETTING =>
					if row_v = '0' then
						my_rows_s <= "11110111";	-- Row 3
					else
						my_rows_s <= "11101111";	-- Row 4
					end if;
					row_v		:= not row_v;
					cnt_v			:= (others => '1');
					state_v 	:= WAITING;

				when WAITING =>
					if cnt_v /= 0 then
						cnt_v := cnt_v - 1;
					else
						state_v 	:= READING;
					end if;

				when READING =>
					if mf_pressed_s = '0' then
						state_v 	:= IDLE;
					else
						state_v 	:= SETTING;
					end if;
					if row_v = '1' then					-- Row 3
						fnkeys_o(1) <= not cols_i(0);		-- Key 1
						fnkeys_o(2) <= not cols_i(1);		-- Key 2
						fnkeys_o(3) <= not cols_i(2);		-- Key 3
						fnkeys_o(4) <= not cols_i(3);		-- Key 4
						fnkeys_o(5) <= not cols_i(4);		-- Key 5
					else										-- Row 4
						fnkeys_o(10) <= not cols_i(0);	-- Key 0
						fnkeys_o(9) <= not cols_i(1);		-- Key 9
						fnkeys_o(8) <= not cols_i(2);		-- Key 8
						fnkeys_o(7) <= not cols_i(3);		-- Key 7
						fnkeys_o(6) <= not cols_i(4);		-- Key 6
					end if;

				when HARDRESET =>
					fnkeys_o(1)	<= '1';
					state_v 		:= IDLE;

			end case;
		end if;
	end process;


end architecture;
