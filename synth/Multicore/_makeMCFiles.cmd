@echo off

set /p version="Enter VGA or HDMI: "

set fname_i=multicore
set fname_o=TBBlue_multicore_%version%

echo Generating Multicore Files
copy .\output_files\%fname_i%.sof D:\Documentacao\Eletronica\Multicore\Multicore_Bitstreams\TBBlue\%fname_o%.sof
c:\altera\13.0sp1\quartus\bin64\quartus_cpf -s EP4CE10 -d EPCS16 -c .\output_files\%fname_i%.sof D:\Documentacao\Eletronica\Multicore\Multicore_Bitstreams\TBBlue\%fname_o%.jic
c:\altera\13.0sp1\quartus\bin64\quartus_cpf -c .\output_files\%fname_i%.sof D:\Documentacao\Eletronica\Multicore\Multicore_Bitstreams\TBBlue\%fname_o%.rbf
pause
